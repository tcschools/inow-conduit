	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_bank_bank
	extrn	___bb_bankstream_bankstream
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_d3d9max2d_d3d9max2d
	extrn	___bb_data_data
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_map_map
	extrn	___bb_maxlua_maxlua
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_threads_threads
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	_bbArrayNew1D
	extrn	_bbDelay
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_bbStringFromInt
	extrn	_brl_filesystem_ChangeDir
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_CreateDir
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_WriteFile
	extrn	_brl_linkedlist_ListAddFirst
	extrn	_brl_linkedlist_ListAddLast
	extrn	_brl_linkedlist_TList
	extrn	_brl_retro_Instr
	extrn	_brl_retro_Left
	extrn	_brl_retro_Replace
	extrn	_brl_retro_Right
	extrn	_brl_retro_Trim
	extrn	_brl_standardio_Print
	extrn	_brl_stream_Eof
	extrn	_brl_stream_ReadLine
	extrn	_brl_stream_WriteLine
	extrn	_system_
	public	__bb_Course_Delete
	public	__bb_Course_New
	public	__bb_Enrollment_Delete
	public	__bb_Enrollment_New
	public	__bb_Staff_Delete
	public	__bb_Staff_New
	public	__bb_Student_Delete
	public	__bb_Student_New
	public	__bb_main
	public	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete
	public	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Application
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Resources
	public	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	public	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	public	_bb_Course
	public	_bb_Enrollment
	public	_bb_SendAll
	public	_bb_Staff
	public	_bb_Student
	public	_bb_cList
	public	_bb_charFix
	public	_bb_convertFromCSV
	public	_bb_eList
	public	_bb_enrollInTraining
	public	_bb_enrollmentsEnabled
	public	_bb_excludeList
	public	_bb_loadCourse
	public	_bb_loadEnrollment
	public	_bb_loadStaff
	public	_bb_loadStudent
	public	_bb_saveCourse
	public	_bb_saveEnrollment
	public	_bb_saveUsers
	public	_bb_schoolYear
	public	_bb_staList
	public	_bb_stuList
	public	_bb_studentsEnabled
	public	_bb_teachersEnabled
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_315],0
	je	_316
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_316:
	mov	dword [_315],1
	call	___bb_blitz_blitz
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_bank_bank
	call	___bb_bankstream_bankstream
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_d3d9max2d_d3d9max2d
	call	___bb_data_data
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_map_map
	call	___bb_maxlua_maxlua
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_threads_threads
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_22
	call	_bbObjectRegisterType
	add	esp,4
	push	_23
	call	_bbObjectRegisterType
	add	esp,4
	push	_29
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Course
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Enrollment
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Staff
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Student
	call	_bbObjectRegisterType
	add	esp,4
	push	_30
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_31
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_301
	push	0
	push	_32
	call	_brl_filesystem_CreateDir
	add	esp,8
_301:
	mov	eax,dword [_303]
	and	eax,1
	cmp	eax,0
	jne	_304
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_cList],eax
	or	dword [_303],1
_304:
	mov	eax,dword [_303]
	and	eax,2
	cmp	eax,0
	jne	_306
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_eList],eax
	or	dword [_303],2
_306:
	mov	eax,dword [_303]
	and	eax,4
	cmp	eax,0
	jne	_308
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_staList],eax
	or	dword [_303],4
_308:
	mov	eax,dword [_303]
	and	eax,8
	cmp	eax,0
	jne	_310
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_stuList],eax
	or	dword [_303],8
_310:
	mov	eax,dword [_303]
	and	eax,16
	cmp	eax,0
	jne	_312
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_excludeList],eax
	or	dword [_303],16
_312:
	push	_35
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_36
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_37
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_38
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_39
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_40
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_41
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_42
	push	dword [_bb_excludeList]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_48
	call	_brl_standardio_Print
	add	esp,4
	push	_49
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadCourse
	add	esp,4
	push	_50
	call	_brl_standardio_Print
	add	esp,4
	push	_51
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadEnrollment
	add	esp,4
	push	_52
	call	_brl_standardio_Print
	add	esp,4
	push	_53
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadStaff
	add	esp,4
	push	_54
	call	_brl_standardio_Print
	add	esp,4
	push	_55
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadStudent
	add	esp,4
	push	_56
	call	_brl_standardio_Print
	add	esp,4
	call	_bb_saveCourse
	push	_57
	call	_brl_standardio_Print
	add	esp,4
	call	_bb_saveUsers
	push	_58
	call	_brl_standardio_Print
	add	esp,4
	call	_bb_saveEnrollment
	mov	ebx,0
	jmp	_314
_61:
	push	_63
	mov	eax,15
	sub	eax,ebx
	push	eax
	call	_bbStringFromInt
	add	esp,4
	push	eax
	push	_62
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_brl_standardio_Print
	add	esp,4
	push	1000
	call	_bbDelay
	add	esp,4
_59:
	add	ebx,1
_314:
	cmp	ebx,15
	jle	_61
_60:
	push	_64
	call	_brl_standardio_Print
	add	esp,4
	push	_65
	call	_brl_standardio_Print
	add	esp,4
	push	_66
	call	_brl_standardio_Print
	add	esp,4
	call	_bb_SendAll
	push	_67
	call	_brl_standardio_Print
	add	esp,4
	call	_bbEnd
	mov	eax,0
	jmp	_227
_227:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_22
	mov	eax,0
	jmp	_230
_230:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete:
	push	ebp
	mov	ebp,esp
_233:
	mov	eax,0
	jmp	_317
_317:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_23
	mov	eax,0
	jmp	_236
_236:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete:
	push	ebp
	mov	ebp,esp
_239:
	mov	eax,0
	jmp	_318
_318:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_29
	mov	eax,0
	jmp	_242
_242:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete:
	push	ebp
	mov	ebp,esp
_245:
	mov	eax,0
	jmp	_319
_319:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Course_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Course
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+28],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	push	_43
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_34
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,0
	jmp	_248
_248:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Course_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_251:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_333
	push	eax
	call	_bbGCFree
	add	esp,4
_333:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_335
	push	eax
	call	_bbGCFree
	add	esp,4
_335:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_337
	push	eax
	call	_bbGCFree
	add	esp,4
_337:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_339
	push	eax
	call	_bbGCFree
	add	esp,4
_339:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_341
	push	eax
	call	_bbGCFree
	add	esp,4
_341:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_343
	push	eax
	call	_bbGCFree
	add	esp,4
_343:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_345
	push	eax
	call	_bbGCFree
	add	esp,4
_345:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_347
	push	eax
	call	_bbGCFree
	add	esp,4
_347:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_349
	push	eax
	call	_bbGCFree
	add	esp,4
_349:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_351
	push	eax
	call	_bbGCFree
	add	esp,4
_351:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_353
	push	eax
	call	_bbGCFree
	add	esp,4
_353:
	mov	eax,0
	jmp	_331
_331:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Enrollment_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Enrollment
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+28],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,dword [_bb_enrollmentsEnabled]
	inc	dword [eax+4]
	mov	dword [ebx+52],eax
	push	_43
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [ebx+56],eax
	mov	eax,0
	jmp	_254
_254:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Enrollment_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_257:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_369
	push	eax
	call	_bbGCFree
	add	esp,4
_369:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_371
	push	eax
	call	_bbGCFree
	add	esp,4
_371:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_373
	push	eax
	call	_bbGCFree
	add	esp,4
_373:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_375
	push	eax
	call	_bbGCFree
	add	esp,4
_375:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_377
	push	eax
	call	_bbGCFree
	add	esp,4
_377:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_379
	push	eax
	call	_bbGCFree
	add	esp,4
_379:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_381
	push	eax
	call	_bbGCFree
	add	esp,4
_381:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_383
	push	eax
	call	_bbGCFree
	add	esp,4
_383:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_385
	push	eax
	call	_bbGCFree
	add	esp,4
_385:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_387
	push	eax
	call	_bbGCFree
	add	esp,4
_387:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_389
	push	eax
	call	_bbGCFree
	add	esp,4
_389:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_391
	push	eax
	call	_bbGCFree
	add	esp,4
_391:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_393
	push	eax
	call	_bbGCFree
	add	esp,4
_393:
	mov	eax,0
	jmp	_367
_367:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Staff_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Staff
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+28],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_44
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,_45
	inc	dword [eax+4]
	mov	dword [ebx+52],eax
	mov	eax,dword [_bb_teachersEnabled]
	inc	dword [eax+4]
	mov	dword [ebx+56],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+60],eax
	push	_46
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [ebx+64],eax
	mov	eax,0
	jmp	_260
_260:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Staff_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_263:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_411
	push	eax
	call	_bbGCFree
	add	esp,4
_411:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_413
	push	eax
	call	_bbGCFree
	add	esp,4
_413:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_415
	push	eax
	call	_bbGCFree
	add	esp,4
_415:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_417
	push	eax
	call	_bbGCFree
	add	esp,4
_417:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_419
	push	eax
	call	_bbGCFree
	add	esp,4
_419:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_421
	push	eax
	call	_bbGCFree
	add	esp,4
_421:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_423
	push	eax
	call	_bbGCFree
	add	esp,4
_423:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_425
	push	eax
	call	_bbGCFree
	add	esp,4
_425:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_427
	push	eax
	call	_bbGCFree
	add	esp,4
_427:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_429
	push	eax
	call	_bbGCFree
	add	esp,4
_429:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_431
	push	eax
	call	_bbGCFree
	add	esp,4
_431:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_433
	push	eax
	call	_bbGCFree
	add	esp,4
_433:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_435
	push	eax
	call	_bbGCFree
	add	esp,4
_435:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_437
	push	eax
	call	_bbGCFree
	add	esp,4
_437:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_439
	push	eax
	call	_bbGCFree
	add	esp,4
_439:
	mov	eax,0
	jmp	_409
_409:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_New:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
	push	ebx
	call	_bbObjectCtor
	add	esp,4
	mov	dword [ebx],_bb_Student
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+8],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+12],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+16],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+20],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+24],eax
	mov	eax,_1
	inc	dword [eax+4]
	mov	dword [ebx+28],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+32],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+36],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+40],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+44],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+48],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+52],eax
	mov	eax,_47
	inc	dword [eax+4]
	mov	dword [ebx+56],eax
	mov	eax,_45
	inc	dword [eax+4]
	mov	dword [ebx+60],eax
	mov	eax,dword [_bb_studentsEnabled]
	inc	dword [eax+4]
	mov	dword [ebx+64],eax
	mov	eax,_bbEmptyString
	inc	dword [eax+4]
	mov	dword [ebx+68],eax
	push	_46
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	dword [ebx+72],eax
	mov	eax,0
	jmp	_266
_266:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_269:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_459
	push	eax
	call	_bbGCFree
	add	esp,4
_459:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_461
	push	eax
	call	_bbGCFree
	add	esp,4
_461:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_463
	push	eax
	call	_bbGCFree
	add	esp,4
_463:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_465
	push	eax
	call	_bbGCFree
	add	esp,4
_465:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_467
	push	eax
	call	_bbGCFree
	add	esp,4
_467:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_469
	push	eax
	call	_bbGCFree
	add	esp,4
_469:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_471
	push	eax
	call	_bbGCFree
	add	esp,4
_471:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_473
	push	eax
	call	_bbGCFree
	add	esp,4
_473:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_475
	push	eax
	call	_bbGCFree
	add	esp,4
_475:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_477
	push	eax
	call	_bbGCFree
	add	esp,4
_477:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_479
	push	eax
	call	_bbGCFree
	add	esp,4
_479:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_481
	push	eax
	call	_bbGCFree
	add	esp,4
_481:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_483
	push	eax
	call	_bbGCFree
	add	esp,4
_483:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_485
	push	eax
	call	_bbGCFree
	add	esp,4
_485:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_487
	push	eax
	call	_bbGCFree
	add	esp,4
_487:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_489
	push	eax
	call	_bbGCFree
	add	esp,4
_489:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_491
	push	eax
	call	_bbGCFree
	add	esp,4
_491:
	mov	eax,0
	jmp	_457
_457:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_SendAll:
	push	ebp
	mov	ebp,esp
	push	_68
	call	_system_
	add	esp,4
	mov	eax,0
	jmp	_271
_271:
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveCourse:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],0
	push	_69
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_69
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_70
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
	mov	dword [ebp-8],_1
	mov	eax,dword [_bb_cList]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp-16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-20],eax
	jmp	_71
_73:
	mov	eax,dword [ebp-20]
	push	_bb_Course
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
	cmp	edi,_bbNullObject
	je	_71
	mov	ebx,dword [edi+8]
	inc	dword [ebx+4]
	mov	eax,dword [edi+32]
	dec	dword [eax+4]
	jnz	_504
	push	eax
	call	_bbGCFree
	add	esp,4
_504:
	mov	dword [edi+32],ebx
	mov	esi,dword [_bb_excludeList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_74
_76:
	mov	eax,ebx
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_74
	push	1
	push	eax
	push	dword [edi+32]
	call	_brl_retro_Instr
	add	esp,12
	cmp	eax,0
	je	_511
	mov	dword [ebp-4],1
_511:
_74:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_76
_75:
	mov	eax,dword [edi+8]
	mov	eax,dword [eax+8]
	sub	eax,4
	push	eax
	push	dword [edi+8]
	call	_brl_retro_Left
	add	esp,8
	push	eax
	push	dword [ebp-8]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_512
	push	_bb_Course
	call	_bbObjectNew
	add	esp,4
	mov	ebx,eax
	push	_77
	mov	eax,dword [edi+8]
	mov	eax,dword [eax+8]
	sub	eax,4
	push	eax
	push	dword [edi+8]
	call	_brl_retro_Left
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_517
	push	eax
	call	_bbGCFree
	add	esp,4
_517:
	mov	dword [ebx+32],esi
	mov	eax,dword [edi+12]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_521
	push	eax
	call	_bbGCFree
	add	esp,4
_521:
	mov	dword [ebx+36],esi
	mov	eax,dword [ebx+32]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_525
	push	eax
	call	_bbGCFree
	add	esp,4
_525:
	mov	dword [ebx+28],esi
	mov	eax,_1
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_529
	push	eax
	call	_bbGCFree
	add	esp,4
_529:
	mov	dword [ebx+40],esi
	cmp	dword [ebp-4],0
	jne	_530
	push	ebx
	push	dword [_bb_cList]
	call	_brl_linkedlist_ListAddFirst
	add	esp,8
	push	dword [ebx+48]
	push	_78
	push	dword [ebx+44]
	push	_78
	push	dword [ebx+40]
	push	_78
	push	dword [ebx+36]
	push	_78
	push	dword [ebx+32]
	push	_78
	push	dword [ebx+28]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
_530:
	mov	eax,dword [edi+8]
	mov	eax,dword [eax+8]
	sub	eax,4
	push	eax
	push	dword [edi+8]
	call	_brl_retro_Left
	add	esp,8
	mov	dword [ebp-8],eax
_512:
	push	dword [edi+24]
	push	_79
	push	dword [edi+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+36]
	dec	dword [eax+4]
	jnz	_534
	push	eax
	call	_bbGCFree
	add	esp,4
_534:
	mov	dword [edi+36],ebx
	mov	ebx,dword [edi+8]
	inc	dword [ebx+4]
	mov	eax,dword [edi+28]
	dec	dword [eax+4]
	jnz	_538
	push	eax
	call	_bbGCFree
	add	esp,4
_538:
	mov	dword [edi+28],ebx
	push	_77
	push	dword [ebp-8]
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+40]
	dec	dword [eax+4]
	jnz	_542
	push	eax
	call	_bbGCFree
	add	esp,4
_542:
	mov	dword [edi+40],ebx
	cmp	dword [ebp-4],0
	jne	_543
	push	dword [edi+48]
	push	_78
	push	dword [edi+44]
	push	_78
	push	dword [edi+40]
	push	_78
	push	dword [edi+36]
	push	_78
	push	dword [edi+32]
	push	_78
	push	dword [edi+28]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
	jmp	_544
_543:
	push	dword [edi+48]
	push	_78
	push	dword [edi+44]
	push	_78
	push	_78
	push	dword [edi+36]
	push	_78
	push	dword [edi+32]
	push	_78
	push	dword [edi+28]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
_544:
	mov	dword [ebp-4],0
_71:
	mov	eax,dword [ebp-20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_73
_72:
	push	dword [ebp-12]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_273
_273:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveEnrollment:
	push	ebp
	mov	ebp,esp
	sub	esp,24
	push	ebx
	push	esi
	push	edi
	push	_80
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-12],eax
	push	_80
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-24],eax
	push	_81
	push	dword [ebp-24]
	call	_brl_stream_WriteLine
	add	esp,8
	mov	eax,dword [_bb_eList]
	mov	dword [ebp-16],eax
	mov	eax,dword [ebp-16]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-20],eax
	jmp	_82
_84:
	mov	eax,dword [ebp-20]
	push	_bb_Enrollment
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	edi,eax
	cmp	edi,_bbNullObject
	je	_82
	mov	dword [ebp-4],0
	mov	ebx,dword [edi+8]
	inc	dword [ebx+4]
	mov	eax,dword [edi+40]
	dec	dword [eax+4]
	jnz	_557
	push	eax
	call	_bbGCFree
	add	esp,4
_557:
	mov	dword [edi+40],ebx
	mov	ebx,dword [edi+12]
	inc	dword [ebx+4]
	mov	eax,dword [edi+44]
	dec	dword [eax+4]
	jnz	_561
	push	eax
	call	_bbGCFree
	add	esp,4
_561:
	mov	dword [edi+44],ebx
	mov	ebx,_47
	inc	dword [ebx+4]
	mov	eax,dword [edi+48]
	dec	dword [eax+4]
	jnz	_565
	push	eax
	call	_bbGCFree
	add	esp,4
_565:
	mov	dword [edi+48],ebx
	mov	ebx,dword [_bb_enrollmentsEnabled]
	inc	dword [ebx+4]
	mov	eax,dword [edi+52]
	dec	dword [eax+4]
	jnz	_569
	push	eax
	call	_bbGCFree
	add	esp,4
_569:
	mov	dword [edi+52],ebx
	push	dword [edi+56]
	push	_78
	push	dword [edi+52]
	push	_78
	push	dword [edi+48]
	push	_78
	push	dword [edi+44]
	push	_78
	push	dword [edi+40]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-24]
	call	_brl_stream_WriteLine
	add	esp,8
	mov	esi,dword [_bb_excludeList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_85
_87:
	mov	eax,ebx
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_85
	push	1
	push	eax
	push	dword [edi+40]
	call	_brl_retro_Instr
	add	esp,12
	cmp	eax,0
	je	_577
	mov	dword [ebp-4],1
_577:
_85:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_87
_86:
	cmp	dword [ebp-4],0
	jne	_578
	push	dword [edi+56]
	push	_78
	push	dword [edi+52]
	push	_78
	push	_88
	push	_78
	push	dword [edi+24]
	push	_78
	push	_77
	mov	eax,dword [edi+40]
	mov	eax,dword [eax+8]
	sub	eax,4
	push	eax
	push	dword [edi+40]
	call	_brl_retro_Left
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
	jmp	_579
_578:
	push	dword [edi+56]
	push	_78
	push	dword [edi+52]
	push	_78
	push	_88
	push	_78
	push	dword [edi+24]
	push	_78
	push	dword [edi+40]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
_579:
	mov	dword [ebp-8],0
	mov	esi,dword [ebp-12]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_89
_91:
	mov	eax,ebx
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_89
	push	eax
	push	edi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_587
	mov	dword [ebp-8],1
	jmp	_90
_587:
_89:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_91
_90:
	cmp	dword [ebp-8],0
	jne	_588
	mov	eax,dword [ebp-12]
	push	edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
_588:
_82:
	mov	eax,dword [ebp-20]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_84
_83:
	mov	esi,dword [ebp-12]
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_92
_94:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_92
	push	eax
	push	dword [ebp-24]
	call	_brl_stream_WriteLine
	add	esp,8
_92:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_94
_93:
	push	dword [ebp-24]
	call	_bb_enrollInTraining
	add	esp,4
	push	dword [ebp-24]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_275
_275:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_charFix:
	push	ebp
	mov	ebp,esp
	mov	eax,dword [ebp+8]
	push	_1
	push	_26
	push	eax
	call	_brl_retro_Replace
	add	esp,12
	push	_1
	push	_95
	push	eax
	call	_brl_retro_Replace
	add	esp,12
	push	_1
	push	_96
	push	eax
	call	_brl_retro_Replace
	add	esp,12
	push	_1
	push	_97
	push	eax
	call	_brl_retro_Replace
	add	esp,12
	jmp	_278
_278:
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveUsers:
	push	ebp
	mov	ebp,esp
	sub	esp,12
	push	ebx
	push	esi
	push	edi
	push	_98
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_98
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-12],eax
	push	_99
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
	mov	edi,dword [_bb_staList]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_100
_102:
	mov	eax,dword [ebp-4]
	push	_bb_Staff
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_100
	mov	eax,dword [ebx+8]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_606
	push	eax
	call	_bbGCFree
	add	esp,4
_606:
	mov	dword [ebx+24],esi
	push	dword [ebx+12]
	call	_bb_charFix
	add	esp,4
	push	eax
	push	_25
	push	dword [ebx+16]
	call	_bb_charFix
	add	esp,4
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_610
	push	eax
	call	_bbGCFree
	add	esp,4
_610:
	mov	dword [ebx+28],esi
	mov	eax,_103
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_614
	push	eax
	call	_bbGCFree
	add	esp,4
_614:
	mov	dword [ebx+32],esi
	push	dword [ebx+16]
	call	_bb_charFix
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_618
	push	eax
	call	_bbGCFree
	add	esp,4
_618:
	mov	dword [ebx+36],esi
	push	dword [ebx+12]
	call	_bb_charFix
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_622
	push	eax
	call	_bbGCFree
	add	esp,4
_622:
	mov	dword [ebx+40],esi
	mov	eax,dword [ebx+20]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_626
	push	eax
	call	_bbGCFree
	add	esp,4
_626:
	mov	dword [ebx+44],esi
	mov	eax,dword [_bb_teachersEnabled]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_630
	push	eax
	call	_bbGCFree
	add	esp,4
_630:
	mov	dword [ebx+56],esi
	mov	eax,_104
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_634
	push	eax
	call	_bbGCFree
	add	esp,4
_634:
	mov	dword [ebx+60],esi
	push	dword [ebx+64]
	push	_78
	push	dword [ebx+60]
	push	_78
	push	dword [ebx+56]
	push	_78
	push	dword [ebx+52]
	push	_78
	push	dword [ebx+48]
	push	_78
	push	dword [ebx+44]
	push	_78
	push	dword [ebx+40]
	push	_78
	push	dword [ebx+36]
	push	_78
	push	dword [ebx+32]
	push	_78
	push	dword [ebx+28]
	push	_78
	push	dword [ebx+24]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
_100:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_102
_101:
	mov	edi,dword [_bb_stuList]
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-8],eax
	jmp	_105
_107:
	mov	eax,dword [ebp-8]
	push	_bb_Student
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	ebx,eax
	cmp	ebx,_bbNullObject
	je	_105
	mov	eax,dword [ebx+24]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_644
	push	eax
	call	_bbGCFree
	add	esp,4
_644:
	mov	dword [ebx+32],esi
	push	dword [ebx+8]
	call	_bb_charFix
	add	esp,4
	push	eax
	push	_25
	push	dword [ebx+12]
	call	_bb_charFix
	add	esp,4
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_648
	push	eax
	call	_bbGCFree
	add	esp,4
_648:
	mov	dword [ebx+36],esi
	mov	eax,_103
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_652
	push	eax
	call	_bbGCFree
	add	esp,4
_652:
	mov	dword [ebx+40],esi
	push	dword [ebx+12]
	call	_bb_charFix
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_656
	push	eax
	call	_bbGCFree
	add	esp,4
_656:
	mov	dword [ebx+44],esi
	push	dword [ebx+8]
	call	_bb_charFix
	add	esp,4
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_660
	push	eax
	call	_bbGCFree
	add	esp,4
_660:
	mov	dword [ebx+48],esi
	push	_108
	push	dword [ebx+8]
	call	_bb_charFix
	add	esp,4
	push	eax
	push	_25
	push	dword [ebx+12]
	call	_bb_charFix
	add	esp,4
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_664
	push	eax
	call	_bbGCFree
	add	esp,4
_664:
	mov	dword [ebx+52],esi
	mov	eax,dword [_bb_studentsEnabled]
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_668
	push	eax
	call	_bbGCFree
	add	esp,4
_668:
	mov	dword [ebx+64],esi
	push	_109
	push	dword [ebx+28]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_669
	mov	eax,_104
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_673
	push	eax
	call	_bbGCFree
	add	esp,4
_673:
	mov	dword [ebx+68],esi
	jmp	_674
_669:
	mov	eax,_110
	inc	dword [eax+4]
	mov	esi,eax
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_678
	push	eax
	call	_bbGCFree
	add	esp,4
_678:
	mov	dword [ebx+68],esi
_674:
	push	dword [ebx+72]
	push	_78
	push	dword [ebx+68]
	push	_78
	push	dword [ebx+64]
	push	_78
	push	dword [ebx+60]
	push	_78
	push	dword [ebx+56]
	push	_78
	push	dword [ebx+52]
	push	_78
	push	dword [ebx+48]
	push	_78
	push	dword [ebx+44]
	push	_78
	push	dword [ebx+40]
	push	_78
	push	dword [ebx+36]
	push	_78
	push	dword [ebx+32]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-12]
	call	_brl_stream_WriteLine
	add	esp,8
_105:
	mov	eax,dword [ebp-8]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_107
_106:
	push	dword [ebp-12]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_280
_280:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromCSV:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	mov	esi,dword [ebp+8]
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	mov	edi,_bbEmptyString
	mov	eax,dword [esi+8]
	mov	dword [ebp-4],eax
_113:
	push	1
	push	esi
	call	_brl_retro_Left
	add	esp,8
	mov	ebx,eax
	push	_78
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_683
	push	edi
	call	_brl_retro_Trim
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	edi,_1
_683:
	push	_78
	push	ebx
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_684
	push	ebx
	push	edi
	call	_bbStringConcat
	add	esp,8
	mov	edi,eax
_684:
	sub	dword [ebp-4],1
	push	dword [ebp-4]
	push	esi
	call	_brl_retro_Right
	add	esp,8
	mov	esi,eax
_111:
	push	_1
	push	esi
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_113
_112:
	push	edi
	call	_brl_retro_Trim
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	mov	eax,dword [ebp-8]
	jmp	_283
_283:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadCourse:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
_116:
	push	5
	push	_685
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,0
	mov	edi,eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_117
_119:
	mov	eax,dword [ebp-4]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_117
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+esi*4+24]
	dec	dword [eax+4]
	jnz	_698
	push	eax
	call	_bbGCFree
	add	esp,4
_698:
	mov	eax,dword [ebp-8]
	mov	dword [eax+esi*4+24],ebx
	add	esi,1
_117:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_119
_118:
	push	_bb_Course
	call	_bbObjectNew
	add	esp,4
	mov	esi,eax
	mov	eax,dword [_bb_cList]
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_704
	push	eax
	call	_bbGCFree
	add	esp,4
_704:
	mov	dword [esi+8],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_708
	push	eax
	call	_bbGCFree
	add	esp,4
_708:
	mov	dword [esi+12],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+8+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_712
	push	eax
	call	_bbGCFree
	add	esp,4
_712:
	mov	dword [esi+16],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+12+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_716
	push	eax
	call	_bbGCFree
	add	esp,4
_716:
	mov	dword [esi+20],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+16+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_720
	push	eax
	call	_bbGCFree
	add	esp,4
_720:
	mov	dword [esi+24],ebx
_114:
	push	dword [ebp+8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_116
_115:
	push	dword [ebp+8]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_286
_286:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadEnrollment:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
_122:
	push	7
	push	_721
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,0
	mov	edi,eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_123
_125:
	mov	eax,dword [ebp-4]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_123
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+esi*4+24]
	dec	dword [eax+4]
	jnz	_734
	push	eax
	call	_bbGCFree
	add	esp,4
_734:
	mov	eax,dword [ebp-8]
	mov	dword [eax+esi*4+24],ebx
	add	esi,1
_123:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_125
_124:
	push	_bb_Enrollment
	call	_bbObjectNew
	add	esp,4
	mov	esi,eax
	mov	eax,dword [_bb_eList]
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_740
	push	eax
	call	_bbGCFree
	add	esp,4
_740:
	mov	dword [esi+8],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_744
	push	eax
	call	_bbGCFree
	add	esp,4
_744:
	mov	dword [esi+12],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+8+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_748
	push	eax
	call	_bbGCFree
	add	esp,4
_748:
	mov	dword [esi+20],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+12+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_752
	push	eax
	call	_bbGCFree
	add	esp,4
_752:
	mov	dword [esi+24],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+16+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+28]
	dec	dword [eax+4]
	jnz	_756
	push	eax
	call	_bbGCFree
	add	esp,4
_756:
	mov	dword [esi+28],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+20+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_760
	push	eax
	call	_bbGCFree
	add	esp,4
_760:
	mov	dword [esi+32],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+24+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_764
	push	eax
	call	_bbGCFree
	add	esp,4
_764:
	mov	dword [esi+36],ebx
_120:
	push	dword [ebp+8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_122
_121:
	push	dword [ebp+8]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_289
_289:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadStaff:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
_128:
	push	4
	push	_765
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,0
	mov	edi,eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_129
_131:
	mov	eax,dword [ebp-4]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_129
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+esi*4+24]
	dec	dword [eax+4]
	jnz	_778
	push	eax
	call	_bbGCFree
	add	esp,4
_778:
	mov	eax,dword [ebp-8]
	mov	dword [eax+esi*4+24],ebx
	add	esi,1
_129:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_131
_130:
	push	_bb_Staff
	call	_bbObjectNew
	add	esp,4
	mov	esi,eax
	mov	eax,dword [_bb_staList]
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_784
	push	eax
	call	_bbGCFree
	add	esp,4
_784:
	mov	dword [esi+8],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_788
	push	eax
	call	_bbGCFree
	add	esp,4
_788:
	mov	dword [esi+12],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+8+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_792
	push	eax
	call	_bbGCFree
	add	esp,4
_792:
	mov	dword [esi+16],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+12+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_796
	push	eax
	call	_bbGCFree
	add	esp,4
_796:
	mov	dword [esi+20],ebx
_126:
	push	dword [ebp+8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_128
_127:
	push	dword [ebp+8]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_292
_292:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadStudent:
	push	ebp
	mov	ebp,esp
	sub	esp,8
	push	ebx
	push	esi
	push	edi
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
_134:
	push	6
	push	_797
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	dword [ebp+8]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	esi,0
	mov	edi,eax
	mov	eax,edi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-4],eax
	jmp	_135
_137:
	mov	eax,dword [ebp-4]
	push	_bbStringClass
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_135
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [ebp-8]
	mov	eax,dword [eax+esi*4+24]
	dec	dword [eax+4]
	jnz	_810
	push	eax
	call	_bbGCFree
	add	esp,4
_810:
	mov	eax,dword [ebp-8]
	mov	dword [eax+esi*4+24],ebx
	add	esi,1
_135:
	mov	eax,dword [ebp-4]
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_137
_136:
	push	_bb_Student
	call	_bbObjectNew
	add	esp,4
	mov	esi,eax
	mov	eax,dword [_bb_stuList]
	push	esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+68]
	add	esp,8
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_816
	push	eax
	call	_bbGCFree
	add	esp,4
_816:
	mov	dword [esi+8],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_820
	push	eax
	call	_bbGCFree
	add	esp,4
_820:
	mov	dword [esi+12],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+8+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_824
	push	eax
	call	_bbGCFree
	add	esp,4
_824:
	mov	dword [esi+16],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+12+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_828
	push	eax
	call	_bbGCFree
	add	esp,4
_828:
	mov	dword [esi+20],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+16+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_832
	push	eax
	call	_bbGCFree
	add	esp,4
_832:
	mov	dword [esi+24],ebx
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+20+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+28]
	dec	dword [eax+4]
	jnz	_836
	push	eax
	call	_bbGCFree
	add	esp,4
_836:
	mov	dword [esi+28],ebx
_132:
	push	dword [ebp+8]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_134
_133:
	push	dword [ebp+8]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	eax,0
	jmp	_295
_295:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_enrollInTraining:
	push	ebp
	mov	ebp,esp
	push	ebx
	push	esi
	push	edi
	mov	edi,dword [ebp+8]
	mov	esi,dword [_bb_stuList]
	mov	eax,esi
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_138
_140:
	mov	eax,ebx
	push	_bb_Student
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	cmp	eax,_bbNullObject
	je	_138
	push	_43
	push	dword [_bb_schoolYear]
	push	_78
	push	_34
	push	_78
	push	_47
	push	_78
	push	dword [eax+32]
	push	_843
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	edi
	call	_brl_stream_WriteLine
	add	esp,8
_138:
	mov	eax,ebx
	push	eax
	mov	eax,dword [eax]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_140
_139:
	mov	eax,0
	jmp	_298
_298:
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_315:
	dd	0
_155:
	db	"z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0",0
_156:
	db	"New",0
_157:
	db	"()i",0
_158:
	db	"Delete",0
	align	4
_154:
	dd	2
	dd	_155
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_22:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_154
	dd	8
	dd	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New
	dd	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_160:
	db	"z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
_161:
	db	"Name",0
_162:
	db	"$",0
	align	4
_163:
	dd	_bbStringClass
	dd	2147483646
	dd	10
	dw	73,110,116,101,103,114,97,116,111,114
_164:
	db	"MajorVersion",0
_165:
	db	"i",0
	align	4
_166:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	48
_167:
	db	"MinorVersion",0
	align	4
_168:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	49
_169:
	db	"Revision",0
_170:
	db	"VersionString",0
	align	4
_171:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	48,46,49,46,48
_172:
	db	"AssemblyInfo",0
	align	4
_173:
	dd	_bbStringClass
	dd	2147483646
	dd	16
	dw	73,110,116,101,103,114,97,116,111,114,32,48,46,49,46,48
_174:
	db	"Platform",0
	align	4
_175:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	87,105,110,51,50
_176:
	db	"Architecture",0
	align	4
_177:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	120,56,54
_178:
	db	"DebugOn",0
	align	4
_159:
	dd	2
	dd	_160
	dd	1
	dd	_161
	dd	_162
	dd	_163
	dd	1
	dd	_164
	dd	_165
	dd	_166
	dd	1
	dd	_167
	dd	_165
	dd	_168
	dd	1
	dd	_169
	dd	_165
	dd	_166
	dd	1
	dd	_170
	dd	_162
	dd	_171
	dd	1
	dd	_172
	dd	_162
	dd	_173
	dd	1
	dd	_174
	dd	_162
	dd	_175
	dd	1
	dd	_176
	dd	_162
	dd	_177
	dd	1
	dd	_178
	dd	_165
	dd	_166
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_23:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_159
	dd	8
	dd	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	dd	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	align	4
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Application:
	dd	_bbNullObject
	align	4
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Resources:
	dd	_bbNullObject
_180:
	db	"z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
	align	4
_179:
	dd	2
	dd	_180
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_29:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_179
	dd	8
	dd	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	dd	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_182:
	db	"Course",0
_183:
	db	"IN_sectionNumber",0
_184:
	db	"IN_sectionName",0
_185:
	db	"IN_startingGrade",0
_186:
	db	"IN_term",0
_187:
	db	"IN_period",0
_188:
	db	"BB_external_course_key",0
_189:
	db	"BB_course_id",0
_190:
	db	"BB_course_name",0
_191:
	db	"BB_master_course_key",0
_192:
	db	"BB_data_source_key",0
_193:
	db	"BB_Available_ind",0
	align	4
_181:
	dd	2
	dd	_182
	dd	3
	dd	_183
	dd	_162
	dd	8
	dd	3
	dd	_184
	dd	_162
	dd	12
	dd	3
	dd	_185
	dd	_162
	dd	16
	dd	3
	dd	_186
	dd	_162
	dd	20
	dd	3
	dd	_187
	dd	_162
	dd	24
	dd	3
	dd	_188
	dd	_162
	dd	28
	dd	3
	dd	_189
	dd	_162
	dd	32
	dd	3
	dd	_190
	dd	_162
	dd	36
	dd	3
	dd	_191
	dd	_162
	dd	40
	dd	3
	dd	_192
	dd	_162
	dd	44
	dd	3
	dd	_193
	dd	_162
	dd	48
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_bb_Course:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_181
	dd	52
	dd	__bb_Course_New
	dd	__bb_Course_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_195:
	db	"Enrollment",0
_196:
	db	"IN_section",0
_197:
	db	"IN_stateIDNumber",0
_198:
	db	"IN_LName",0
_199:
	db	"IN_FName",0
_200:
	db	"IN_TNum",0
_201:
	db	"IN_Teacher",0
_202:
	db	"IN_SchoolNumber",0
_203:
	db	"IN_AcademicSession",0
_204:
	db	"BB_external_person_key",0
_205:
	db	"BB_role",0
_206:
	db	"BB_available_ind",0
	align	4
_194:
	dd	2
	dd	_195
	dd	3
	dd	_196
	dd	_162
	dd	8
	dd	3
	dd	_197
	dd	_162
	dd	12
	dd	3
	dd	_198
	dd	_162
	dd	16
	dd	3
	dd	_199
	dd	_162
	dd	20
	dd	3
	dd	_200
	dd	_162
	dd	24
	dd	3
	dd	_201
	dd	_162
	dd	28
	dd	3
	dd	_202
	dd	_162
	dd	32
	dd	3
	dd	_203
	dd	_162
	dd	36
	dd	3
	dd	_188
	dd	_162
	dd	40
	dd	3
	dd	_204
	dd	_162
	dd	44
	dd	3
	dd	_205
	dd	_162
	dd	48
	dd	3
	dd	_206
	dd	_162
	dd	52
	dd	3
	dd	_192
	dd	_162
	dd	56
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_bb_Enrollment:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_194
	dd	60
	dd	__bb_Enrollment_New
	dd	__bb_Enrollment_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_208:
	db	"Staff",0
_209:
	db	"IN_referenceCode",0
_210:
	db	"IN_lastName",0
_211:
	db	"IN_firstName",0
_212:
	db	"IN_userName",0
_213:
	db	"BB_user_ID",0
_214:
	db	"BB_passwd",0
_215:
	db	"BB_firstName",0
_216:
	db	"BB_lastName",0
_217:
	db	"BB_email",0
_218:
	db	"BB_instution_role",0
_219:
	db	"BB_system_role",0
_220:
	db	"BB_row_status",0
	align	4
_207:
	dd	2
	dd	_208
	dd	3
	dd	_209
	dd	_162
	dd	8
	dd	3
	dd	_210
	dd	_162
	dd	12
	dd	3
	dd	_211
	dd	_162
	dd	16
	dd	3
	dd	_212
	dd	_162
	dd	20
	dd	3
	dd	_204
	dd	_162
	dd	24
	dd	3
	dd	_213
	dd	_162
	dd	28
	dd	3
	dd	_214
	dd	_162
	dd	32
	dd	3
	dd	_215
	dd	_162
	dd	36
	dd	3
	dd	_216
	dd	_162
	dd	40
	dd	3
	dd	_217
	dd	_162
	dd	44
	dd	3
	dd	_218
	dd	_162
	dd	48
	dd	3
	dd	_219
	dd	_162
	dd	52
	dd	3
	dd	_206
	dd	_162
	dd	56
	dd	3
	dd	_220
	dd	_162
	dd	60
	dd	3
	dd	_192
	dd	_162
	dd	64
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_bb_Staff:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_207
	dd	68
	dd	__bb_Staff_New
	dd	__bb_Staff_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_222:
	db	"Student",0
_223:
	db	"IN_middleName",0
_224:
	db	"IN_institution",0
_225:
	db	"IN_enrollmentStatus",0
_226:
	db	"BB_system_Role",0
	align	4
_221:
	dd	2
	dd	_222
	dd	3
	dd	_210
	dd	_162
	dd	8
	dd	3
	dd	_211
	dd	_162
	dd	12
	dd	3
	dd	_223
	dd	_162
	dd	16
	dd	3
	dd	_224
	dd	_162
	dd	20
	dd	3
	dd	_209
	dd	_162
	dd	24
	dd	3
	dd	_225
	dd	_162
	dd	28
	dd	3
	dd	_204
	dd	_162
	dd	32
	dd	3
	dd	_213
	dd	_162
	dd	36
	dd	3
	dd	_214
	dd	_162
	dd	40
	dd	3
	dd	_215
	dd	_162
	dd	44
	dd	3
	dd	_216
	dd	_162
	dd	48
	dd	3
	dd	_217
	dd	_162
	dd	52
	dd	3
	dd	_218
	dd	_162
	dd	56
	dd	3
	dd	_226
	dd	_162
	dd	60
	dd	3
	dd	_206
	dd	_162
	dd	64
	dd	3
	dd	_220
	dd	_162
	dd	68
	dd	3
	dd	_192
	dd	_162
	dd	72
	dd	6
	dd	_156
	dd	_157
	dd	16
	dd	6
	dd	_158
	dd	_157
	dd	20
	dd	0
	align	4
_bb_Student:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_221
	dd	76
	dd	__bb_Student_New
	dd	__bb_Student_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	align	4
_300:
	dd	_bbNullObject
	align	4
_30:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	67,58,92,66,66
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	100,97,116,97,92
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	100,97,116,97
	align	4
_33:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	49,52,49,53
	align	4
_bb_schoolYear:
	dd	_33
	align	4
_34:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	89
	align	4
_bb_teachersEnabled:
	dd	_34
	align	4
_bb_studentsEnabled:
	dd	_34
	align	4
_bb_enrollmentsEnabled:
	dd	_34
	align	4
_303:
	dd	0
	align	4
_bb_cList:
	dd	_bbNullObject
	align	4
_bb_eList:
	dd	_bbNullObject
	align	4
_bb_staList:
	dd	_bbNullObject
	align	4
_bb_stuList:
	dd	_bbNullObject
	align	4
_bb_excludeList:
	dd	_bbNullObject
	align	4
_35:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	52,56,48,48,55,51
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	52,48,48,48,50,54
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	50,50,48,48,49,49,97,97
	align	4
_38:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	50,50,48,48,56,49,97,98
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	50,50,48,48,53,49,97,97
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	50,53,48,48,48,50
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	50,48,48,48,49,55
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	56,48,50,49,48,52,97,98
	align	4
_48:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	76,111,97,100,105,110,103,32,67,111,117,114,115,101,115
	align	4
_49:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	84,97,108,108,97,115,115,101,101,67,111,117,114,115,101,46
	dw	99,115,118
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	76,111,97,100,105,110,103,32,69,110,114,111,108,108,109,101
	dw	110,116,115
	align	4
_51:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	84,97,108,108,97,115,115,101,101,69,110,114,111,108,108,109
	dw	101,110,116,46,99,115,118
	align	4
_52:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	76,111,97,100,105,110,103,32,83,116,97,102,102
	align	4
_53:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	84,97,108,108,97,115,115,101,101,83,116,97,102,102,46,99
	dw	115,118
	align	4
_54:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	76,111,97,100,105,110,103,32,83,116,117,100,101,110,116,115
	align	4
_55:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	84,97,108,108,97,115,115,101,101,83,116,117,100,101,110,116
	dw	46,99,115,118
	align	4
_56:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	67,111,110,118,101,114,116,105,110,103,32,67,111,117,114,115
	dw	101,115
	align	4
_57:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	67,111,110,118,101,114,116,105,110,103,32,85,115,101,114,115
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	22
	dw	67,111,110,118,101,114,116,105,110,103,32,69,110,114,111,108
	dw	108,109,101,110,116,115
	align	4
_63:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	32,109,111,114,101,32,115,101,99,111,110,100,115,46
	align	4
_62:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	80,108,101,97,115,101,32,119,97,105,116,32
	align	4
_64:
	dd	_bbStringClass
	dd	2147483647
	dd	15
	dw	83,101,110,100,105,110,103,32,67,111,117,114,115,101,115
	align	4
_65:
	dd	_bbStringClass
	dd	2147483647
	dd	13
	dw	83,101,110,100,105,110,103,32,85,115,101,114,115
	align	4
_66:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	83,101,110,100,105,110,103,32,69,110,114,111,108,108,109,101
	dw	110,116,115
	align	4
_67:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	68,111,110,101
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	95,67,111,117,114,115,101,115
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	102,97,99,117,108,116,121
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	78,111,110,101
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	95,85,115,101,114,115
	align	4
_47:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	115,116,117,100,101,110,116
	align	4
_68:
	dd	_bbStringClass
	dd	2147483647
	dd	22
	dw	67,58,92,66,66,92,68,97,116,97,92,99,111,109,109,97
	dw	110,100,46,98,97,116
	align	4
_69:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	100,97,116,97,92,67,76,65,83,83,69,83,46,116,120,116
	align	4
_70:
	dd	_bbStringClass
	dd	2147483647
	dd	89
	dw	101,120,116,101,114,110,97,108,95,99,111,117,114,115,101,95
	dw	107,101,121,124,99,111,117,114,115,101,95,105,100,124,99,111
	dw	117,114,115,101,95,110,97,109,101,124,109,97,115,116,101,114
	dw	95,99,111,117,114,115,101,95,107,101,121,124,100,97,116,97
	dw	95,115,111,117,114,99,101,95,107,101,121,124,65,118,97,105
	dw	108,97,98,108,101,95,105,110,100
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	46,112,97,114,101,110,116
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	124
	align	4
_79:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	45,80
	align	4
_80:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	100,97,116,97,92,69,78,82,79,76,76,77,69,78,84,46
	dw	116,120,116
	align	4
_81:
	dd	_bbStringClass
	dd	2147483647
	dd	74
	dw	101,120,116,101,114,110,97,108,95,99,111,117,114,115,101,95
	dw	107,101,121,124,101,120,116,101,114,110,97,108,95,112,101,114
	dw	115,111,110,95,107,101,121,124,114,111,108,101,124,97,118,97
	dw	105,108,97,98,108,101,95,105,110,100,124,100,97,116,97,95
	dw	115,111,117,114,99,101,95,107,101,121
	align	4
_88:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,110,115,116,114,117,99,116,111,114
	align	4
_26:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	32
	align	4
_95:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	45
	align	4
_96:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	40
	align	4
_97:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	41
	align	4
_98:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	100,97,116,97,92,85,83,69,82,83,46,116,120,116
	align	4
_99:
	dd	_bbStringClass
	dd	2147483647
	dd	129
	dw	69,120,116,101,114,110,97,108,95,80,101,114,115,111,110,95
	dw	75,101,121,124,85,115,101,114,95,73,68,124,80,97,115,115
	dw	119,100,124,70,105,114,115,116,110,97,109,101,124,76,97,115
	dw	116,110,97,109,101,124,69,109,97,105,108,124,73,110,115,116
	dw	105,116,117,116,105,111,110,95,82,111,108,101,124,83,121,115
	dw	116,101,109,95,82,111,108,101,124,65,118,97,105,108,97,98
	dw	108,101,95,105,110,100,124,114,111,119,95,115,116,97,116,117
	dw	115,124,68,97,116,97,95,83,111,117,114,99,101,95,75,101
	dw	121
	align	4
_25:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	46
	align	4
_103:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	49,50,51,52,53
	align	4
_104:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	101,110,97,98,108,101,100
	align	4
_108:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	64,116,99,115,116,117,100,101,110,116,46,99,111,109
	align	4
_109:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	67,117,114,114,101,110,116,108,121,69,110,114,111,108,108,101
	dw	100
	align	4
_110:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	100,105,115,97,98,108,101,100
_685:
	db	"$",0
_721:
	db	"$",0
_765:
	db	"$",0
_797:
	db	"$",0
	align	4
_843:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	115,116,117,105,116,124
