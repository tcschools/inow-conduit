	format	MS COFF
	extrn	___bb_appstub_appstub
	extrn	___bb_audio_audio
	extrn	___bb_bank_bank
	extrn	___bb_bankstream_bankstream
	extrn	___bb_basic_basic
	extrn	___bb_blitz_blitz
	extrn	___bb_bmploader_bmploader
	extrn	___bb_d3d7max2d_d3d7max2d
	extrn	___bb_d3d9max2d_d3d9max2d
	extrn	___bb_data_data
	extrn	___bb_directsoundaudio_directsoundaudio
	extrn	___bb_eventqueue_eventqueue
	extrn	___bb_freeaudioaudio_freeaudioaudio
	extrn	___bb_freejoy_freejoy
	extrn	___bb_freeprocess_freeprocess
	extrn	___bb_freetypefont_freetypefont
	extrn	___bb_glew_glew
	extrn	___bb_gnet_gnet
	extrn	___bb_jpgloader_jpgloader
	extrn	___bb_macos_macos
	extrn	___bb_map_map
	extrn	___bb_maxlua_maxlua
	extrn	___bb_maxutil_maxutil
	extrn	___bb_oggloader_oggloader
	extrn	___bb_openalaudio_openalaudio
	extrn	___bb_pngloader_pngloader
	extrn	___bb_retro_retro
	extrn	___bb_tgaloader_tgaloader
	extrn	___bb_threads_threads
	extrn	___bb_timer_timer
	extrn	___bb_wavloader_wavloader
	extrn	_bbArrayNew1D
	extrn	_bbEmptyArray
	extrn	_bbEmptyString
	extrn	_bbEnd
	extrn	_bbGCFree
	extrn	_bbNullObject
	extrn	_bbObjectClass
	extrn	_bbObjectCompare
	extrn	_bbObjectCtor
	extrn	_bbObjectDowncast
	extrn	_bbObjectFree
	extrn	_bbObjectNew
	extrn	_bbObjectRegisterType
	extrn	_bbObjectReserved
	extrn	_bbObjectSendMessage
	extrn	_bbObjectToString
	extrn	_bbOnDebugEnterScope
	extrn	_bbOnDebugEnterStm
	extrn	_bbOnDebugLeaveScope
	extrn	_bbStringClass
	extrn	_bbStringCompare
	extrn	_bbStringConcat
	extrn	_brl_blitz_ArrayBoundsError
	extrn	_brl_blitz_NullObjectError
	extrn	_brl_filesystem_ChangeDir
	extrn	_brl_filesystem_CloseFile
	extrn	_brl_filesystem_CreateDir
	extrn	_brl_filesystem_DeleteFile
	extrn	_brl_filesystem_FileType
	extrn	_brl_filesystem_ReadFile
	extrn	_brl_filesystem_WriteFile
	extrn	_brl_linkedlist_ListAddLast
	extrn	_brl_linkedlist_TList
	extrn	_brl_retro_Left
	extrn	_brl_retro_Right
	extrn	_brl_retro_Trim
	extrn	_brl_stream_Eof
	extrn	_brl_stream_ReadLine
	extrn	_brl_stream_WriteLine
	public	__bb_Course_Delete
	public	__bb_Course_New
	public	__bb_Enrollment_Delete
	public	__bb_Enrollment_New
	public	__bb_Staff_Delete
	public	__bb_Staff_New
	public	__bb_Student_Delete
	public	__bb_Student_New
	public	__bb_main
	public	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete
	public	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Application
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	public	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Resources
	public	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	public	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	public	_bb_Course
	public	_bb_Enrollment
	public	_bb_Staff
	public	_bb_Student
	public	_bb_cList
	public	_bb_convertFromCSV
	public	_bb_eList
	public	_bb_loadCourse
	public	_bb_loadEnrollment
	public	_bb_loadStaff
	public	_bb_loadStudent
	public	_bb_saveCourse
	public	_bb_saveEnrollment
	public	_bb_saveUsers
	public	_bb_schoolYear
	public	_bb_staList
	public	_bb_stuList
	section	"code" code
__bb_main:
	push	ebp
	mov	ebp,esp
	push	ebx
	cmp	dword [_300],0
	je	_301
	mov	eax,0
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_301:
	mov	dword [_300],1
	push	ebp
	push	_290
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	___bb_blitz_blitz
	call	___bb_appstub_appstub
	call	___bb_audio_audio
	call	___bb_bank_bank
	call	___bb_bankstream_bankstream
	call	___bb_basic_basic
	call	___bb_bmploader_bmploader
	call	___bb_d3d7max2d_d3d7max2d
	call	___bb_d3d9max2d_d3d9max2d
	call	___bb_data_data
	call	___bb_directsoundaudio_directsoundaudio
	call	___bb_eventqueue_eventqueue
	call	___bb_freeaudioaudio_freeaudioaudio
	call	___bb_freetypefont_freetypefont
	call	___bb_gnet_gnet
	call	___bb_jpgloader_jpgloader
	call	___bb_map_map
	call	___bb_maxlua_maxlua
	call	___bb_maxutil_maxutil
	call	___bb_oggloader_oggloader
	call	___bb_openalaudio_openalaudio
	call	___bb_pngloader_pngloader
	call	___bb_retro_retro
	call	___bb_tgaloader_tgaloader
	call	___bb_threads_threads
	call	___bb_timer_timer
	call	___bb_wavloader_wavloader
	call	___bb_freejoy_freejoy
	call	___bb_freeprocess_freeprocess
	call	___bb_glew_glew
	call	___bb_macos_macos
	push	_22
	call	_bbObjectRegisterType
	add	esp,4
	push	_23
	call	_bbObjectRegisterType
	add	esp,4
	push	_257
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_29
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Course
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Enrollment
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Staff
	call	_bbObjectRegisterType
	add	esp,4
	push	_bb_Student
	call	_bbObjectRegisterType
	add	esp,4
	push	_260
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_262
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_30
	call	_brl_filesystem_ChangeDir
	add	esp,4
	push	_264
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_31
	call	_brl_filesystem_FileType
	add	esp,4
	cmp	eax,0
	jne	_265
	push	ebp
	push	_267
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_266
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	0
	push	_32
	call	_brl_filesystem_CreateDir
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_265:
	push	_268
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_269
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_271]
	and	eax,1
	cmp	eax,0
	jne	_272
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_cList],eax
	or	dword [_271],1
_272:
	push	_273
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_271]
	and	eax,2
	cmp	eax,0
	jne	_275
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_eList],eax
	or	dword [_271],2
_275:
	push	_276
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_271]
	and	eax,4
	cmp	eax,0
	jne	_278
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_staList],eax
	or	dword [_271],4
_278:
	push	_279
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [_271]
	and	eax,8
	cmp	eax,0
	jne	_281
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	inc	dword [eax+4]
	mov	dword [_bb_stuList],eax
	or	dword [_271],8
_281:
	push	_282
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_41
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadCourse
	add	esp,4
	push	_283
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_42
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadEnrollment
	add	esp,4
	push	_284
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_43
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadStaff
	add	esp,4
	push	_285
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_44
	call	_brl_filesystem_ReadFile
	add	esp,4
	push	eax
	call	_bb_loadStudent
	add	esp,4
	push	_286
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_saveCourse
	push	_287
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_saveEnrollment
	push	_288
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bb_saveUsers
	push	_289
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	_bbEnd
	mov	ebx,0
	jmp	_192
_192:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_303
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_22
	push	ebp
	push	_302
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_195
_195:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete:
	push	ebp
	mov	ebp,esp
_198:
	mov	eax,0
	jmp	_306
_306:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_308
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_23
	push	ebp
	push	_307
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_201
_201:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete:
	push	ebp
	mov	ebp,esp
_204:
	mov	eax,0
	jmp	_310
_310:
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_312
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_29
	push	ebp
	push	_311
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_207
_207:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete:
	push	ebp
	mov	ebp,esp
_210:
	mov	eax,0
	jmp	_313
_313:
	mov	esp,ebp
	pop	ebp
	ret
__bb_Course_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_326
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Course
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	push	_34
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+44],eax
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	push	ebp
	push	_325
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_213
_213:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Course_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_216:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_330
	push	eax
	call	_bbGCFree
	add	esp,4
_330:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_332
	push	eax
	call	_bbGCFree
	add	esp,4
_332:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_334
	push	eax
	call	_bbGCFree
	add	esp,4
_334:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_336
	push	eax
	call	_bbGCFree
	add	esp,4
_336:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_338
	push	eax
	call	_bbGCFree
	add	esp,4
_338:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_340
	push	eax
	call	_bbGCFree
	add	esp,4
_340:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_342
	push	eax
	call	_bbGCFree
	add	esp,4
_342:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_344
	push	eax
	call	_bbGCFree
	add	esp,4
_344:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_346
	push	eax
	call	_bbGCFree
	add	esp,4
_346:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_348
	push	eax
	call	_bbGCFree
	add	esp,4
_348:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_350
	push	eax
	call	_bbGCFree
	add	esp,4
_350:
	mov	eax,0
	jmp	_328
_328:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Enrollment_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_365
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Enrollment
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],edx
	push	_35
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+56],eax
	push	ebp
	push	_364
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_219
_219:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Enrollment_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_222:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_369
	push	eax
	call	_bbGCFree
	add	esp,4
_369:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_371
	push	eax
	call	_bbGCFree
	add	esp,4
_371:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_373
	push	eax
	call	_bbGCFree
	add	esp,4
_373:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_375
	push	eax
	call	_bbGCFree
	add	esp,4
_375:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_377
	push	eax
	call	_bbGCFree
	add	esp,4
_377:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_379
	push	eax
	call	_bbGCFree
	add	esp,4
_379:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_381
	push	eax
	call	_bbGCFree
	add	esp,4
_381:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_383
	push	eax
	call	_bbGCFree
	add	esp,4
_383:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_385
	push	eax
	call	_bbGCFree
	add	esp,4
_385:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_387
	push	eax
	call	_bbGCFree
	add	esp,4
_387:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_389
	push	eax
	call	_bbGCFree
	add	esp,4
_389:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_391
	push	eax
	call	_bbGCFree
	add	esp,4
_391:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_393
	push	eax
	call	_bbGCFree
	add	esp,4
_393:
	mov	eax,0
	jmp	_367
_367:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Staff_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_410
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Staff
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_36
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	mov	edx,_37
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],edx
	push	_38
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+64],eax
	push	ebp
	push	_409
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_225
_225:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Staff_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_228:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_414
	push	eax
	call	_bbGCFree
	add	esp,4
_414:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_416
	push	eax
	call	_bbGCFree
	add	esp,4
_416:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_418
	push	eax
	call	_bbGCFree
	add	esp,4
_418:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_420
	push	eax
	call	_bbGCFree
	add	esp,4
_420:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_422
	push	eax
	call	_bbGCFree
	add	esp,4
_422:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_424
	push	eax
	call	_bbGCFree
	add	esp,4
_424:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_426
	push	eax
	call	_bbGCFree
	add	esp,4
_426:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_428
	push	eax
	call	_bbGCFree
	add	esp,4
_428:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_430
	push	eax
	call	_bbGCFree
	add	esp,4
_430:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_432
	push	eax
	call	_bbGCFree
	add	esp,4
_432:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_434
	push	eax
	call	_bbGCFree
	add	esp,4
_434:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_436
	push	eax
	call	_bbGCFree
	add	esp,4
_436:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_438
	push	eax
	call	_bbGCFree
	add	esp,4
_438:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_440
	push	eax
	call	_bbGCFree
	add	esp,4
_440:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_442
	push	eax
	call	_bbGCFree
	add	esp,4
_442:
	mov	eax,0
	jmp	_412
_412:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_New:
	push	ebp
	mov	ebp,esp
	sub	esp,4
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	push	ebp
	push	_461
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	dword [ebp-4]
	call	_bbObjectCtor
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	dword [eax],_bb_Student
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+8],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+12],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+16],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+20],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+24],edx
	mov	edx,_1
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+28],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+32],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+36],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+40],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+44],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+48],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+52],edx
	mov	edx,_39
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+56],edx
	mov	edx,_37
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+60],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+64],edx
	mov	edx,_bbEmptyString
	inc	dword [edx+4]
	mov	eax,dword [ebp-4]
	mov	dword [eax+68],edx
	push	_40
	push	dword [_bb_schoolYear]
	call	_bbStringConcat
	add	esp,8
	inc	dword [eax+4]
	mov	edx,dword [ebp-4]
	mov	dword [edx+72],eax
	push	ebp
	push	_460
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
	mov	ebx,0
	jmp	_231
_231:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
__bb_Student_Delete:
	push	ebp
	mov	ebp,esp
	push	ebx
	mov	ebx,dword [ebp+8]
_234:
	mov	eax,dword [ebx+72]
	dec	dword [eax+4]
	jnz	_465
	push	eax
	call	_bbGCFree
	add	esp,4
_465:
	mov	eax,dword [ebx+68]
	dec	dword [eax+4]
	jnz	_467
	push	eax
	call	_bbGCFree
	add	esp,4
_467:
	mov	eax,dword [ebx+64]
	dec	dword [eax+4]
	jnz	_469
	push	eax
	call	_bbGCFree
	add	esp,4
_469:
	mov	eax,dword [ebx+60]
	dec	dword [eax+4]
	jnz	_471
	push	eax
	call	_bbGCFree
	add	esp,4
_471:
	mov	eax,dword [ebx+56]
	dec	dword [eax+4]
	jnz	_473
	push	eax
	call	_bbGCFree
	add	esp,4
_473:
	mov	eax,dword [ebx+52]
	dec	dword [eax+4]
	jnz	_475
	push	eax
	call	_bbGCFree
	add	esp,4
_475:
	mov	eax,dword [ebx+48]
	dec	dword [eax+4]
	jnz	_477
	push	eax
	call	_bbGCFree
	add	esp,4
_477:
	mov	eax,dword [ebx+44]
	dec	dword [eax+4]
	jnz	_479
	push	eax
	call	_bbGCFree
	add	esp,4
_479:
	mov	eax,dword [ebx+40]
	dec	dword [eax+4]
	jnz	_481
	push	eax
	call	_bbGCFree
	add	esp,4
_481:
	mov	eax,dword [ebx+36]
	dec	dword [eax+4]
	jnz	_483
	push	eax
	call	_bbGCFree
	add	esp,4
_483:
	mov	eax,dword [ebx+32]
	dec	dword [eax+4]
	jnz	_485
	push	eax
	call	_bbGCFree
	add	esp,4
_485:
	mov	eax,dword [ebx+28]
	dec	dword [eax+4]
	jnz	_487
	push	eax
	call	_bbGCFree
	add	esp,4
_487:
	mov	eax,dword [ebx+24]
	dec	dword [eax+4]
	jnz	_489
	push	eax
	call	_bbGCFree
	add	esp,4
_489:
	mov	eax,dword [ebx+20]
	dec	dword [eax+4]
	jnz	_491
	push	eax
	call	_bbGCFree
	add	esp,4
_491:
	mov	eax,dword [ebx+16]
	dec	dword [eax+4]
	jnz	_493
	push	eax
	call	_bbGCFree
	add	esp,4
_493:
	mov	eax,dword [ebx+12]
	dec	dword [eax+4]
	jnz	_495
	push	eax
	call	_bbGCFree
	add	esp,4
_495:
	mov	eax,dword [ebx+8]
	dec	dword [eax+4]
	jnz	_497
	push	eax
	call	_bbGCFree
	add	esp,4
_497:
	mov	eax,0
	jmp	_463
_463:
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveCourse:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_570
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_498
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_45
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_499
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_45
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-4],eax
	push	_501
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_46
	push	dword [ebp-4]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_502
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	eax,dword [_bb_cList]
	mov	dword [ebp-24],eax
	mov	ebx,dword [ebp-24]
	cmp	ebx,_bbNullObject
	jne	_506
	call	_brl_blitz_NullObjectError
_506:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-28],eax
	jmp	_47
_49:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_511
	call	_brl_blitz_NullObjectError
_511:
	push	_bb_Course
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_47
	mov	eax,ebp
	push	eax
	push	_567
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_512
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_514
	call	_brl_blitz_NullObjectError
_514:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_517
	call	_brl_blitz_NullObjectError
_517:
	mov	ebx,dword [ebx+8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_521
	push	eax
	call	_bbGCFree
	add	esp,4
_521:
	mov	dword [esi+32],ebx
	push	_522
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_524
	call	_brl_blitz_NullObjectError
_524:
	mov	edi,ebx
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_527
	call	_brl_blitz_NullObjectError
_527:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_529
	call	_brl_blitz_NullObjectError
_529:
	push	dword [ebx+24]
	push	_50
	push	dword [esi+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+36]
	dec	dword [eax+4]
	jnz	_533
	push	eax
	call	_bbGCFree
	add	esp,4
_533:
	mov	dword [edi+36],ebx
	push	_534
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_536
	call	_brl_blitz_NullObjectError
_536:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_539
	call	_brl_blitz_NullObjectError
_539:
	mov	ebx,dword [ebx+8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+28]
	dec	dword [eax+4]
	jnz	_543
	push	eax
	call	_bbGCFree
	add	esp,4
_543:
	mov	dword [esi+28],ebx
	push	_544
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_546
	call	_brl_blitz_NullObjectError
_546:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_549
	call	_brl_blitz_NullObjectError
_549:
	mov	ebx,dword [ebx+12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_553
	push	eax
	call	_bbGCFree
	add	esp,4
_553:
	mov	dword [esi+40],ebx
	push	_554
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_556
	call	_brl_blitz_NullObjectError
_556:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_558
	call	_brl_blitz_NullObjectError
_558:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	jne	_560
	call	_brl_blitz_NullObjectError
_560:
	mov	edi,dword [ebp-8]
	cmp	edi,_bbNullObject
	jne	_562
	call	_brl_blitz_NullObjectError
_562:
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_564
	call	_brl_blitz_NullObjectError
_564:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_566
	call	_brl_blitz_NullObjectError
_566:
	push	dword [ebx+48]
	push	_51
	push	dword [esi+44]
	push	_51
	push	dword [edi+40]
	push	_51
	mov	eax,dword [ebp-12]
	push	dword [eax+36]
	push	_51
	mov	eax,dword [ebp-16]
	push	dword [eax+32]
	push	_51
	mov	eax,dword [ebp-20]
	push	dword [eax+28]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-4]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_47:
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_509
	call	_brl_blitz_NullObjectError
_509:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_49
_48:
	push	_569
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_236
_236:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveEnrollment:
	push	ebp
	mov	ebp,esp
	sub	esp,48
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],0
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-28],_bbEmptyString
	mov	eax,ebp
	push	eax
	push	_689
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_574
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_52
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_575
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-4],eax
	push	_577
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_52
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-8],eax
	push	_579
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_53
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_580
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	eax,dword [_bb_eList]
	mov	dword [ebp-44],eax
	mov	ebx,dword [ebp-44]
	cmp	ebx,_bbNullObject
	jne	_584
	call	_brl_blitz_NullObjectError
_584:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-48],eax
	jmp	_54
_56:
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_589
	call	_brl_blitz_NullObjectError
_589:
	push	_bb_Enrollment
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_54
	mov	eax,ebp
	push	eax
	push	_672
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_590
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_592
	call	_brl_blitz_NullObjectError
_592:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_595
	call	_brl_blitz_NullObjectError
_595:
	mov	ebx,dword [ebx+8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_599
	push	eax
	call	_bbGCFree
	add	esp,4
_599:
	mov	dword [esi+40],ebx
	push	_600
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_602
	call	_brl_blitz_NullObjectError
_602:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_605
	call	_brl_blitz_NullObjectError
_605:
	mov	ebx,dword [ebx+12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+44]
	dec	dword [eax+4]
	jnz	_609
	push	eax
	call	_bbGCFree
	add	esp,4
_609:
	mov	dword [esi+44],ebx
	push	_610
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_612
	call	_brl_blitz_NullObjectError
_612:
	mov	ebx,_39
	inc	dword [ebx+4]
	mov	eax,dword [esi+48]
	dec	dword [eax+4]
	jnz	_617
	push	eax
	call	_bbGCFree
	add	esp,4
_617:
	mov	dword [esi+48],ebx
	push	_618
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_620
	call	_brl_blitz_NullObjectError
_620:
	mov	ebx,_57
	inc	dword [ebx+4]
	mov	eax,dword [esi+52]
	dec	dword [eax+4]
	jnz	_625
	push	eax
	call	_bbGCFree
	add	esp,4
_625:
	mov	dword [esi+52],ebx
	push	_626
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-40],eax
	cmp	dword [ebp-40],_bbNullObject
	jne	_628
	call	_brl_blitz_NullObjectError
_628:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_630
	call	_brl_blitz_NullObjectError
_630:
	mov	edi,dword [ebp-12]
	cmp	edi,_bbNullObject
	jne	_632
	call	_brl_blitz_NullObjectError
_632:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_634
	call	_brl_blitz_NullObjectError
_634:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_636
	call	_brl_blitz_NullObjectError
_636:
	push	_58
	push	dword [ebx+56]
	push	_51
	push	dword [esi+52]
	push	_51
	push	dword [edi+48]
	push	_51
	mov	eax,dword [ebp-36]
	push	dword [eax+44]
	push	_51
	mov	eax,dword [ebp-40]
	push	dword [eax+40]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_637
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_639
	call	_brl_blitz_NullObjectError
_639:
	mov	edi,dword [ebp-12]
	cmp	edi,_bbNullObject
	jne	_641
	call	_brl_blitz_NullObjectError
_641:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_643
	call	_brl_blitz_NullObjectError
_643:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_645
	call	_brl_blitz_NullObjectError
_645:
	push	_60
	push	dword [ebx+56]
	push	_51
	push	dword [esi+52]
	push	_51
	push	_59
	push	_51
	push	dword [edi+24]
	push	_51
	mov	eax,dword [ebp-32]
	push	dword [eax+40]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-16],eax
	push	_647
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],0
	push	_649
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_653
	call	_brl_blitz_NullObjectError
_653:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_61
_63:
	cmp	ebx,_bbNullObject
	jne	_658
	call	_brl_blitz_NullObjectError
_658:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_61
	mov	eax,ebp
	push	eax
	push	_664
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_659
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-24]
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_660
	mov	eax,ebp
	push	eax
	push	_663
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_661
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-20],1
	push	_662
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	call	dword [_bbOnDebugLeaveScope]
	call	dword [_bbOnDebugLeaveScope]
	jmp	_62
_660:
	call	dword [_bbOnDebugLeaveScope]
_61:
	cmp	ebx,_bbNullObject
	jne	_656
	call	_brl_blitz_NullObjectError
_656:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_63
_62:
	push	_666
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	cmp	dword [ebp-20],0
	jne	_667
	mov	eax,ebp
	push	eax
	push	_671
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_668
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-4]
	cmp	ebx,_bbNullObject
	jne	_670
	call	_brl_blitz_NullObjectError
_670:
	push	dword [ebp-16]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_667:
	call	dword [_bbOnDebugLeaveScope]
_54:
	mov	ebx,dword [ebp-48]
	cmp	ebx,_bbNullObject
	jne	_587
	call	_brl_blitz_NullObjectError
_587:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_56
_55:
	push	_676
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-28],_bbEmptyString
	mov	esi,dword [ebp-4]
	cmp	esi,_bbNullObject
	jne	_680
	call	_brl_blitz_NullObjectError
_680:
	push	esi
	mov	eax,dword [esi]
	call	dword [eax+140]
	add	esp,4
	mov	ebx,eax
	jmp	_64
_66:
	cmp	ebx,_bbNullObject
	jne	_685
	call	_brl_blitz_NullObjectError
_685:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	je	_64
	mov	eax,ebp
	push	eax
	push	_687
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_686
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-28]
	push	dword [ebp-8]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_64:
	cmp	ebx,_bbNullObject
	jne	_683
	call	_brl_blitz_NullObjectError
_683:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_66
_65:
	push	_688
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-8]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_238
_238:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_saveUsers:
	push	ebp
	mov	ebp,esp
	sub	esp,92
	push	ebx
	push	esi
	push	edi
	mov	dword [ebp-4],_bbNullObject
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_936
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_692
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_67
	call	_brl_filesystem_DeleteFile
	add	esp,4
	push	_693
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_67
	call	_brl_filesystem_WriteFile
	add	esp,4
	mov	dword [ebp-4],eax
	push	_695
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_68
	push	dword [ebp-4]
	call	_brl_stream_WriteLine
	add	esp,8
	push	_696
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-8],_bbNullObject
	mov	eax,dword [_bb_staList]
	mov	dword [ebp-80],eax
	mov	ebx,dword [ebp-80]
	cmp	ebx,_bbNullObject
	jne	_700
	call	_brl_blitz_NullObjectError
_700:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-88],eax
	jmp	_69
_71:
	mov	ebx,dword [ebp-88]
	cmp	ebx,_bbNullObject
	jne	_705
	call	_brl_blitz_NullObjectError
_705:
	push	_bb_Staff
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-8],eax
	cmp	dword [ebp-8],_bbNullObject
	je	_69
	mov	eax,ebp
	push	eax
	push	_805
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_706
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_708
	call	_brl_blitz_NullObjectError
_708:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_711
	call	_brl_blitz_NullObjectError
_711:
	mov	ebx,dword [ebx+8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_715
	push	eax
	call	_bbGCFree
	add	esp,4
_715:
	mov	dword [esi+24],ebx
	push	_716
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_718
	call	_brl_blitz_NullObjectError
_718:
	mov	edi,ebx
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_721
	call	_brl_blitz_NullObjectError
_721:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_723
	call	_brl_blitz_NullObjectError
_723:
	push	dword [ebx+12]
	push	_25
	push	dword [esi+16]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+28]
	dec	dword [eax+4]
	jnz	_727
	push	eax
	call	_bbGCFree
	add	esp,4
_727:
	mov	dword [edi+28],ebx
	push	_728
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_730
	call	_brl_blitz_NullObjectError
_730:
	mov	ebx,_72
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_735
	push	eax
	call	_bbGCFree
	add	esp,4
_735:
	mov	dword [esi+32],ebx
	push	_736
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_738
	call	_brl_blitz_NullObjectError
_738:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_741
	call	_brl_blitz_NullObjectError
_741:
	mov	ebx,dword [ebx+16]
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_745
	push	eax
	call	_bbGCFree
	add	esp,4
_745:
	mov	dword [esi+36],ebx
	push	_746
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_748
	call	_brl_blitz_NullObjectError
_748:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_751
	call	_brl_blitz_NullObjectError
_751:
	mov	ebx,dword [ebx+12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_755
	push	eax
	call	_bbGCFree
	add	esp,4
_755:
	mov	dword [esi+40],ebx
	push	_756
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_758
	call	_brl_blitz_NullObjectError
_758:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_761
	call	_brl_blitz_NullObjectError
_761:
	mov	ebx,dword [ebx+20]
	inc	dword [ebx+4]
	mov	eax,dword [esi+44]
	dec	dword [eax+4]
	jnz	_765
	push	eax
	call	_bbGCFree
	add	esp,4
_765:
	mov	dword [esi+44],ebx
	push	_766
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_768
	call	_brl_blitz_NullObjectError
_768:
	mov	ebx,_57
	inc	dword [ebx+4]
	mov	eax,dword [esi+56]
	dec	dword [eax+4]
	jnz	_773
	push	eax
	call	_bbGCFree
	add	esp,4
_773:
	mov	dword [esi+56],ebx
	push	_774
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_776
	call	_brl_blitz_NullObjectError
_776:
	mov	ebx,_73
	inc	dword [ebx+4]
	mov	eax,dword [esi+60]
	dec	dword [eax+4]
	jnz	_781
	push	eax
	call	_bbGCFree
	add	esp,4
_781:
	mov	dword [esi+60],ebx
	push	_782
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-8]
	mov	dword [ebp-76],eax
	cmp	dword [ebp-76],_bbNullObject
	jne	_784
	call	_brl_blitz_NullObjectError
_784:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-64],eax
	cmp	dword [ebp-64],_bbNullObject
	jne	_786
	call	_brl_blitz_NullObjectError
_786:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-56],eax
	cmp	dword [ebp-56],_bbNullObject
	jne	_788
	call	_brl_blitz_NullObjectError
_788:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-48],eax
	cmp	dword [ebp-48],_bbNullObject
	jne	_790
	call	_brl_blitz_NullObjectError
_790:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-40],eax
	cmp	dword [ebp-40],_bbNullObject
	jne	_792
	call	_brl_blitz_NullObjectError
_792:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-32],eax
	cmp	dword [ebp-32],_bbNullObject
	jne	_794
	call	_brl_blitz_NullObjectError
_794:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-28],eax
	cmp	dword [ebp-28],_bbNullObject
	jne	_796
	call	_brl_blitz_NullObjectError
_796:
	mov	eax,dword [ebp-8]
	mov	dword [ebp-20],eax
	cmp	dword [ebp-20],_bbNullObject
	jne	_798
	call	_brl_blitz_NullObjectError
_798:
	mov	edi,dword [ebp-8]
	cmp	edi,_bbNullObject
	jne	_800
	call	_brl_blitz_NullObjectError
_800:
	mov	esi,dword [ebp-8]
	cmp	esi,_bbNullObject
	jne	_802
	call	_brl_blitz_NullObjectError
_802:
	mov	ebx,dword [ebp-8]
	cmp	ebx,_bbNullObject
	jne	_804
	call	_brl_blitz_NullObjectError
_804:
	push	dword [ebx+64]
	push	_51
	push	dword [esi+60]
	push	_51
	push	dword [edi+56]
	push	_51
	mov	eax,dword [ebp-20]
	push	dword [eax+52]
	push	_51
	mov	eax,dword [ebp-28]
	push	dword [eax+48]
	push	_51
	mov	eax,dword [ebp-32]
	push	dword [eax+44]
	push	_51
	mov	eax,dword [ebp-40]
	push	dword [eax+40]
	push	_51
	mov	eax,dword [ebp-48]
	push	dword [eax+36]
	push	_51
	mov	eax,dword [ebp-56]
	push	dword [eax+32]
	push	_51
	mov	eax,dword [ebp-64]
	push	dword [eax+28]
	push	_51
	mov	eax,dword [ebp-76]
	push	dword [eax+24]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-4]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_69:
	mov	ebx,dword [ebp-88]
	cmp	ebx,_bbNullObject
	jne	_703
	call	_brl_blitz_NullObjectError
_703:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_71
_70:
	push	_807
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbNullObject
	mov	eax,dword [_bb_stuList]
	mov	dword [ebp-84],eax
	mov	ebx,dword [ebp-84]
	cmp	ebx,_bbNullObject
	jne	_811
	call	_brl_blitz_NullObjectError
_811:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	dword [ebp-92],eax
	jmp	_74
_76:
	mov	ebx,dword [ebp-92]
	cmp	ebx,_bbNullObject
	jne	_816
	call	_brl_blitz_NullObjectError
_816:
	push	_bb_Student
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-12],eax
	cmp	dword [ebp-12],_bbNullObject
	je	_74
	mov	eax,ebp
	push	eax
	push	_933
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_817
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_819
	call	_brl_blitz_NullObjectError
_819:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_822
	call	_brl_blitz_NullObjectError
_822:
	mov	ebx,dword [ebx+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_826
	push	eax
	call	_bbGCFree
	add	esp,4
_826:
	mov	dword [esi+32],ebx
	push	_827
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_829
	call	_brl_blitz_NullObjectError
_829:
	mov	edi,ebx
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_832
	call	_brl_blitz_NullObjectError
_832:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_834
	call	_brl_blitz_NullObjectError
_834:
	push	dword [ebx+8]
	push	_25
	push	dword [esi+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+36]
	dec	dword [eax+4]
	jnz	_838
	push	eax
	call	_bbGCFree
	add	esp,4
_838:
	mov	dword [edi+36],ebx
	push	_839
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_841
	call	_brl_blitz_NullObjectError
_841:
	mov	ebx,_72
	inc	dword [ebx+4]
	mov	eax,dword [esi+40]
	dec	dword [eax+4]
	jnz	_846
	push	eax
	call	_bbGCFree
	add	esp,4
_846:
	mov	dword [esi+40],ebx
	push	_847
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_849
	call	_brl_blitz_NullObjectError
_849:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_852
	call	_brl_blitz_NullObjectError
_852:
	mov	ebx,dword [ebx+12]
	inc	dword [ebx+4]
	mov	eax,dword [esi+44]
	dec	dword [eax+4]
	jnz	_856
	push	eax
	call	_bbGCFree
	add	esp,4
_856:
	mov	dword [esi+44],ebx
	push	_857
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_859
	call	_brl_blitz_NullObjectError
_859:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_862
	call	_brl_blitz_NullObjectError
_862:
	mov	ebx,dword [ebx+8]
	inc	dword [ebx+4]
	mov	eax,dword [esi+48]
	dec	dword [eax+4]
	jnz	_866
	push	eax
	call	_bbGCFree
	add	esp,4
_866:
	mov	dword [esi+48],ebx
	push	_867
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_869
	call	_brl_blitz_NullObjectError
_869:
	mov	edi,ebx
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_872
	call	_brl_blitz_NullObjectError
_872:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_874
	call	_brl_blitz_NullObjectError
_874:
	push	_77
	push	dword [ebx+8]
	push	_25
	push	dword [esi+12]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	mov	ebx,eax
	inc	dword [ebx+4]
	mov	eax,dword [edi+52]
	dec	dword [eax+4]
	jnz	_878
	push	eax
	call	_bbGCFree
	add	esp,4
_878:
	mov	dword [edi+52],ebx
	push	_879
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_881
	call	_brl_blitz_NullObjectError
_881:
	mov	ebx,_57
	inc	dword [ebx+4]
	mov	eax,dword [esi+64]
	dec	dword [eax+4]
	jnz	_886
	push	eax
	call	_bbGCFree
	add	esp,4
_886:
	mov	dword [esi+64],ebx
	push	_887
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_889
	call	_brl_blitz_NullObjectError
_889:
	push	_78
	push	dword [ebx+28]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_890
	mov	eax,ebp
	push	eax
	push	_899
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_891
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_893
	call	_brl_blitz_NullObjectError
_893:
	mov	ebx,_73
	inc	dword [ebx+4]
	mov	eax,dword [esi+68]
	dec	dword [eax+4]
	jnz	_898
	push	eax
	call	_bbGCFree
	add	esp,4
_898:
	mov	dword [esi+68],ebx
	call	dword [_bbOnDebugLeaveScope]
	jmp	_900
_890:
	mov	eax,ebp
	push	eax
	push	_909
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_901
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_903
	call	_brl_blitz_NullObjectError
_903:
	mov	ebx,_79
	inc	dword [ebx+4]
	mov	eax,dword [esi+68]
	dec	dword [eax+4]
	jnz	_908
	push	eax
	call	_bbGCFree
	add	esp,4
_908:
	mov	dword [esi+68],ebx
	call	dword [_bbOnDebugLeaveScope]
_900:
	push	_910
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-12]
	mov	dword [ebp-72],eax
	cmp	dword [ebp-72],_bbNullObject
	jne	_912
	call	_brl_blitz_NullObjectError
_912:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-68],eax
	cmp	dword [ebp-68],_bbNullObject
	jne	_914
	call	_brl_blitz_NullObjectError
_914:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-60],eax
	cmp	dword [ebp-60],_bbNullObject
	jne	_916
	call	_brl_blitz_NullObjectError
_916:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-52],eax
	cmp	dword [ebp-52],_bbNullObject
	jne	_918
	call	_brl_blitz_NullObjectError
_918:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-44],eax
	cmp	dword [ebp-44],_bbNullObject
	jne	_920
	call	_brl_blitz_NullObjectError
_920:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-36],eax
	cmp	dword [ebp-36],_bbNullObject
	jne	_922
	call	_brl_blitz_NullObjectError
_922:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	jne	_924
	call	_brl_blitz_NullObjectError
_924:
	mov	eax,dword [ebp-12]
	mov	dword [ebp-16],eax
	cmp	dword [ebp-16],_bbNullObject
	jne	_926
	call	_brl_blitz_NullObjectError
_926:
	mov	edi,dword [ebp-12]
	cmp	edi,_bbNullObject
	jne	_928
	call	_brl_blitz_NullObjectError
_928:
	mov	esi,dword [ebp-12]
	cmp	esi,_bbNullObject
	jne	_930
	call	_brl_blitz_NullObjectError
_930:
	mov	ebx,dword [ebp-12]
	cmp	ebx,_bbNullObject
	jne	_932
	call	_brl_blitz_NullObjectError
_932:
	push	dword [ebx+72]
	push	_51
	push	dword [esi+68]
	push	_51
	push	dword [edi+64]
	push	_51
	mov	eax,dword [ebp-16]
	push	dword [eax+60]
	push	_51
	mov	eax,dword [ebp-24]
	push	dword [eax+56]
	push	_51
	mov	eax,dword [ebp-36]
	push	dword [eax+52]
	push	_51
	mov	eax,dword [ebp-44]
	push	dword [eax+48]
	push	_51
	mov	eax,dword [ebp-52]
	push	dword [eax+44]
	push	_51
	mov	eax,dword [ebp-60]
	push	dword [eax+40]
	push	_51
	mov	eax,dword [ebp-68]
	push	dword [eax+36]
	push	_51
	mov	eax,dword [ebp-72]
	push	dword [eax+32]
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	call	_bbStringConcat
	add	esp,8
	push	eax
	push	dword [ebp-4]
	call	_brl_stream_WriteLine
	add	esp,8
	call	dword [_bbOnDebugLeaveScope]
_74:
	mov	ebx,dword [ebp-92]
	cmp	ebx,_bbNullObject
	jne	_814
	call	_brl_blitz_NullObjectError
_814:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_76
_75:
	push	_935
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_240
_240:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_convertFromCSV:
	push	ebp
	mov	ebp,esp
	sub	esp,20
	push	ebx
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbNullObject
	mov	dword [ebp-12],_bbEmptyString
	mov	dword [ebp-16],_bbEmptyString
	mov	dword [ebp-20],0
	push	ebp
	push	_962
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_938
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_brl_linkedlist_TList
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-8],eax
	push	_940
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_bbEmptyString
	push	_942
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],_bbEmptyString
	push	_944
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	eax,dword [ebp-4]
	mov	eax,dword [eax+8]
	mov	dword [ebp-20],eax
	push	_946
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_82:
	push	ebp
	push	_959
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_947
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	1
	push	dword [ebp-4]
	call	_brl_retro_Left
	add	esp,8
	mov	dword [ebp-16],eax
	push	_948
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_51
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_949
	push	ebp
	push	_952
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_950
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_brl_retro_Trim
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_951
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-12],_1
	call	dword [_bbOnDebugLeaveScope]
_949:
	push	_953
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_51
	push	dword [ebp-16]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	je	_954
	push	ebp
	push	_956
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_955
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-16]
	push	dword [ebp-12]
	call	_bbStringConcat
	add	esp,8
	mov	dword [ebp-12],eax
	call	dword [_bbOnDebugLeaveScope]
_954:
	push	_957
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	sub	dword [ebp-20],1
	push	_958
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-20]
	push	dword [ebp-4]
	call	_brl_retro_Right
	add	esp,8
	mov	dword [ebp-4],eax
	call	dword [_bbOnDebugLeaveScope]
_80:
	push	_1
	push	dword [ebp-4]
	call	_bbStringCompare
	add	esp,8
	cmp	eax,0
	jne	_82
_81:
	push	_960
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-12]
	call	_brl_retro_Trim
	add	esp,4
	push	eax
	push	dword [ebp-8]
	call	_brl_linkedlist_ListAddLast
	add	esp,8
	push	_961
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-8]
	jmp	_243
_243:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadCourse:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyArray
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-20],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1062
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_968
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_969
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_970
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_971
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_972
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_85:
	mov	eax,ebp
	push	eax
	push	_1056
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_973
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	5
	push	_974
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	_976
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_978
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_980
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-28],eax
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_984
	call	_brl_blitz_NullObjectError
_984:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_86
_88:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_989
	call	_brl_blitz_NullObjectError
_989:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_86
	mov	eax,ebp
	push	eax
	push	_999
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_990
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_992
	call	_brl_blitz_ArrayBoundsError
_992:
	mov	eax,dword [ebp-8]
	shl	ebx,2
	add	eax,ebx
	mov	esi,eax
	mov	eax,dword [ebp-24]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_997
	push	eax
	call	_bbGCFree
	add	esp,4
_997:
	mov	dword [esi+24],ebx
	push	_998
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_86:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_987
	call	_brl_blitz_NullObjectError
_987:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_88
_87:
	push	_1001
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Course
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1003
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_cList]
	cmp	ebx,_bbNullObject
	jne	_1005
	call	_brl_blitz_NullObjectError
_1005:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1006
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1008
	call	_brl_blitz_NullObjectError
_1008:
	mov	ebx,0
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1011
	call	_brl_blitz_ArrayBoundsError
_1011:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_1015
	push	eax
	call	_bbGCFree
	add	esp,4
_1015:
	mov	dword [esi+8],ebx
	push	_1016
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1018
	call	_brl_blitz_NullObjectError
_1018:
	mov	ebx,1
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1021
	call	_brl_blitz_ArrayBoundsError
_1021:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_1025
	push	eax
	call	_bbGCFree
	add	esp,4
_1025:
	mov	dword [esi+12],ebx
	push	_1026
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1028
	call	_brl_blitz_NullObjectError
_1028:
	mov	ebx,2
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1031
	call	_brl_blitz_ArrayBoundsError
_1031:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_1035
	push	eax
	call	_bbGCFree
	add	esp,4
_1035:
	mov	dword [esi+16],ebx
	push	_1036
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1038
	call	_brl_blitz_NullObjectError
_1038:
	mov	ebx,3
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1041
	call	_brl_blitz_ArrayBoundsError
_1041:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1045
	push	eax
	call	_bbGCFree
	add	esp,4
_1045:
	mov	dword [esi+20],ebx
	push	_1046
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1048
	call	_brl_blitz_NullObjectError
_1048:
	mov	ebx,4
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1051
	call	_brl_blitz_ArrayBoundsError
_1051:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1055
	push	eax
	call	_bbGCFree
	add	esp,4
_1055:
	mov	dword [esi+24],ebx
	call	dword [_bbOnDebugLeaveScope]
_83:
	push	dword [ebp-4]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_85
_84:
	push	_1061
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_246
_246:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadEnrollment:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyArray
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-20],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1175
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1065
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1066
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1067
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1068
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1069
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_91:
	mov	eax,ebp
	push	eax
	push	_1172
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1070
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	7
	push	_1071
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1073
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1075
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_1077
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-28],eax
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1081
	call	_brl_blitz_NullObjectError
_1081:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_92
_94:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1086
	call	_brl_blitz_NullObjectError
_1086:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_92
	mov	eax,ebp
	push	eax
	push	_1096
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1087
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1089
	call	_brl_blitz_ArrayBoundsError
_1089:
	mov	eax,dword [ebp-8]
	shl	ebx,2
	add	eax,ebx
	mov	esi,eax
	mov	eax,dword [ebp-24]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1094
	push	eax
	call	_bbGCFree
	add	esp,4
_1094:
	mov	dword [esi+24],ebx
	push	_1095
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_92:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1084
	call	_brl_blitz_NullObjectError
_1084:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_94
_93:
	push	_1097
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Enrollment
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1099
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_eList]
	cmp	ebx,_bbNullObject
	jne	_1101
	call	_brl_blitz_NullObjectError
_1101:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1102
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1104
	call	_brl_blitz_NullObjectError
_1104:
	mov	ebx,0
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1107
	call	_brl_blitz_ArrayBoundsError
_1107:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_1111
	push	eax
	call	_bbGCFree
	add	esp,4
_1111:
	mov	dword [esi+8],ebx
	push	_1112
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1114
	call	_brl_blitz_NullObjectError
_1114:
	mov	ebx,1
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1117
	call	_brl_blitz_ArrayBoundsError
_1117:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_1121
	push	eax
	call	_bbGCFree
	add	esp,4
_1121:
	mov	dword [esi+12],ebx
	push	_1122
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1124
	call	_brl_blitz_NullObjectError
_1124:
	mov	ebx,2
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1127
	call	_brl_blitz_ArrayBoundsError
_1127:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1131
	push	eax
	call	_bbGCFree
	add	esp,4
_1131:
	mov	dword [esi+20],ebx
	push	_1132
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1134
	call	_brl_blitz_NullObjectError
_1134:
	mov	ebx,3
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1137
	call	_brl_blitz_ArrayBoundsError
_1137:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1141
	push	eax
	call	_bbGCFree
	add	esp,4
_1141:
	mov	dword [esi+24],ebx
	push	_1142
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1144
	call	_brl_blitz_NullObjectError
_1144:
	mov	ebx,4
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1147
	call	_brl_blitz_ArrayBoundsError
_1147:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+28]
	dec	dword [eax+4]
	jnz	_1151
	push	eax
	call	_bbGCFree
	add	esp,4
_1151:
	mov	dword [esi+28],ebx
	push	_1152
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1154
	call	_brl_blitz_NullObjectError
_1154:
	mov	ebx,5
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1157
	call	_brl_blitz_ArrayBoundsError
_1157:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+32]
	dec	dword [eax+4]
	jnz	_1161
	push	eax
	call	_bbGCFree
	add	esp,4
_1161:
	mov	dword [esi+32],ebx
	push	_1162
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1164
	call	_brl_blitz_NullObjectError
_1164:
	mov	ebx,6
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1167
	call	_brl_blitz_ArrayBoundsError
_1167:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+36]
	dec	dword [eax+4]
	jnz	_1171
	push	eax
	call	_bbGCFree
	add	esp,4
_1171:
	mov	dword [esi+36],ebx
	call	dword [_bbOnDebugLeaveScope]
_89:
	push	dword [ebp-4]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_91
_90:
	push	_1174
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_249
_249:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadStaff:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyArray
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-20],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1256
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1177
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1178
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1179
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1180
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1181
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_97:
	mov	eax,ebp
	push	eax
	push	_1254
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1182
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	4
	push	_1183
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1185
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1187
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_1189
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-28],eax
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1193
	call	_brl_blitz_NullObjectError
_1193:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_98
_100:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1198
	call	_brl_blitz_NullObjectError
_1198:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_98
	mov	eax,ebp
	push	eax
	push	_1208
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1199
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1201
	call	_brl_blitz_ArrayBoundsError
_1201:
	mov	eax,dword [ebp-8]
	shl	ebx,2
	add	eax,ebx
	mov	esi,eax
	mov	eax,dword [ebp-24]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1206
	push	eax
	call	_bbGCFree
	add	esp,4
_1206:
	mov	dword [esi+24],ebx
	push	_1207
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_98:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1196
	call	_brl_blitz_NullObjectError
_1196:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_100
_99:
	push	_1209
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Staff
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1211
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_staList]
	cmp	ebx,_bbNullObject
	jne	_1213
	call	_brl_blitz_NullObjectError
_1213:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1214
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1216
	call	_brl_blitz_NullObjectError
_1216:
	mov	ebx,0
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1219
	call	_brl_blitz_ArrayBoundsError
_1219:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_1223
	push	eax
	call	_bbGCFree
	add	esp,4
_1223:
	mov	dword [esi+8],ebx
	push	_1224
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1226
	call	_brl_blitz_NullObjectError
_1226:
	mov	ebx,1
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1229
	call	_brl_blitz_ArrayBoundsError
_1229:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_1233
	push	eax
	call	_bbGCFree
	add	esp,4
_1233:
	mov	dword [esi+12],ebx
	push	_1234
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1236
	call	_brl_blitz_NullObjectError
_1236:
	mov	ebx,2
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1239
	call	_brl_blitz_ArrayBoundsError
_1239:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_1243
	push	eax
	call	_bbGCFree
	add	esp,4
_1243:
	mov	dword [esi+16],ebx
	push	_1244
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1246
	call	_brl_blitz_NullObjectError
_1246:
	mov	ebx,3
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1249
	call	_brl_blitz_ArrayBoundsError
_1249:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1253
	push	eax
	call	_bbGCFree
	add	esp,4
_1253:
	mov	dword [esi+20],ebx
	call	dword [_bbOnDebugLeaveScope]
_95:
	push	dword [ebp-4]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_97
_96:
	push	_1255
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_252
_252:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
_bb_loadStudent:
	push	ebp
	mov	ebp,esp
	sub	esp,28
	push	ebx
	push	esi
	push	edi
	mov	eax,dword [ebp+8]
	mov	dword [ebp-4],eax
	mov	dword [ebp-8],_bbEmptyArray
	mov	dword [ebp-12],_bbNullObject
	mov	dword [ebp-16],0
	mov	dword [ebp-24],_bbEmptyString
	mov	dword [ebp-20],_bbNullObject
	mov	eax,ebp
	push	eax
	push	_1357
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1258
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1259
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1260
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1261
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	_1262
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
_103:
	mov	eax,ebp
	push	eax
	push	_1355
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1263
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	6
	push	_1264
	call	_bbArrayNew1D
	add	esp,8
	mov	dword [ebp-8],eax
	push	_1266
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_stream_ReadLine
	add	esp,4
	push	eax
	call	_bb_convertFromCSV
	add	esp,4
	mov	dword [ebp-12],eax
	push	_1268
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-16],0
	push	_1270
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	dword [ebp-24],_bbEmptyString
	mov	eax,dword [ebp-12]
	mov	dword [ebp-28],eax
	mov	ebx,dword [ebp-28]
	cmp	ebx,_bbNullObject
	jne	_1274
	call	_brl_blitz_NullObjectError
_1274:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+140]
	add	esp,4
	mov	edi,eax
	jmp	_104
_106:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1279
	call	_brl_blitz_NullObjectError
_1279:
	push	_bbStringClass
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+52]
	add	esp,4
	push	eax
	call	_bbObjectDowncast
	add	esp,8
	mov	dword [ebp-24],eax
	cmp	dword [ebp-24],_bbNullObject
	je	_104
	mov	eax,ebp
	push	eax
	push	_1289
	call	dword [_bbOnDebugEnterScope]
	add	esp,8
	push	_1280
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [ebp-16]
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1282
	call	_brl_blitz_ArrayBoundsError
_1282:
	mov	eax,dword [ebp-8]
	shl	ebx,2
	add	eax,ebx
	mov	esi,eax
	mov	eax,dword [ebp-24]
	inc	dword [eax+4]
	mov	ebx,eax
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1287
	push	eax
	call	_bbGCFree
	add	esp,4
_1287:
	mov	dword [esi+24],ebx
	push	_1288
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	add	dword [ebp-16],1
	call	dword [_bbOnDebugLeaveScope]
_104:
	mov	ebx,edi
	cmp	ebx,_bbNullObject
	jne	_1277
	call	_brl_blitz_NullObjectError
_1277:
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+48]
	add	esp,4
	cmp	eax,0
	jne	_106
_105:
	push	_1290
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	_bb_Student
	call	_bbObjectNew
	add	esp,4
	mov	dword [ebp-20],eax
	push	_1292
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	ebx,dword [_bb_stuList]
	cmp	ebx,_bbNullObject
	jne	_1294
	call	_brl_blitz_NullObjectError
_1294:
	push	dword [ebp-20]
	push	ebx
	mov	eax,dword [ebx]
	call	dword [eax+68]
	add	esp,8
	push	_1295
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1297
	call	_brl_blitz_NullObjectError
_1297:
	mov	ebx,0
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1300
	call	_brl_blitz_ArrayBoundsError
_1300:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+8]
	dec	dword [eax+4]
	jnz	_1304
	push	eax
	call	_bbGCFree
	add	esp,4
_1304:
	mov	dword [esi+8],ebx
	push	_1305
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1307
	call	_brl_blitz_NullObjectError
_1307:
	mov	ebx,1
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1310
	call	_brl_blitz_ArrayBoundsError
_1310:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+12]
	dec	dword [eax+4]
	jnz	_1314
	push	eax
	call	_bbGCFree
	add	esp,4
_1314:
	mov	dword [esi+12],ebx
	push	_1315
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1317
	call	_brl_blitz_NullObjectError
_1317:
	mov	ebx,2
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1320
	call	_brl_blitz_ArrayBoundsError
_1320:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+16]
	dec	dword [eax+4]
	jnz	_1324
	push	eax
	call	_bbGCFree
	add	esp,4
_1324:
	mov	dword [esi+16],ebx
	push	_1325
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1327
	call	_brl_blitz_NullObjectError
_1327:
	mov	ebx,3
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1330
	call	_brl_blitz_ArrayBoundsError
_1330:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+20]
	dec	dword [eax+4]
	jnz	_1334
	push	eax
	call	_bbGCFree
	add	esp,4
_1334:
	mov	dword [esi+20],ebx
	push	_1335
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1337
	call	_brl_blitz_NullObjectError
_1337:
	mov	ebx,4
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1340
	call	_brl_blitz_ArrayBoundsError
_1340:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+24]
	dec	dword [eax+4]
	jnz	_1344
	push	eax
	call	_bbGCFree
	add	esp,4
_1344:
	mov	dword [esi+24],ebx
	push	_1345
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	mov	esi,dword [ebp-20]
	cmp	esi,_bbNullObject
	jne	_1347
	call	_brl_blitz_NullObjectError
_1347:
	mov	ebx,5
	mov	eax,dword [ebp-8]
	cmp	ebx,dword [eax+20]
	jb	_1350
	call	_brl_blitz_ArrayBoundsError
_1350:
	mov	eax,dword [ebp-8]
	mov	ebx,dword [eax+ebx*4+24]
	inc	dword [ebx+4]
	mov	eax,dword [esi+28]
	dec	dword [eax+4]
	jnz	_1354
	push	eax
	call	_bbGCFree
	add	esp,4
_1354:
	mov	dword [esi+28],ebx
	call	dword [_bbOnDebugLeaveScope]
_101:
	push	dword [ebp-4]
	call	_brl_stream_Eof
	add	esp,4
	cmp	eax,0
	je	_103
_102:
	push	_1356
	call	dword [_bbOnDebugEnterStm]
	add	esp,4
	push	dword [ebp-4]
	call	_brl_filesystem_CloseFile
	add	esp,4
	mov	ebx,0
	jmp	_255
_255:
	call	dword [_bbOnDebugLeaveScope]
	mov	eax,ebx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp,ebp
	pop	ebp
	ret
	section	"data" data writeable align 8
	align	4
_300:
	dd	0
_291:
	db	"Integrator",0
_292:
	db	"My",0
_293:
	db	":z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
	align	4
_261:
	dd	_bbNullObject
_294:
	db	"schoolYear",0
_127:
	db	"$",0
	align	4
_33:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	49,53,49,54
	align	4
_bb_schoolYear:
	dd	_33
_295:
	db	"cList",0
_296:
	db	":TList",0
	align	4
_bb_cList:
	dd	_bbNullObject
_297:
	db	"eList",0
	align	4
_bb_eList:
	dd	_bbNullObject
_298:
	db	"staList",0
	align	4
_bb_staList:
	dd	_bbNullObject
_299:
	db	"stuList",0
	align	4
_bb_stuList:
	dd	_bbNullObject
	align	4
_290:
	dd	1
	dd	_291
	dd	4
	dd	_292
	dd	_293
	dd	_261
	dd	4
	dd	_294
	dd	_127
	dd	_bb_schoolYear
	dd	4
	dd	_295
	dd	_296
	dd	_bb_cList
	dd	4
	dd	_297
	dd	_296
	dd	_bb_eList
	dd	4
	dd	_298
	dd	_296
	dd	_bb_staList
	dd	4
	dd	_299
	dd	_296
	dd	_bb_stuList
	dd	0
_120:
	db	"z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0",0
_121:
	db	"New",0
_122:
	db	"()i",0
_123:
	db	"Delete",0
	align	4
_119:
	dd	2
	dd	_120
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_22:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_119
	dd	8
	dd	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_New
	dd	__bb_z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_125:
	db	"z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
_126:
	db	"Name",0
	align	4
_128:
	dd	_bbStringClass
	dd	2147483646
	dd	10
	dw	73,110,116,101,103,114,97,116,111,114
_129:
	db	"MajorVersion",0
_130:
	db	"i",0
	align	4
_131:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	48
_132:
	db	"MinorVersion",0
_133:
	db	"Revision",0
	align	4
_134:
	dd	_bbStringClass
	dd	2147483646
	dd	1
	dw	49
_135:
	db	"VersionString",0
	align	4
_136:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	48,46,48,46,49
_137:
	db	"AssemblyInfo",0
	align	4
_138:
	dd	_bbStringClass
	dd	2147483646
	dd	16
	dw	73,110,116,101,103,114,97,116,111,114,32,48,46,48,46,49
_139:
	db	"Platform",0
	align	4
_140:
	dd	_bbStringClass
	dd	2147483646
	dd	5
	dw	87,105,110,51,50
_141:
	db	"Architecture",0
	align	4
_142:
	dd	_bbStringClass
	dd	2147483646
	dd	3
	dw	120,56,54
_143:
	db	"DebugOn",0
	align	4
_124:
	dd	2
	dd	_125
	dd	1
	dd	_126
	dd	_127
	dd	_128
	dd	1
	dd	_129
	dd	_130
	dd	_131
	dd	1
	dd	_132
	dd	_130
	dd	_131
	dd	1
	dd	_133
	dd	_130
	dd	_134
	dd	1
	dd	_135
	dd	_127
	dd	_136
	dd	1
	dd	_137
	dd	_127
	dd	_138
	dd	1
	dd	_139
	dd	_127
	dd	_140
	dd	1
	dd	_141
	dd	_127
	dd	_142
	dd	1
	dd	_143
	dd	_130
	dd	_134
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_23:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_124
	dd	8
	dd	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	dd	__bb_z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_258:
	db	"C:/Users/Ryan/Desktop/bb integration/Integrator.bmx",0
	align	4
_257:
	dd	_258
	dd	76
	dd	5
	align	4
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Application:
	dd	_bbNullObject
	align	4
_259:
	dd	_258
	dd	77
	dd	5
	align	4
__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Resources:
	dd	_bbNullObject
_145:
	db	"z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
	align	4
_144:
	dd	2
	dd	_145
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_29:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_144
	dd	8
	dd	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_New
	dd	__bb_z_My_3bc424c3_7db9_4c4a_b143_25aa96d326ad_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_147:
	db	"Course",0
_148:
	db	"IN_sectionNumber",0
_149:
	db	"IN_sectionName",0
_150:
	db	"IN_startingGrade",0
_151:
	db	"IN_term",0
_152:
	db	"IN_period",0
_153:
	db	"BB_external_course_key",0
_154:
	db	"BB_course_id",0
_155:
	db	"BB_course_name",0
_156:
	db	"BB_master_course_key",0
_157:
	db	"BB_data_source_key",0
_158:
	db	"BB_Available_ind",0
	align	4
_146:
	dd	2
	dd	_147
	dd	3
	dd	_148
	dd	_127
	dd	8
	dd	3
	dd	_149
	dd	_127
	dd	12
	dd	3
	dd	_150
	dd	_127
	dd	16
	dd	3
	dd	_151
	dd	_127
	dd	20
	dd	3
	dd	_152
	dd	_127
	dd	24
	dd	3
	dd	_153
	dd	_127
	dd	28
	dd	3
	dd	_154
	dd	_127
	dd	32
	dd	3
	dd	_155
	dd	_127
	dd	36
	dd	3
	dd	_156
	dd	_127
	dd	40
	dd	3
	dd	_157
	dd	_127
	dd	44
	dd	3
	dd	_158
	dd	_127
	dd	48
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_bb_Course:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_146
	dd	52
	dd	__bb_Course_New
	dd	__bb_Course_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_160:
	db	"Enrollment",0
_161:
	db	"IN_section",0
_162:
	db	"IN_stateIDNumber",0
_163:
	db	"IN_LName",0
_164:
	db	"IN_FName",0
_165:
	db	"IN_TNum",0
_166:
	db	"IN_Teacher",0
_167:
	db	"IN_SchoolNumber",0
_168:
	db	"IN_AcademicSession",0
_169:
	db	"BB_external_person_key",0
_170:
	db	"BB_role",0
_171:
	db	"BB_available_ind",0
	align	4
_159:
	dd	2
	dd	_160
	dd	3
	dd	_161
	dd	_127
	dd	8
	dd	3
	dd	_162
	dd	_127
	dd	12
	dd	3
	dd	_163
	dd	_127
	dd	16
	dd	3
	dd	_164
	dd	_127
	dd	20
	dd	3
	dd	_165
	dd	_127
	dd	24
	dd	3
	dd	_166
	dd	_127
	dd	28
	dd	3
	dd	_167
	dd	_127
	dd	32
	dd	3
	dd	_168
	dd	_127
	dd	36
	dd	3
	dd	_153
	dd	_127
	dd	40
	dd	3
	dd	_169
	dd	_127
	dd	44
	dd	3
	dd	_170
	dd	_127
	dd	48
	dd	3
	dd	_171
	dd	_127
	dd	52
	dd	3
	dd	_157
	dd	_127
	dd	56
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_bb_Enrollment:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_159
	dd	60
	dd	__bb_Enrollment_New
	dd	__bb_Enrollment_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_173:
	db	"Staff",0
_174:
	db	"IN_referenceCode",0
_175:
	db	"IN_lastName",0
_176:
	db	"IN_firstName",0
_177:
	db	"IN_userName",0
_178:
	db	"BB_user_ID",0
_179:
	db	"BB_passwd",0
_180:
	db	"BB_firstName",0
_181:
	db	"BB_lastName",0
_182:
	db	"BB_email",0
_183:
	db	"BB_instution_role",0
_184:
	db	"BB_system_role",0
_185:
	db	"BB_row_status",0
	align	4
_172:
	dd	2
	dd	_173
	dd	3
	dd	_174
	dd	_127
	dd	8
	dd	3
	dd	_175
	dd	_127
	dd	12
	dd	3
	dd	_176
	dd	_127
	dd	16
	dd	3
	dd	_177
	dd	_127
	dd	20
	dd	3
	dd	_169
	dd	_127
	dd	24
	dd	3
	dd	_178
	dd	_127
	dd	28
	dd	3
	dd	_179
	dd	_127
	dd	32
	dd	3
	dd	_180
	dd	_127
	dd	36
	dd	3
	dd	_181
	dd	_127
	dd	40
	dd	3
	dd	_182
	dd	_127
	dd	44
	dd	3
	dd	_183
	dd	_127
	dd	48
	dd	3
	dd	_184
	dd	_127
	dd	52
	dd	3
	dd	_171
	dd	_127
	dd	56
	dd	3
	dd	_185
	dd	_127
	dd	60
	dd	3
	dd	_157
	dd	_127
	dd	64
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_bb_Staff:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_172
	dd	68
	dd	__bb_Staff_New
	dd	__bb_Staff_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
_187:
	db	"Student",0
_188:
	db	"IN_middleName",0
_189:
	db	"IN_institution",0
_190:
	db	"IN_enrollmentStatus",0
_191:
	db	"BB_system_Role",0
	align	4
_186:
	dd	2
	dd	_187
	dd	3
	dd	_175
	dd	_127
	dd	8
	dd	3
	dd	_176
	dd	_127
	dd	12
	dd	3
	dd	_188
	dd	_127
	dd	16
	dd	3
	dd	_189
	dd	_127
	dd	20
	dd	3
	dd	_174
	dd	_127
	dd	24
	dd	3
	dd	_190
	dd	_127
	dd	28
	dd	3
	dd	_169
	dd	_127
	dd	32
	dd	3
	dd	_178
	dd	_127
	dd	36
	dd	3
	dd	_179
	dd	_127
	dd	40
	dd	3
	dd	_180
	dd	_127
	dd	44
	dd	3
	dd	_181
	dd	_127
	dd	48
	dd	3
	dd	_182
	dd	_127
	dd	52
	dd	3
	dd	_183
	dd	_127
	dd	56
	dd	3
	dd	_191
	dd	_127
	dd	60
	dd	3
	dd	_171
	dd	_127
	dd	64
	dd	3
	dd	_185
	dd	_127
	dd	68
	dd	3
	dd	_157
	dd	_127
	dd	72
	dd	6
	dd	_121
	dd	_122
	dd	16
	dd	6
	dd	_123
	dd	_122
	dd	20
	dd	0
	align	4
_bb_Student:
	dd	_bbObjectClass
	dd	_bbObjectFree
	dd	_186
	dd	76
	dd	__bb_Student_New
	dd	__bb_Student_Delete
	dd	_bbObjectToString
	dd	_bbObjectCompare
	dd	_bbObjectSendMessage
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	dd	_bbObjectReserved
	align	4
_260:
	dd	_258
	dd	81
	dd	1
_263:
	db	"C:/Users/Ryan/Desktop/bb integration/main.bmx",0
	align	4
_262:
	dd	_263
	dd	3
	dd	1
	align	4
_30:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	67,58,92,66,66
	align	4
_264:
	dd	_263
	dd	4
	dd	1
	align	4
_31:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	100,97,116,97,92
	align	4
_267:
	dd	3
	dd	0
	dd	0
	align	4
_266:
	dd	_263
	dd	5
	dd	2
	align	4
_32:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	100,97,116,97
	align	4
_268:
	dd	_263
	dd	7
	dd	1
	align	4
_269:
	dd	_263
	dd	9
	dd	1
	align	4
_271:
	dd	0
	align	4
_273:
	dd	_263
	dd	10
	dd	1
	align	4
_276:
	dd	_263
	dd	11
	dd	1
	align	4
_279:
	dd	_263
	dd	12
	dd	1
	align	4
_282:
	dd	_263
	dd	90
	dd	1
	align	4
_41:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	84,97,108,108,97,115,115,101,101,67,111,117,114,115,101,46
	dw	99,115,118
	align	4
_283:
	dd	_263
	dd	92
	dd	1
	align	4
_42:
	dd	_bbStringClass
	dd	2147483647
	dd	23
	dw	84,97,108,108,97,115,115,101,101,69,110,114,111,108,108,109
	dw	101,110,116,46,99,115,118
	align	4
_284:
	dd	_263
	dd	94
	dd	1
	align	4
_43:
	dd	_bbStringClass
	dd	2147483647
	dd	18
	dw	84,97,108,108,97,115,115,101,101,83,116,97,102,102,46,99
	dw	115,118
	align	4
_285:
	dd	_263
	dd	96
	dd	1
	align	4
_44:
	dd	_bbStringClass
	dd	2147483647
	dd	20
	dw	84,97,108,108,97,115,115,101,101,83,116,117,100,101,110,116
	dw	46,99,115,118
	align	4
_286:
	dd	_263
	dd	99
	dd	1
	align	4
_287:
	dd	_263
	dd	101
	dd	1
	align	4
_288:
	dd	_263
	dd	103
	dd	1
	align	4
_289:
	dd	_263
	dd	115
	dd	1
_304:
	db	"Self",0
_305:
	db	":z_3bc424c3_7db9_4c4a_b143_25aa96d326ad_3_0",0
	align	4
_303:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_305
	dd	-4
	dd	0
	align	4
_302:
	dd	3
	dd	0
	dd	0
_309:
	db	":z_blide_bg3bc424c3_7db9_4c4a_b143_25aa96d326ad",0
	align	4
_308:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_309
	dd	-4
	dd	0
	align	4
_307:
	dd	3
	dd	0
	dd	0
	align	4
_312:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_293
	dd	-4
	dd	0
	align	4
_311:
	dd	3
	dd	0
	dd	0
_327:
	db	":Course",0
	align	4
_326:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_327
	dd	-4
	dd	0
	align	4
_1:
	dd	_bbStringClass
	dd	2147483647
	dd	0
	align	4
_34:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	95,67,111,117,114,115,101,115
	align	4
_325:
	dd	3
	dd	0
	dd	0
_366:
	db	":Enrollment",0
	align	4
_365:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_366
	dd	-4
	dd	0
	align	4
_35:
	dd	_bbStringClass
	dd	2147483647
	dd	12
	dw	95,69,110,114,111,108,108,109,101,110,116,115
	align	4
_364:
	dd	3
	dd	0
	dd	0
_411:
	db	":Staff",0
	align	4
_410:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_411
	dd	-4
	dd	0
	align	4
_36:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	102,97,99,117,108,116,121
	align	4
_37:
	dd	_bbStringClass
	dd	2147483647
	dd	4
	dw	78,111,110,101
	align	4
_38:
	dd	_bbStringClass
	dd	2147483647
	dd	6
	dw	95,83,116,97,102,102
	align	4
_409:
	dd	3
	dd	0
	dd	0
_462:
	db	":Student",0
	align	4
_461:
	dd	1
	dd	_121
	dd	2
	dd	_304
	dd	_462
	dd	-4
	dd	0
	align	4
_39:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	115,116,117,100,101,110,116
	align	4
_40:
	dd	_bbStringClass
	dd	2147483647
	dd	9
	dw	95,83,116,117,100,101,110,116,115
	align	4
_460:
	dd	3
	dd	0
	dd	0
_571:
	db	"saveCourse",0
_572:
	db	"nfile",0
_573:
	db	":TStream",0
	align	4
_570:
	dd	1
	dd	_571
	dd	2
	dd	_572
	dd	_573
	dd	-4
	dd	0
	align	4
_498:
	dd	_263
	dd	118
	dd	2
	align	4
_45:
	dd	_bbStringClass
	dd	2147483647
	dd	16
	dw	100,97,116,97,92,67,76,65,83,83,69,83,46,116,120,116
	align	4
_499:
	dd	_263
	dd	120
	dd	2
	align	4
_501:
	dd	_263
	dd	121
	dd	2
	align	4
_46:
	dd	_bbStringClass
	dd	2147483647
	dd	89
	dw	101,120,116,101,114,110,97,108,95,99,111,117,114,115,101,95
	dw	107,101,121,124,99,111,117,114,115,101,95,105,100,124,99,111
	dw	117,114,115,101,95,110,97,109,101,124,109,97,115,116,101,114
	dw	95,99,111,117,114,115,101,95,107,101,121,124,100,97,116,97
	dw	95,115,111,117,114,99,101,95,107,101,121,124,65,118,97,105
	dw	108,97,98,108,101,95,105,110,100
	align	4
_502:
	dd	_263
	dd	122
	dd	2
_568:
	db	"nCourse",0
	align	4
_567:
	dd	3
	dd	0
	dd	2
	dd	_568
	dd	_327
	dd	-8
	dd	0
	align	4
_512:
	dd	_263
	dd	124
	dd	3
	align	4
_522:
	dd	_263
	dd	125
	dd	3
	align	4
_50:
	dd	_bbStringClass
	dd	2147483647
	dd	2
	dw	45,80
	align	4
_534:
	dd	_263
	dd	127
	dd	3
	align	4
_544:
	dd	_263
	dd	128
	dd	3
	align	4
_554:
	dd	_263
	dd	129
	dd	3
	align	4
_51:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	124
	align	4
_569:
	dd	_263
	dd	131
	dd	2
_690:
	db	"saveEnrollment",0
_691:
	db	"teList",0
	align	4
_689:
	dd	1
	dd	_690
	dd	2
	dd	_691
	dd	_296
	dd	-4
	dd	2
	dd	_572
	dd	_573
	dd	-8
	dd	0
	align	4
_574:
	dd	_263
	dd	135
	dd	2
	align	4
_52:
	dd	_bbStringClass
	dd	2147483647
	dd	19
	dw	100,97,116,97,92,69,78,82,79,76,76,77,69,78,84,46
	dw	116,120,116
	align	4
_575:
	dd	_263
	dd	136
	dd	2
	align	4
_577:
	dd	_263
	dd	137
	dd	2
	align	4
_579:
	dd	_263
	dd	138
	dd	2
	align	4
_53:
	dd	_bbStringClass
	dd	2147483647
	dd	74
	dw	101,120,116,101,114,110,97,108,95,99,111,117,114,115,101,95
	dw	107,101,121,124,101,120,116,101,114,110,97,108,95,112,101,114
	dw	115,111,110,95,107,101,121,124,114,111,108,101,124,97,118,97
	dw	105,108,97,98,108,101,95,105,110,100,124,100,97,116,97,95
	dw	115,111,117,114,99,101,95,107,101,121
	align	4
_580:
	dd	_263
	dd	139
	dd	2
_673:
	db	"nRoll",0
_674:
	db	"teach",0
_675:
	db	"dup",0
	align	4
_672:
	dd	3
	dd	0
	dd	2
	dd	_673
	dd	_366
	dd	-12
	dd	2
	dd	_674
	dd	_127
	dd	-16
	dd	2
	dd	_675
	dd	_130
	dd	-20
	dd	0
	align	4
_590:
	dd	_263
	dd	140
	dd	3
	align	4
_600:
	dd	_263
	dd	141
	dd	3
	align	4
_610:
	dd	_263
	dd	142
	dd	3
	align	4
_618:
	dd	_263
	dd	143
	dd	3
	align	4
_57:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	78
	align	4
_626:
	dd	_263
	dd	145
	dd	3
	align	4
_58:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	95,83,116,117,100,101,110,116
	align	4
_637:
	dd	_263
	dd	151
	dd	3
	align	4
_60:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	95,84,101,97,99,104,101,114
	align	4
_59:
	dd	_bbStringClass
	dd	2147483647
	dd	10
	dw	105,110,115,116,114,117,99,116,111,114
	align	4
_647:
	dd	_263
	dd	156
	dd	3
	align	4
_649:
	dd	_263
	dd	157
	dd	3
_665:
	db	"Str",0
	align	4
_664:
	dd	3
	dd	0
	dd	2
	dd	_665
	dd	_127
	dd	-24
	dd	0
	align	4
_659:
	dd	_263
	dd	158
	dd	4
	align	4
_663:
	dd	3
	dd	0
	dd	0
	align	4
_661:
	dd	_263
	dd	159
	dd	5
	align	4
_662:
	dd	_263
	dd	160
	dd	5
	align	4
_666:
	dd	_263
	dd	163
	dd	3
	align	4
_671:
	dd	3
	dd	0
	dd	0
	align	4
_668:
	dd	_263
	dd	164
	dd	4
	align	4
_676:
	dd	_263
	dd	167
	dd	2
	align	4
_687:
	dd	3
	dd	0
	dd	2
	dd	_674
	dd	_127
	dd	-28
	dd	0
	align	4
_686:
	dd	_263
	dd	168
	dd	3
	align	4
_688:
	dd	_263
	dd	170
	dd	2
_937:
	db	"saveUsers",0
	align	4
_936:
	dd	1
	dd	_937
	dd	2
	dd	_572
	dd	_573
	dd	-4
	dd	0
	align	4
_692:
	dd	_263
	dd	174
	dd	2
	align	4
_67:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	100,97,116,97,92,85,83,69,82,83,46,116,120,116
	align	4
_693:
	dd	_263
	dd	176
	dd	2
	align	4
_695:
	dd	_263
	dd	177
	dd	2
	align	4
_68:
	dd	_bbStringClass
	dd	2147483647
	dd	129
	dw	69,120,116,101,114,110,97,108,95,80,101,114,115,111,110,95
	dw	75,101,121,124,85,115,101,114,95,73,68,124,80,97,115,115
	dw	119,100,124,70,105,114,115,116,110,97,109,101,124,76,97,115
	dw	116,110,97,109,101,124,69,109,97,105,108,124,73,110,115,116
	dw	105,116,117,116,105,111,110,95,82,111,108,101,124,83,121,115
	dw	116,101,109,95,82,111,108,101,124,65,118,97,105,108,97,98
	dw	108,101,95,105,110,100,124,114,111,119,95,115,116,97,116,117
	dw	115,124,68,97,116,97,95,83,111,117,114,99,101,95,75,101
	dw	121
	align	4
_696:
	dd	_263
	dd	178
	dd	2
_806:
	db	"nStaff",0
	align	4
_805:
	dd	3
	dd	0
	dd	2
	dd	_806
	dd	_411
	dd	-8
	dd	0
	align	4
_706:
	dd	_263
	dd	179
	dd	3
	align	4
_716:
	dd	_263
	dd	180
	dd	3
	align	4
_25:
	dd	_bbStringClass
	dd	2147483647
	dd	1
	dw	46
	align	4
_728:
	dd	_263
	dd	181
	dd	3
	align	4
_72:
	dd	_bbStringClass
	dd	2147483647
	dd	5
	dw	49,50,51,52,53
	align	4
_736:
	dd	_263
	dd	182
	dd	3
	align	4
_746:
	dd	_263
	dd	183
	dd	3
	align	4
_756:
	dd	_263
	dd	184
	dd	3
	align	4
_766:
	dd	_263
	dd	187
	dd	3
	align	4
_774:
	dd	_263
	dd	188
	dd	3
	align	4
_73:
	dd	_bbStringClass
	dd	2147483647
	dd	7
	dw	101,110,97,98,108,101,100
	align	4
_782:
	dd	_263
	dd	191
	dd	3
	align	4
_807:
	dd	_263
	dd	204
	dd	2
_934:
	db	"nStu",0
	align	4
_933:
	dd	3
	dd	0
	dd	2
	dd	_934
	dd	_462
	dd	-12
	dd	0
	align	4
_817:
	dd	_263
	dd	205
	dd	3
	align	4
_827:
	dd	_263
	dd	206
	dd	3
	align	4
_839:
	dd	_263
	dd	207
	dd	3
	align	4
_847:
	dd	_263
	dd	208
	dd	3
	align	4
_857:
	dd	_263
	dd	209
	dd	3
	align	4
_867:
	dd	_263
	dd	210
	dd	3
	align	4
_77:
	dd	_bbStringClass
	dd	2147483647
	dd	14
	dw	64,116,99,115,46,107,49,50,46,97,108,46,117,115
	align	4
_879:
	dd	_263
	dd	213
	dd	3
	align	4
_887:
	dd	_263
	dd	214
	dd	3
	align	4
_78:
	dd	_bbStringClass
	dd	2147483647
	dd	17
	dw	67,117,114,114,101,110,116,108,121,69,110,114,111,108,108,101
	dw	100
	align	4
_899:
	dd	3
	dd	0
	dd	0
	align	4
_891:
	dd	_263
	dd	215
	dd	4
	align	4
_909:
	dd	3
	dd	0
	dd	0
	align	4
_901:
	dd	_263
	dd	217
	dd	4
	align	4
_79:
	dd	_bbStringClass
	dd	2147483647
	dd	8
	dw	100,105,115,97,98,108,101,100
	align	4
_910:
	dd	_263
	dd	221
	dd	3
	align	4
_935:
	dd	_263
	dd	233
	dd	2
_963:
	db	"convertFromCSV",0
_964:
	db	"line",0
_965:
	db	"lineArray",0
_966:
	db	"word",0
_967:
	db	"letter",0
	align	4
_962:
	dd	1
	dd	_963
	dd	2
	dd	_964
	dd	_127
	dd	-4
	dd	2
	dd	_965
	dd	_296
	dd	-8
	dd	2
	dd	_966
	dd	_127
	dd	-12
	dd	2
	dd	_967
	dd	_127
	dd	-16
	dd	2
	dd	_130
	dd	_130
	dd	-20
	dd	0
	align	4
_938:
	dd	_263
	dd	245
	dd	2
	align	4
_940:
	dd	_263
	dd	246
	dd	2
	align	4
_942:
	dd	_263
	dd	247
	dd	2
	align	4
_944:
	dd	_263
	dd	248
	dd	2
	align	4
_946:
	dd	_263
	dd	260
	dd	2
	align	4
_959:
	dd	3
	dd	0
	dd	0
	align	4
_947:
	dd	_263
	dd	250
	dd	3
	align	4
_948:
	dd	_263
	dd	251
	dd	3
	align	4
_952:
	dd	3
	dd	0
	dd	0
	align	4
_950:
	dd	_263
	dd	252
	dd	4
	align	4
_951:
	dd	_263
	dd	253
	dd	4
	align	4
_953:
	dd	_263
	dd	255
	dd	3
	align	4
_956:
	dd	3
	dd	0
	dd	0
	align	4
_955:
	dd	_263
	dd	256
	dd	4
	align	4
_957:
	dd	_263
	dd	258
	dd	3
	align	4
_958:
	dd	_263
	dd	259
	dd	3
	align	4
_960:
	dd	_263
	dd	261
	dd	2
	align	4
_961:
	dd	_263
	dd	262
	dd	2
_1063:
	db	"loadCourse",0
_1064:
	db	"file",0
	align	4
_1062:
	dd	1
	dd	_1063
	dd	2
	dd	_1064
	dd	_573
	dd	-4
	dd	0
	align	4
_968:
	dd	_263
	dd	266
	dd	2
	align	4
_969:
	dd	_263
	dd	267
	dd	2
	align	4
_970:
	dd	_263
	dd	268
	dd	2
	align	4
_971:
	dd	_263
	dd	269
	dd	2
	align	4
_972:
	dd	_263
	dd	290
	dd	2
_1057:
	db	"data",0
_1058:
	db	"[]$",0
_1059:
	db	"dataList",0
_1060:
	db	"thisCourse",0
	align	4
_1056:
	dd	3
	dd	0
	dd	2
	dd	_1057
	dd	_1058
	dd	-8
	dd	2
	dd	_1059
	dd	_296
	dd	-12
	dd	2
	dd	_130
	dd	_130
	dd	-16
	dd	2
	dd	_1060
	dd	_327
	dd	-20
	dd	0
	align	4
_973:
	dd	_263
	dd	271
	dd	3
_974:
	db	"$",0
	align	4
_976:
	dd	_263
	dd	272
	dd	3
	align	4
_978:
	dd	_263
	dd	273
	dd	3
	align	4
_980:
	dd	_263
	dd	274
	dd	3
_1000:
	db	"colm",0
	align	4
_999:
	dd	3
	dd	0
	dd	2
	dd	_1000
	dd	_127
	dd	-24
	dd	0
	align	4
_990:
	dd	_263
	dd	275
	dd	4
	align	4
_998:
	dd	_263
	dd	276
	dd	4
	align	4
_1001:
	dd	_263
	dd	278
	dd	3
	align	4
_1003:
	dd	_263
	dd	279
	dd	3
	align	4
_1006:
	dd	_263
	dd	280
	dd	3
	align	4
_1016:
	dd	_263
	dd	282
	dd	3
	align	4
_1026:
	dd	_263
	dd	284
	dd	3
	align	4
_1036:
	dd	_263
	dd	286
	dd	3
	align	4
_1046:
	dd	_263
	dd	288
	dd	3
	align	4
_1061:
	dd	_263
	dd	291
	dd	2
_1176:
	db	"loadEnrollment",0
	align	4
_1175:
	dd	1
	dd	_1176
	dd	2
	dd	_1064
	dd	_573
	dd	-4
	dd	0
	align	4
_1065:
	dd	_263
	dd	295
	dd	2
	align	4
_1066:
	dd	_263
	dd	296
	dd	2
	align	4
_1067:
	dd	_263
	dd	297
	dd	2
	align	4
_1068:
	dd	_263
	dd	298
	dd	2
	align	4
_1069:
	dd	_263
	dd	325
	dd	2
_1173:
	db	"nroll",0
	align	4
_1172:
	dd	3
	dd	0
	dd	2
	dd	_1057
	dd	_1058
	dd	-8
	dd	2
	dd	_1059
	dd	_296
	dd	-12
	dd	2
	dd	_130
	dd	_130
	dd	-16
	dd	2
	dd	_1173
	dd	_366
	dd	-20
	dd	0
	align	4
_1070:
	dd	_263
	dd	300
	dd	3
_1071:
	db	"$",0
	align	4
_1073:
	dd	_263
	dd	301
	dd	3
	align	4
_1075:
	dd	_263
	dd	302
	dd	3
	align	4
_1077:
	dd	_263
	dd	303
	dd	3
	align	4
_1096:
	dd	3
	dd	0
	dd	2
	dd	_1000
	dd	_127
	dd	-24
	dd	0
	align	4
_1087:
	dd	_263
	dd	304
	dd	4
	align	4
_1095:
	dd	_263
	dd	305
	dd	4
	align	4
_1097:
	dd	_263
	dd	307
	dd	3
	align	4
_1099:
	dd	_263
	dd	308
	dd	3
	align	4
_1102:
	dd	_263
	dd	309
	dd	3
	align	4
_1112:
	dd	_263
	dd	311
	dd	3
	align	4
_1122:
	dd	_263
	dd	315
	dd	3
	align	4
_1132:
	dd	_263
	dd	317
	dd	3
	align	4
_1142:
	dd	_263
	dd	319
	dd	3
	align	4
_1152:
	dd	_263
	dd	321
	dd	3
	align	4
_1162:
	dd	_263
	dd	323
	dd	3
	align	4
_1174:
	dd	_263
	dd	326
	dd	2
_1257:
	db	"loadStaff",0
	align	4
_1256:
	dd	1
	dd	_1257
	dd	2
	dd	_1064
	dd	_573
	dd	-4
	dd	0
	align	4
_1177:
	dd	_263
	dd	330
	dd	2
	align	4
_1178:
	dd	_263
	dd	331
	dd	2
	align	4
_1179:
	dd	_263
	dd	332
	dd	2
	align	4
_1180:
	dd	_263
	dd	333
	dd	2
	align	4
_1181:
	dd	_263
	dd	352
	dd	2
	align	4
_1254:
	dd	3
	dd	0
	dd	2
	dd	_1057
	dd	_1058
	dd	-8
	dd	2
	dd	_1059
	dd	_296
	dd	-12
	dd	2
	dd	_130
	dd	_130
	dd	-16
	dd	2
	dd	_806
	dd	_411
	dd	-20
	dd	0
	align	4
_1182:
	dd	_263
	dd	335
	dd	3
_1183:
	db	"$",0
	align	4
_1185:
	dd	_263
	dd	336
	dd	3
	align	4
_1187:
	dd	_263
	dd	337
	dd	3
	align	4
_1189:
	dd	_263
	dd	338
	dd	3
	align	4
_1208:
	dd	3
	dd	0
	dd	2
	dd	_1000
	dd	_127
	dd	-24
	dd	0
	align	4
_1199:
	dd	_263
	dd	339
	dd	4
	align	4
_1207:
	dd	_263
	dd	340
	dd	4
	align	4
_1209:
	dd	_263
	dd	342
	dd	3
	align	4
_1211:
	dd	_263
	dd	343
	dd	3
	align	4
_1214:
	dd	_263
	dd	344
	dd	3
	align	4
_1224:
	dd	_263
	dd	346
	dd	3
	align	4
_1234:
	dd	_263
	dd	348
	dd	3
	align	4
_1244:
	dd	_263
	dd	350
	dd	3
	align	4
_1255:
	dd	_263
	dd	353
	dd	2
_1358:
	db	"loadStudent",0
	align	4
_1357:
	dd	1
	dd	_1358
	dd	2
	dd	_1064
	dd	_573
	dd	-4
	dd	0
	align	4
_1258:
	dd	_263
	dd	357
	dd	2
	align	4
_1259:
	dd	_263
	dd	358
	dd	2
	align	4
_1260:
	dd	_263
	dd	359
	dd	2
	align	4
_1261:
	dd	_263
	dd	360
	dd	2
	align	4
_1262:
	dd	_263
	dd	377
	dd	2
	align	4
_1355:
	dd	3
	dd	0
	dd	2
	dd	_1057
	dd	_1058
	dd	-8
	dd	2
	dd	_1059
	dd	_296
	dd	-12
	dd	2
	dd	_130
	dd	_130
	dd	-16
	dd	2
	dd	_934
	dd	_462
	dd	-20
	dd	0
	align	4
_1263:
	dd	_263
	dd	362
	dd	3
_1264:
	db	"$",0
	align	4
_1266:
	dd	_263
	dd	363
	dd	3
	align	4
_1268:
	dd	_263
	dd	364
	dd	3
	align	4
_1270:
	dd	_263
	dd	365
	dd	3
	align	4
_1289:
	dd	3
	dd	0
	dd	2
	dd	_1000
	dd	_127
	dd	-24
	dd	0
	align	4
_1280:
	dd	_263
	dd	366
	dd	4
	align	4
_1288:
	dd	_263
	dd	367
	dd	4
	align	4
_1290:
	dd	_263
	dd	369
	dd	3
	align	4
_1292:
	dd	_263
	dd	370
	dd	3
	align	4
_1295:
	dd	_263
	dd	371
	dd	3
	align	4
_1305:
	dd	_263
	dd	372
	dd	3
	align	4
_1315:
	dd	_263
	dd	373
	dd	3
	align	4
_1325:
	dd	_263
	dd	374
	dd	3
	align	4
_1335:
	dd	_263
	dd	375
	dd	3
	align	4
_1345:
	dd	_263
	dd	376
	dd	3
	align	4
_1356:
	dd	_263
	dd	378
	dd	2
