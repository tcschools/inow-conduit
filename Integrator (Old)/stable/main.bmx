'This BMX file was edited with BLIde ( http://www.blide.org )
'print "Setting directory to C:\BB"

'ToDo: Group classes based on subject and teacher, not just subject.
'Include the password field for new users only, users who have been added previously should be uploaded in a seperate file that does not include the password field.
'Class Names should include the semester they are being taught in.
'Class year should be attached to the end of the Class ID
'All classess and enrollments should be in one data source and all users should be in one data source.
'Need to make a remote controller for the mac to modify these settings.
'This should all be automatic.


ChangeDir("C:\BB")
If FileType("data\") = 0
	CreateDir("data")
End If

Global schoolYear:String = "1415"
Global teachersEnabled:String = "Y"
Global studentsEnabled:String = "Y"
Global enrollmentsEnabled:String = "Y"

Global cList:TList = New TList
Global eList:TList = New TList
Global staList:TList = New TList
Global stuList:TList = New TList

Global excludeList:TList = New TList

'Add all excluded class IDs here
ListAddLast(excludeList, "480073")
ListAddLast(excludeList, "400026")
ListAddLast(excludeList, "220011aa")
ListAddLast(excludeList, "220081ab")
ListAddLast(excludeList, "220051aa")
ListAddLast(excludeList, "250002")
ListAddLast(excludeList, "200017")
ListAddLast(excludeList, "802104ab")


Type Course
	Field IN_sectionNumber:String = ""
	Field IN_sectionName:String = ""
	Field IN_startingGrade:String = ""
	Field IN_term:String = ""
	Field IN_period:String = ""
	
	Field BB_external_course_key:String 'IN_sectionNumber
	Field BB_course_id:String 'IN_sectionNumber
	Field BB_course_name:String 'IN_sectionName
	Field BB_master_course_key:String 'If course is a child course add it's parents external course key here
	Field BB_data_source_key:String = schoolYear + "_Courses"
	Field BB_Available_ind:String = "Y" 'If should not be made available type "N"
End Type

Type Enrollment
	Field IN_section:String = ""
	Field IN_stateIDNumber:String = ""
	Field IN_LName:String = ""
	Field IN_FName:String = ""
	Field IN_TNum:String = ""
	Field IN_Teacher:String = ""
	Field IN_SchoolNumber:String = ""
	Field IN_AcademicSession:String = ""
	
	Field BB_external_course_key:String 'section
	Field BB_external_person_key:String 'stateIDNumber
	Field BB_role:String 'Student unless TNum = staff referencecode
	Field BB_available_ind:String = enrollmentsEnabled 'blank unles specified as "N"
	Field BB_data_source_key:String = schoolYear + "_Courses"
End Type

Type Staff
	Field IN_referenceCode:String
	Field IN_lastName:String
	Field IN_firstName:String
	Field IN_userName:String
	
	Field BB_external_person_key:String 'referenceCode
	Field BB_user_ID:String 'IN_firstName + "." + IN_lastName
	Field BB_passwd:String 'default 12345
	Field BB_firstName:String 'IN_firstName
	Field BB_lastName:String 'IN_lastName
	Field BB_email:String 'IN_userName
	Field BB_instution_role:String = "faculty"
	Field BB_system_role:String = "None"
	Field BB_available_ind:String = teachersEnabled '"Y" or "N"
	Field BB_row_status:String 'enabled or disabled
	Field BB_data_source_key:String = schoolYear + "_Users"
End Type

Type Student
	Field IN_lastName:String = ""
	Field IN_firstName:String = ""
	Field IN_middleName:String = ""
	Field IN_institution:String = ""
	Field IN_referenceCode:String = ""
	Field IN_enrollmentStatus:String = ""
	
	Field BB_external_person_key:String 'referenceCode
	Field BB_user_ID:String 'IN_firstName + "." + IN_lastName
	Field BB_passwd:String 'default = 12345
	Field BB_firstName:String 'IN_firstName
	Field BB_lastName:String 'IN_lastName
	Field BB_email:String 'IN_firstName + "." + IN_lastName + "@tcs.k12.al.us"
	Field BB_instution_role:String = "student"
	Field BB_system_Role:String = "None"
	Field BB_available_ind:String = studentsEnabled '"Y" or "N"
	Field BB_row_status:String 'enabled or disabled
	Field BB_data_source_key:String = schoolYear + "_Users"
End Type



Print"Loading Courses"
loadCourse(ReadFile("TallasseeCourse.csv"))
Print "Loading Enrollments"
loadEnrollment(ReadFile("TallasseeEnrollment.csv"))
Print "Loading Staff"
loadStaff(ReadFile("TallasseeStaff.csv"))
Print "Loading Students"
loadStudent(ReadFile("TallasseeStudent.csv"))

Print "Converting Courses"
saveCourse()
Print "Converting Users"
saveUsers()
Print "Converting Enrollments"
saveEnrollment()

For Local s = 0 To 15
	Print "Please wait " + (15 - s) + " more seconds."
	Delay (1000)
Next
Print "Sending Courses"
'sendCourse()
Print "Sending Users"
'sendUsers()
Print "Sending Enrollments"
'sendEnrollment()
SendAll() '----------------------------------------
Print "Done"
End

Function SendAll()
	system_("C:\BB\Data\command.bat")
End Function

Function saveCourse()
	'Backup file CLASSES.txt
	Local excludeClass = 0
	DeleteFile("data\CLASSES.txt")
	
	'Check for differences in the old and new files
	
	Local nfile:TStream = WriteFile("data\CLASSES.txt")'OpenFile("data\CLASSES.txt")
	WriteLine(nfile, "external_course_key|course_id|course_name|master_course_key|data_source_key|Available_ind")
	Local lastSection:String = ""
	For Local nCourse:Course = EachIn cList
		'nCourse.BB_Available_ind
		nCourse.BB_course_id = nCourse.IN_sectionNumber

		For Local cID:String = EachIn excludeList
			If Instr(nCourse.BB_course_id, cID)'Left(nCourse.BB_external_course_key, Len(nCourse.BB_external_course_key) - 4) = cID
				excludeClass = 1
			EndIf
		Next
		
			If lastSection <> Left(nCourse.IN_sectionNumber, Len(nCourse.IN_sectionNumber) - 4)
				Local pCourse:Course = New Course
			
				pCourse.BB_course_id = Left(nCourse.IN_sectionNumber, Len(nCourse.IN_sectionNumber) - 4) + ".parent"
				pCourse.BB_course_name = nCourse.IN_sectionName
				pCourse.BB_external_course_key = pCourse.BB_course_id
				pCourse.BB_master_course_key = ""
			
				If excludeClass = 0
					ListAddFirst(cList, pCourse)
					WriteLine(nfile, pCourse.BB_external_course_key + "|" + pCourse.BB_course_id + "|" + pCourse.BB_course_name + "|" + pCourse.BB_master_course_key + "|" + pCourse.BB_data_source_key + "|" + pCourse.BB_Available_ind)
				EndIf
				lastSection = Left(nCourse.IN_sectionNumber, Len(nCourse.IN_sectionNumber) - 4)
			EndIf
		
		
		
			nCourse.BB_course_name = nCourse.IN_sectionName + "-P" + nCourse.IN_period
			'nCourse.BB_data_source_key
			nCourse.BB_external_course_key = nCourse.IN_sectionNumber
			nCourse.BB_master_course_key = lastSection + ".parent"
			
		
		If excludeClass = 0
			WriteLine(nfile, nCourse.BB_external_course_key + "|" + nCourse.BB_course_id + "|" + nCourse.BB_course_name + "|" + nCourse.BB_master_course_key + "|" + nCourse.BB_data_source_key + "|" + nCourse.BB_Available_ind)
		Else
			WriteLine(nfile, nCourse.BB_external_course_key + "|" + nCourse.BB_course_id + "|" + nCourse.BB_course_name + "|" + "|" + nCourse.BB_data_source_key + "|" + nCourse.BB_Available_ind)
		EndIf
		excludeClass = 0
	Next
	CloseFile(nfile)
End Function

Function saveEnrollment()
	
	DeleteFile("data\ENROLLMENT.txt")
	Local teList:TList = New TList
	Local nfile:TStream = WriteFile("data\ENROLLMENT.txt")
	WriteLine(nfile, "external_course_key|external_person_key|role|available_ind|data_source_key")
	For Local nRoll:Enrollment = EachIn eList
		Local excludeClass = 0
		nRoll.BB_external_course_key = nRoll.IN_section
		nRoll.BB_external_person_key = nRoll.IN_stateIDNumber
		nRoll.BB_role = "student"
		nRoll.BB_available_ind = enrollmentsEnabled
		
		WriteLine(nfile, nRoll.BB_external_course_key ..
		+ "|" + nRoll.BB_external_person_key ..
		+ "|" + nRoll.BB_role ..
		+ "|" + nRoll.BB_available_ind ..
		+ "|" + nRoll.BB_data_source_key)
		
		Local teach:String
		
		For Local cID:String = EachIn excludeList
			If Instr(nRoll.BB_external_course_key, cID) '(nRoll.BB_external_course_key, Len(nRoll.BB_external_course_key) - 4) = cID
				'Print nRoll.BB_external_course_key
				excludeClass = 1
			EndIf
		Next
		
		If excludeClass = 0
			teach = Left(nRoll.BB_external_course_key, Len(nRoll.BB_external_course_key) - 4) + ".parent" ..
			+ "|" + nRoll.IN_TNum ..
			+ "|" + "instructor" ..
			+ "|" + nRoll.BB_available_ind ..
			+ "|" + nRoll.BB_data_source_key
		Else
			teach = nRoll.BB_external_course_key ..
			+ "|" + nRoll.IN_TNum ..
			+ "|" + "instructor" ..
			+ "|" + nRoll.BB_available_ind ..
			+ "|" + nRoll.BB_data_source_key
		EndIf
		Local dup = 0
		For Local Str:String = EachIn teList
			If teach = Str
				dup = 1
				Exit
			EndIf
		Next
		If dup = 0
			teList.AddLast(teach)
		End If
	Next
	For Local teach:String = EachIn teList
		WriteLine(nfile, teach)
	Next
	enrollInTraining(nfile)
	CloseFile(nfile)
End Function

Function charFix:String(str:String)
	str = Replace(str, " ", "")
	str = Replace(str, "-", "")
	'str = Replace(str, "'", "")
	str = Replace(str, "(", "")
	Str = Replace(Str, ")", "")
	Return str
End Function

Function saveUsers()
	DeleteFile("data\USERS.txt")
	
	Local nfile:TStream = WriteFile("data\USERS.txt")'OpenFile("data\CLASSES.txt")
	WriteLine(nfile, "External_Person_Key|User_ID|Passwd|Firstname|Lastname|Email|Institution_Role|System_Role|Available_ind|row_status|Data_Source_Key")'
	For Local nStaff:Staff = EachIn staList
		nStaff.BB_external_person_key = nStaff.IN_referenceCode'nStaff.IN_referenceCode
		nStaff.BB_user_ID = charFix(nStaff.IN_firstName) + "." + charFix(nStaff.IN_lastName)
		nStaff.BB_passwd = "12345"
		nStaff.BB_firstName = charFix(nStaff.IN_firstName)
		nStaff.BB_lastName = charFix(nStaff.IN_lastName)
		nStaff.BB_email = nStaff.IN_userName
		'nStaff.BB_instution_role
		'nStaff.BB_system_role
		nStaff.BB_available_ind = teachersEnabled
		nStaff.BB_row_status = "enabled"
		'nStaff.BB_data_source_key
		
		WriteLine(nfile, nStaff.BB_external_person_key ..
		+ "|" + nStaff.BB_user_ID ..
		+ "|" + nStaff.BB_passwd ..
		+ "|" + nStaff.BB_firstName ..
		+ "|" + nStaff.BB_lastName ..
		+ "|" + nStaff.BB_email ..
		+ "|" + nStaff.BB_instution_role ..
		+ "|" + nStaff.BB_system_role ..
		+ "|" + nStaff.BB_available_ind ..
		+ "|" + nStaff.BB_row_status ..
		+ "|" + nStaff.BB_data_source_key)'nStaff.BB_data_source_key
	Next
	
	For Local nStu:Student = EachIn stuList
		nStu.BB_external_person_key = nStu.IN_referenceCode'nStu.IN_referenceCode
		nStu.BB_user_ID = charFix(nStu.IN_firstName) + "." + charFix(nStu.IN_lastName)
		nStu.BB_passwd = "12345"
		nStu.BB_firstName = charFix(nStu.IN_firstName)
		nStu.BB_lastName = charFix(nStu.IN_lastName)
		nStu.BB_email = charFix(nStu.IN_firstName) + "." + charFix(nStu.IN_lastName) + "@tcstudent.com"
		'nStu.BB_instution_role
		'nStu.BB_system_role
		nStu.BB_available_ind = studentsEnabled
		If nStu.IN_enrollmentStatus = "CurrentlyEnrolled"
			nStu.BB_row_status = "enabled"
		Else
			nStu.BB_row_status = "disabled"
		EndIf
		'nStaff.BB_data_source_key
		
		WriteLine(nfile, nStu.BB_external_person_key ..
		+ "|" + nStu.BB_user_ID ..
		+ "|" + nStu.BB_passwd ..
		+ "|" + nStu.BB_firstName ..
		+ "|" + nStu.BB_lastName ..
		+ "|" + nStu.BB_email ..
		+ "|" + nStu.BB_instution_role ..
		+ "|" + nStu.BB_system_role ..
		+ "|" + nStu.BB_available_ind ..
		+ "|" + nStu.BB_row_status ..
		+ "|" + nStu.BB_data_source_key)'nStu.BB_data_source_key)
	Next
	CloseFile(nfile)
End Function









Function convertFromCSV:TList(line:String)
	Local lineArray:TList = New TList
	Local word:String
	Local letter:String
	Local i:Int = Len(line)
	Repeat
		letter = Left(line, 1)
		If letter = "|"
			ListAddLast(lineArray, Trim(word))
			word = ""
		EndIf
		If letter <> "|"
			word = word + letter
		EndIf
		i = i - 1
		line = Right(line, i)
	Until line = ""
	ListAddLast(lineArray, Trim(word))
	Return lineArray
End Function

Function loadCourse(file:TStream)
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[5]
		Local dataList:TList = convertFromCSV(ReadLine(file))
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local thisCourse:Course = New Course
		cList.AddLast(thisCourse)
		thisCourse.IN_sectionNumber = data[0]
		'print(thisCourse.IN_sectionNumber)
		thisCourse.IN_sectionName = data[1]
		'print(thisCourse.IN_sectionName)
		thisCourse.IN_startingGrade = data[2]
		'print(thisCourse.IN_startingGrade)
		thisCourse.IN_term = data[3]
		'print(thisCourse.IN_term)
		thisCourse.IN_period = data[4]
		'print(thisCourse.IN_period)
	Until Eof(file)
	CloseFile(file)
End Function

Function loadEnrollment(file:TStream)
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[7]
		Local dataList:TList = convertFromCSV(ReadLine(file))
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nroll:Enrollment = New Enrollment
		eList.AddLast(nroll)
		nroll.IN_section = data[0]
		'print nroll.IN_section
		nroll.IN_stateIDNumber = data[1]
		'print nroll.IN_stateIDNumber
		'nRoll.IN_LName = 'data[2]
		''print nroll.IN_LName
		nroll.IN_FName = data[2]
		'print nroll.IN_FName
		nroll.IN_TNum = data[3]
		'print nroll.IN_TNum
		nroll.IN_Teacher = data[4]
		'print nroll.IN_Teacher
		nroll.IN_SchoolNumber = data[5]
		'print nroll.IN_SchoolNumber
		nroll.IN_AcademicSession = data[6]
		'print nroll.IN_AcademicSession
	Until Eof(file)
	CloseFile file
End Function

Function loadStaff(file:TStream)
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[4]
		Local dataList:TList = convertFromCSV(ReadLine(file))
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nStaff:Staff = New Staff
		staList.AddLast(nStaff)
		nStaff.IN_referenceCode = data[0]
		'print nStaff.IN_referenceCode
		nStaff.IN_lastName = data[1]
		'print nStaff.IN_lastName
		nStaff.IN_firstName = data[2]
		'print nStaff.IN_firstName
		nStaff.IN_userName = data[3]
		'print nStaff.IN_userName
	Until Eof(file)
	CloseFile file
End Function

Function loadStudent(file:TStream)
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[6]
		Local dataList:TList = convertFromCSV(ReadLine(file))
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nStu:Student = New Student
		stuList.addLast(nStu)
		nStu.IN_lastName = data[0]
		nStu.IN_firstName = data[1]
		nStu.IN_middleName = data[2]
		nStu.IN_institution = data[3]
		nStu.IN_referenceCode = data[4]
		nStu.IN_enrollmentStatus = data[5]
	Until Eof(file)
	CloseFile file
End Function

Function enrollInTraining(nfile:TStream)
	For Local listu:Student = EachIn stuList
		WriteLine(nfile, "stuit" ..
		+ "|" + listu.BB_external_person_key ..
		+ "|" + "student" ..
		+ "|" + "Y" ..
		+ "|" + schoolYear + "_Courses")
	Next
End Function