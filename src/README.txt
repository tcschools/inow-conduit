How it Works:

Scheduled task at 7PM downloads the Exports from INow for BlackBoard
Scheduled task at 9PM converts the exports to their proper application formats by running the Integrator program
Scheduled task at 10PM runs command to upload the BlackBoard files
Scheduled task at 12AM runs JSSConduit to upload casper data.
Scheduled task at 1am sends email_process.txt to Yankee.
Scheduled task on yankee at 2am creates student ad accounts.

To stop teachers from being enrolled in last years classes, rename the folder in the BB Export to something other than lastYear
To get students to be enabled along with teachers, disable summerMode in the config by adding a * next to it.
To add a class that does not need to be bundled, add it to the bottom of the config file.
