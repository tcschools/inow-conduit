'Casper Export

'Directory Variable
Global CasperExport_data:String = programDir + "/Casper Export/data/"

'Type
Type casperClassEnroll
	Field className:String
	Field group:String = "NULL"
	Field siteName:String = "HS Students"
	Field studentUsername:String
	Field teacherUsername:String
	Field meetingTimes:String = "M T W Th F"
	Field startTime:String
	Field endTime:String
	Field appleTv:String = ""
End Type

'Lists
Global casperEnrollList:TList = New TList

'Export
Function doCasperProcessExport()
	'ISS
	
	'Special Teachers
	
	'Admin
	Local adminCasperTallasseeHigh:casperClassEnroll = New casperClassEnroll
	ListAddLast(casperEnrollList, adminCasperTallasseeHigh)
	
	adminCasperTallasseeHigh.ClassName = "Tallassee High"
	
	For Local checkStudent:InowStudent = EachIn InowStudentList
		If adminCasperTallasseeHigh.studentUsername = ""
			adminCasperTallasseeHigh.studentUsername = checkStudent.firstName + "." + checkStudent.lastName
		Else
			adminCasperTallasseeHigh.studentUsername = adminCasperTallasseeHigh.studentUsername + "," + checkStudent.firstName + "." + checkStudent.lastName
		End If
	Next
	
	adminCasperTallasseeHigh.teacherUsername = "ryan.dean,jennifer.miller"
	adminCasperTallasseeHigh.startTime = "12:00 AM"
	adminCasperTallasseeHigh.endTime = "11:59 PM"
	
	'General Classes
	If Not summerMode

		For Local nextINowEnroll:InowEnrollment = EachIn InowEnrollList
			Local tmpCasper:casperClassEnroll = New casperClassEnroll
			ListAddLast(casperEnrollList, tmpCasper)
			
			tmpCasper.className = nextINowEnroll.section
			
			For Local checkStudent:InowStudent = EachIn InowStudentList
				If checkStudent.studentID = nextINowEnroll.stateId
					tmpCasper.studentUsername = checkStudent.firstName + "." + checkStudent.lastName
					Exit
				End If
			Next
			
			For Local checkTeacher:InowStaff = EachIn InowStaffList
				If checkTeacher.teacherID = nextInowEnroll.teacherNum
					tmpCasper.teacherUsername = Left(checkTeacher.email, Len(checkTeacher.email) - 14)' checkTeacher.firstName + "." + checkTeacher.lastName
				End If
			Next
			
			For Local checkCourse:InowCourse = EachIn InowCourseList
				If checkCourse.sectionNumber = nextINowEnroll.section
					tmpCasper.className = "P" + checkCourse.period + " " + checkCourse.longName + ""
					Select checkCourse.period
						Case "1"
							tmpCasper.startTime = "7:50 AM"
							tmpCasper.endTime = "8:44 AM"
						Case "2"
							tmpCasper.startTime = "8:48 AM"
							tmpCasper.endTime = "9:42 AM"
						Case "3"
							tmpCasper.startTime = "9:48 AM"
							tmpCasper.endTime = "10:42 AM"
						Case "4"
							tmpCasper.startTime = "10:46 AM"
							tmpCasper.endTime = "11:40 AM"
						Case "5"
							tmpCasper.startTime = "11:44 AM"
							tmpCasper.endTime = "1:17 PM"
						Case "6"
							tmpCasper.startTime = "1:23 PM"
							tmpCasper.endTime = "2:17 PM"
						Case "7"
							tmpCasper.startTime = "2:21 PM"
							tmpCasper.endTime = "3:15 PM"
					End Select
					Exit
				End If
			Next
			
			
		Next
	EndIf
End Function

Function doCasperExport()
	DeleteFile(CasperExport_data + "thsClass.txt")
	
	Local casperFile:TStream = WriteFile(CasperExport_data + "thsClass.txt")
	WriteLine(casperFile, "class_name" + tabString + "jss_group" + tabString + "site_name" + tabString + "student_usernames" + tabString + "teacher_usernames" + tabString + "meeting_day" + tabString + "start_time" + tabString + "end_time" + tabString + "apple_tv_udids")
	
	For Local tmpCasper:casperClassEnroll = EachIn casperEnrollList
		WriteLine(casperFile, tmpCasper.ClassName + tabString + tmpCasper.group + tabString + tmpCasper.siteName + tabString + tmpCasper.studentUsername.Replace(" ", "") + tabString + tmpCasper.teacherUsername.Replace(" ", "") + tabString + tmpCasper.meetingTimes + tabString + tmpCasper.startTime + tabString + tmpCasper.endTime + tabString + tmpCasper.appleTv)
	Next
	
	CloseFile(casperFile)
End Function

Function doCasperSend()
	writeLog("[Casper] Casper files are set automatically on schedule through JSSConduit")
End Function