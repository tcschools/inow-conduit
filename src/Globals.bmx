'Integrator Globals
'Must be called first

'Get launch directory
Global programDir:String = CurrentDir:String()

'Start Log before anything
startLog()


'CONFIG FILE

'Open configuration file and set variables
Local configFile:TStream = OpenFile("config.txt")
	writeLog("====================CONFIG FILE VARIABLES====================")
	'hideApp
	Global hideApp = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		hideApp = True
		writeLog("==| hideApp = true")
	Else
		writeLog("==| hideApp = false")
	EndIf

	'debugApp
	Global debugApp = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		debugApp = True
		writeLog("==| debugApp = true")
	Else
		writeLog("==| debugApp = false")
	EndIf

	'teachersEnabled
	Global teachersEnabled = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		teachersEnabled = True
		writeLog("==| teachersEnabled = true")
	Else
		writeLog("==| teachersEnabled = false")
	EndIf

	'studentsEnabled
	Global studentsEnabled = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		studentsEnabled = True
		writeLog("==| studentsEnabled = true")
	Else
		writeLog("==| studentsEnabled = false")
	EndIf

	'enrollmentsEnabled
	Global enrollmentsEnabled = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		enrollmentsEnabled = True
		writeLog("==| enrollmentsEnabled = true")
	Else
		writeLog("==| enrollmentsEnabled = false")
	EndIf

	'summerMode
	Global summerMode = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		summerMode = True
		writeLog("==| summerMode = true")
	Else
		writeLog("==| summerMode = false")
	EndIf
	
	'lastYearEnabled
	Global lastYearEnabled = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		lastYearEnabled = True
		writeLog("==| lastYearEnabled = true")
	Else
		writeLog("==| lastYearEnabled = false")
	EndIf

	'courseSuffix
	Global courseSuffix:String = Upper(Trim(Right(ReadLine(configFile), 4)))
	writeLog("==| courseSuffix = " + courseSuffix)
	
	Global semester:String = Upper(Trim(Right(ReadLine(configFile), 1)))
	writeLog("==| semester = " + semester)
	
	Global onActiveSem = False
	If Upper(Trim(Right(ReadLine(configFile), 4))) = Upper(Trim("true"))
		onActiveSem = True
		writeLog("==| onActiveSem = true")
	Else
		writeLog("==| onActiveSem = false")
	EndIf
	
	'=====BlackBoard Parent Class Exclude List=====
	ReadLine(configFile)
	ReadLine(configFile)
	
	'excludeList
	Global excludeList:TList = New TList
	writeLog("=====BlackBoard Parent Class Exclude List=====")
	
	While Not Eof(configFile)
		Local tmpExcludeListEntry:String = ReadLine(configFile)
		ListAddLast(excludeList, tmpExcludeListEntry)
		writeLog("==| " + courseSuffix + "." + tmpExcludeListEntry + " will not be parented in BlackBoard.")
	Wend


CloseFile(configFile)


'Useful for tabing in print
Global tabString:String = "	"







