'Main Integrator Functions
'Must be called right before main.

'Import Functions
Function doImport()
	writeLog("[Import] Information Now Import")
	doInowImport()
End Function

'Data processing functions
Function doProcess()
	'Process all Imports first
	writeLog("[Process] Processing Inow Import")
	doInowProcessImport()
	
	'Then process exports
	writeLog("[Process] Processing BlackBoard Export")
	doBBProcessExport()
	
	writeLog("[Process] Processing Casper Export")
	doCasperProcessExport()
	
	writeLog("[Process] Processing Google Export")
	doGoogleProcessExport()
End Function

'The write data to file
Function doExport()
	writeLog("[Export] Writing BlackBoard data to file")
	doBBExport()
	
	writeLog("[Export] Writing Casper data to file")
	doCasperExport()
	
	writeLog("[Export] Writing Google data to file")
	doGoogleExport()
End Function

'Send data to proper locations
Function doSend()
	writeLog("[Send] Sending BlackBoard Data")
	doBBSend()
	
	writeLog("[Send] Sending Casper Data")
	doCasperSend()
	
	writeLog("[Send] Sending Google Data")
	doGoogleSend()
End Function

'Misc Functions
Function convertFromCSV:TList(line:String, delim:String)
	Local lineArray:TList = New TList
	Local word:String
	Local letter:String
	Local i:Int = Len(line)
	Repeat
		letter = Left(line, 1)
		If letter = delim '"|"
			ListAddLast(lineArray, Trim(word))
			word = ""
		EndIf
		If letter <> delim '"|"
			word = word + letter
		EndIf
		i = i - 1
		line = Right(line, i)
	Until line = ""
	ListAddLast(lineArray, Trim(word))
	Return lineArray
End Function
