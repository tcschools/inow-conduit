'Integrator for INow exports for blackboard, casper and others
'Must be called last

writeLog("====================Integrator Execution====================")

'Do all Imports
writeLog("[INFO] Starting imports.")
doImport()

'Process all data for export
writeLog("[INFO] Processing imports in memory.")
doProcess()

'Export all data to files
writeLog("[INFO] Writing data to file.")
doExport()

'send files to proper places.
writeLog("[INFO] Sending data.")
doSend()

'Extra function Jim needed.
writeLog("[INFO] Running extra DirectCert funtion for Jim.")
runDC()
		
'write log to file	
killLog()

End









