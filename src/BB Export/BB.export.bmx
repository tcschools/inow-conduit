'Export for Blackboard

'Blackboard Directory
Global BBExport_data:String = programDir + "/BB Export/data/"

'Lists
Global bbClassList:TList = New TList
Global bbEnrollList:TList = New TList
Global bbUserList:TList = New TList
Global bbTeachEnrollList:TList = New TList

Global outdatedClass:TList = New TList

'Classes
Type bbClass
	Field externalCourseKey:String
	Field courseID:String
	Field courseName:String
	Field courseParent:String
	Field dataSourceKey:String
	Field isAvailable:String
End Type

Type bbEnroll
	Field externalCourseKey:String
	Field externalPersonKey:String
	Field role:String
	Field isAvailable:String
	Field dataSourceKey:String
End Type

Type bbUser
	Field externalPersonKey:String
	Field userID:String
	Field password:String
	Field firstName:String
	Field lastName:String
	Field email:String
	Field role:String
	Field systemRole:String
	Field isAvailable:String
	Field isEnabled:String
	Field dataSourceKey:String
End Type

'Process BlackBoard data
Function doBBProcessExport()
	writeLog("[BlackBoard] Processing Classes")
	BBProcessClasses()
	
	writeLog("[BlackBoard] Processing Enrollments")
	BBProcessEnrollment()
	
	writeLog("[BlackBoard] Optimizing Class Parenting")
	BBFixDoubleTeachers()
	
	writeLog("[BlackBoard] Processing Users")
	BBProcessUsers()
End Function

'Export data to file
Function doBBExport()
	BBExportClasses()
	BBExportEnroll()
	BBExportUsers()
End Function

'Send data
Function doBBSend()
	writeLog("[BlackBoard] Running " + BBExport_data + "command.bat")
	system_(BBExport_data + "command.bat")
End Function

'Optimize Class Parenting
Function BBFixDoubleTeachers()
	Local checkedClasses:TList = New TList
	
	Local splitParents:TList = New TList
	
	Local num = 2
	
	For Local tmpBBEnroll:bbEnroll = EachIn bbEnrollList
		If tmpBBEnroll.role = "instructor"
			writeLog(tmpBBEnroll.externalPersonKey + " - " + tmpBBEnroll.externalCourseKey)
			For Local thisClass:String = EachIn checkedClasses
				If thisClass = tmpBBEnroll.externalCourseKey
					'spacer
					
					
					'Find old parent
					Local oldParent:bbClass
					Local skip = True
					For oldParent = EachIn bbClassList
						If oldParent.externalCourseKey = tmpBBEnroll.externalCourseKey
							writeLog("===[Optimize] " + oldParent.courseName + " (" + oldParent.courseID + ") already has a teacher, creating parent class for new teacher.")
							skip = False
							Exit
						EndIf
					Next
					If skip
						Continue
					End If
					'WaitKey()
					
					
					'Check if more than 2 teachers
					Local useCourseId:String = oldParent.courseID
					num = 2
					For Local otherSplit:String = EachIn splitParents
						If otherSplit = useCourseId
							useCourseId = Left(useCourseId, Len(useCourseId) - 1)
							num = num + 1
							writeLog("===[Info] This class has more than just 2 teachers. This teacher is the " + num + " teacher teaching this section.")
							Exit
						EndIf
					Next
					
					
					
'					If lastSection = oldParent.courseID
'						num = num + 1
'						writeLog("===[Info] This class has more than just 2 teachers. This teacher is the " + num + " teacher teaching this section.")
'					Else
'						num = 2
'					EndIf
'					lastSection = oldParent.courseID
					
					'Create parent2 class
					Local parent2Class:bbClass = New bbClass
					ListAddFirst(bbClassList, parent2Class)
					parent2Class.courseID = useCourseId + num
					parent2Class.courseName = oldParent.courseName
					parent2Class.externalCourseKey = parent2Class.courseID
					parent2Class.dataSourceKey = oldParent.dataSourceKey
					parent2Class.courseParent = ""
					parent2Class.isAvailable = "Y"
					writeLog("===[Info] Created new parent class container: " + parent2Class.courseID)
					
					ListAddLast(splitParents, parent2Class.courseID)
					
					If Left(parent2Class.courseID, 4) = "SEM1"
						Local parent3Class:bbClass = New bbClass
						ListAddFirst(bbClassList, parent3Class)
						parent3Class.courseID = "SEM2." + Right(useCourseId, Len(useCourseId) - 5) + num
						parent3Class.courseName = "SEM2 " + Right(oldParent.courseName, Len(oldParent.courseName) - 5)
						parent3Class.externalCourseKey = parent3Class.courseID
						parent3Class.dataSourceKey = oldParent.dataSourceKey
						parent3Class.courseParent = ""
						parent3Class.isAvailable = "Y"
						writeLog("===[Info] Created new parent class container: " + parent3Class.courseID)
					EndIf
					
					If Left(parent2Class.courseID, 4) = "SEM2"
						Local parent3Class:bbClass = New bbClass
						ListAddFirst(bbClassList, parent3Class)
						parent3Class.courseID = "SEM1." + Right(useCourseId, Len(useCourseId) - 5) + num
						parent3Class.courseName = "SEM1 " + Right(oldParent.courseName, Len(oldParent.courseName) - 5)
						parent3Class.externalCourseKey = parent3Class.courseID
						parent3Class.dataSourceKey = oldParent.dataSourceKey
						parent3Class.courseParent = ""
						parent3Class.isAvailable = "Y"
						writeLog("===[Info] Created new parent class container: " + parent3Class.courseID)
					EndIf
					
					Local teacherName:String
					'Search INow enrollments to find matching section, if teacher matches change BBEnroll counterpart to have parent2
					For Local checkInowCourse:InowCourse = EachIn InowCourseList
						If Instr(Left(parent2Class.courseID, Len(parent2Class.courseID) - 8), courseSuffix + "." + Left(checkInowCourse.sectionNumber, Len(checkInowCourse.sectionNumber) - 4))
							For Local checkInowEnroll:InowEnrollment = EachIn InowEnrollList
								If checkInowEnroll.section = checkInowCourse.sectionNumber
									If checkInowEnroll.teacherNum = tmpBBEnroll.externalPersonKey
										teacherName = checkInowEnroll.teacher
										writeLog("----[Period] " + teacherName + " teaches period " + checkInowCourse.period + " of " + checkInowCourse.longName + " for the year " + courseSuffix)
										
										'Find section in bbClass
										'Change parent of this section to new parent
										For Local checkBBClass:bbClass = EachIn bbClassList
											If checkBBClass.externalCourseKey = courseSuffix + "." + checkInowCourse.sectionNumber
												checkBBClass.courseParent = parent2Class.courseID
												
												If Not Instr(checkInowCourse.term, ",")
			
													If Instr(checkInowCourse.term, "1st Semester")
														checkBBClass.courseParent = "SEM1." + Right(checkBBClass.courseParent, Len(checkBBClass.courseParent) - 5)
													ElseIf Instr(checkInowCourse.term, "2nd Semester")
														checkBBClass.courseParent = "SEM2." + Right(checkBBClass.courseParent, Len(checkBBClass.courseParent) - 5)
													EndIf
												EndIf
												
												
												writeLog("----[Info] Found original class in database and set parent value to " + checkBBClass.courseParent)
												Exit
											EndIf
										Next
										
										
										
										Exit
									End If
								End If
							Next
						EndIf
					Next
					
					
					
					'Enroll teacher in new Parent
					'Remove teacher from old parent
					tmpBBEnroll.externalCourseKey = parent2Class.externalCourseKey
					
					
					If Left(tmpBBEnroll.externalCourseKey, 4) = "SEM1"
						Local sem2Enroll:bbEnroll = New bbEnroll
						sem2Enroll.dataSourceKey = tmpBBEnroll.dataSourceKey
						sem2Enroll.externalCourseKey = "SEM2" + Right(tmpBBEnroll.externalCourseKey, Len(tmpBBEnroll.externalCourseKey) - 4)
						sem2Enroll.externalPersonKey = tmpBBEnroll.externalPersonKey
						sem2Enroll.isAvailable = tmpBBEnroll.isAvailable
						sem2Enroll.role = tmpBBEnroll.role
						ListAddFirst(bbEnrollList, sem2Enroll)
						
						writeLog("===[BlackBoard] Enrolled " + teacherName + " as instructor of " + sem2Enroll.externalCourseKey)
					End If
					
					If Left(tmpBBEnroll.externalCourseKey, 4) = "SEM2"
						Local sem2Enroll:bbEnroll = New bbEnroll
						sem2Enroll.dataSourceKey = tmpBBEnroll.dataSourceKey
						sem2Enroll.externalCourseKey = "SEM1" + Right(tmpBBEnroll.externalCourseKey, Len(tmpBBEnroll.externalCourseKey) - 4)
						sem2Enroll.externalPersonKey = tmpBBEnroll.externalPersonKey
						sem2Enroll.isAvailable = tmpBBEnroll.isAvailable
						sem2Enroll.role = tmpBBEnroll.role
						ListAddFirst(bbEnrollList, sem2Enroll)
						
						writeLog("===[BlackBoard] Enrolled " + teacherName + " as instructor of " + sem2Enroll.externalCourseKey)
					End If
					
					writeLog("===[BlackBoard] Enrolled " + teacherName + " as instructor of " + tmpBBEnroll.externalCourseKey)
					
					

				EndIf
			Next
			ListAddLast(checkedClasses, tmpBBEnroll.externalCourseKey)
		End If
	Next
	writeLog("====================")
End Function

'Write Files to data
Function BBExportEnroll()
	DeleteFile(BBExport_data + "ENROLLMENT.txt")
	
	Local bbEnrollFile:TStream = WriteFile(BBExport_data + "ENROLLMENT.txt")
	WriteLine(bbEnrollFile, "external_course_key|external_person_key|role|available_ind|data_source_key")
	
	For Local tmpBBEnroll:bbEnroll = EachIn bbEnrollList
		Local writeCourse:String = tmpBBEnroll.externalCourseKey
		If Left(writeCourse, 3) <> "SEM" And Left(writeCourse, 4) <> courseSuffix
			writeCourse = courseSuffix + "." + writeCourse
		EndIf
	
	
		WriteLine(bbEnrollFile, writeCourse + "|" + tmpBBEnroll.externalPersonKey + "|" + tmpBBEnroll.role + "|" + tmpBBEnroll.isAvailable + "|" + tmpBBEnroll.dataSourceKey)
	Next
	
	If lastYearEnabled
		writeLog("[Info] Enrolling teachers in last years classes.")
		Local oldFile:TStream = OpenFile(BBExport_data + "..\lastYear\Enrollment.txt")
		ReadLine(oldFile)
		While Not Eof(oldFile)
			Local datatop:String = ReadLine(oldFile)
			If Instr(datatop, "instructor")
				WriteLine(bbEnrollFile, ReadLine(oldFile))
			EndIf
		Wend
		CloseFile(oldFile)
	EndIf
	
	CloseFile(bbEnrollFile)
End Function

Function BBExportClasses()
	DeleteFile(BBExport_data + "CLASSES.txt")
	
	Local bbClassFile:TStream = WriteFile(BBExport_data + "CLASSES.txt")
	WriteLine(bbClassFile, "external_course_key|course_id|course_name|master_course_key|data_source_key|Available_ind")
	
	For Local tmpBBClass:bbClass = EachIn bbClassList
		Local doExclude = 0
		
		For Local checkExclude:String = EachIn excludeList
			If Instr(tmpBBClass.courseID, checkExclude)
				doExclude = 1
			End If
		Next
		
						
		
		
		If doExclude = 0
			WriteLine(bbClassFile, tmpBBClass.externalCourseKey + "|" + tmpBBClass.courseID + "|" + tmpBBClass.courseName + "|" + tmpBBClass.courseParent + "|" + tmpBBClass.dataSourceKey + "|" + tmpBBClass.isAvailable)
		Else
			WriteLine(bbClassFile, tmpBBClass.externalCourseKey + "|" + tmpBBClass.courseID + "|" + tmpBBClass.courseName + "|" + "|" + tmpBBClass.dataSourceKey + "|" + tmpBBClass.isAvailable)
		EndIf
	Next
	
	If lastYearEnabled
		writeLog("[Info] Opening last years classes for access.")
		Local oldFile:TStream = OpenFile(BBExport_data + "..\lastYear\CLASSES.txt")
		ReadLine(oldFile)
		While Not Eof(oldFile)
			WriteLine(bbClassFile, ReadLine(oldFile))
		Wend
		CloseFile(oldFile)
	EndIf
	
	CloseFile(bbClassFile)
	
End Function

Function BBExportUsers()
	DeleteFile(BBExport_data + "USERS.txt")
	
	Local bbUserFile:TStream = WriteFile(BBExport_data + "USERS.txt")
	WriteLine(bbUserFile, "External_Person_Key|User_ID|Passwd|Firstname|Lastname|Email|Institution_Role|System_Role|Available_ind|row_status|Data_Source_Key")
	
	For Local tmpBBUser:bbUser = EachIn bbUserList
		WriteLine(bbUserFile, tmpBBUser.externalPersonKey + "|" + tmpBBUser.userID + "|" + tmpBBUser.password + "|" + tmpBBUser.firstName + "|" + tmpBBUser.lastName + "|" + tmpBBUser.email.Replace(" ", "") + "|" + tmpBBUser.role + "|" + tmpBBUser.systemRole + "|" + tmpBBUser.isAvailable + "|" + tmpBBUser.isEnabled + "|" + tmpBBUser.dataSourceKey)
	Next
	
	CloseFile(bbUserFile)
End Function

Function BBProcessUsers()
	
	For Local nextTeacher:InowStaff = EachIn InowStaffList
		Local tmpBBUser:bbUser = New bbUser
		ListAddLast(bbUserList, tmpBBUser)
		
		tmpBBUser.externalPersonKey = nextTeacher.teacherID
		tmpBBUser.userID = nextTeacher.firstName + "." + nextTeacher.lastName
		tmpBBUser.userID = tmpBBUser.userID.Replace(" ", "") 'Removes all spaces in the username
		tmpBBUser.password = "12345"
		tmpBBUser.firstName = nextTeacher.firstName
		tmpBBUser.lastName = nextTeacher.lastName
		tmpBBUser.email = nextTeacher.email
		tmpBBUser.role = "faculty"
		tmpBBUser.systemRole = "None"
		If teachersEnabled
			tmpBBUser.isAvailable = "Y"
		Else
			tmpBBUser.isAvailable = "N"
		EndIf
		tmpBBUser.isEnabled = "enabled"
		tmpBBUser.dataSourceKey = "1415_Users"
	Next
	
	For Local nextStudent:InowStudent = EachIn InowStudentList
		Local tmpBBUser:bbUser = New bbUser
		ListAddLast(bbUserList, tmpBBUser)
		
		tmpBBUser.externalPersonKey = nextStudent.studentID
		tmpBBUser.userID = nextStudent.firstName + "." + nextStudent.lastName
		tmpBBUser.userID = tmpBBUser.userID.Replace(" ", "")
		tmpBBUser.password = "12345"
		tmpBBUser.firstName = nextStudent.firstName
		tmpBBUser.lastName = nextStudent.lastName
		tmpBBUser.email = nextStudent.firstName + "." + nextStudent.lastName + "@tcstudent.com"
		tmpBBUser.role = "student"
		tmpBBUser.systemRole = "None"
		If studentsEnabled And Not summerMode
			tmpBBUser.isAvailable = "Y"
		Else
			tmpBBUser.isAvailable = "N"
		EndIf
		tmpBBUser.isEnabled = "enabled"
		tmpBBUser.dataSourceKey = "1415_Users"
	Next
End Function

Function BBProcessEnrollment()
	Local studentEnrollment:TList = New TList
	Local teacherEnrollment:TList = New TList

	'Do this for every INow Enrollment
	For Local nextInowEnrollment:InowEnrollment = EachIn InowEnrollList
	
		
	
		'Is class excluded from bundling
		Local doExclude = False
		
		For Local checkExclude:String = EachIn excludeList
			If Instr(nextInowEnrollment.section, checkExclude)
				writeLog("EXCLUDE CLASS FOUND")
				doExclude = True
			End If
		Next
		
		'Create Enrollment Object
		Local tmpBBEnroll:bbEnroll = New bbEnroll
		
		'Set variables that are static
		ListAddLast(bbEnrollList, tmpBBEnroll)
		
		'Create Student Enrollment
		tmpBBEnroll.externalCourseKey = nextInowEnrollment.section
		tmpBBEnroll.role = "student"
		tmpBBEnroll.externalPersonKey = nextInowEnrollment.stateId
		If studentsEnabled
			tmpBBEnroll.isAvailable = "Y"
		Else
			tmpBBEnroll.isAvailable = "N"
		EndIf
		tmpBBEnroll.dataSourceKey = courseSuffix + "_Courses"
		
		writeLog("Student: " + tmpBBEnroll.externalPersonKey + " Class: " + tmpBBEnroll.externalCourseKey)
		ListAddLast(studentEnrollment, tmpBBEnroll)
		
		'Set dynamic variables based on role.
		Local isNewTeach = True
		If Not doExclude
			'Check if Teacher has been enrolled
			'If not create Teacher Enrollment
			For Local testIsTeach:InowStaff = EachIn InowStaffList
				If nextInowEnrollment.teacherNum = testIsTeach.teacherID
					Local tmpTeachEnroll:bbEnroll = New bbEnroll
					
					tmpTeachEnroll.role = "instructor"
					tmpTeachEnroll.externalPersonKey = testIsTeach.teacherID
					
					For Local semesterClass:bbClass = EachIn bbClassList
						If semesterClass.externalCourseKey = courseSuffix + "." + nextInowEnrollment.section
							tmpTeachEnroll.externalCourseKey = semesterClass.courseParent
							
							If Left(tmpTeachEnroll.externalCourseKey, 4) = "SEM1"
								Local course:String = Right(tmpTeachEnroll.externalCourseKey, Len(tmpTeachEnroll.externalCourseKey) - 4)
								course = "SEM2" + course
								Local tmpTeach2Enroll:bbEnroll = New bbEnroll
								tmpTeach2Enroll.role = "instructor"
								tmpTeach2Enroll.externalPersonKey = testIsTeach.teacherID
								tmpTeach2Enroll.externalCourseKey = course
								If teachersEnabled
									tmpTeach2Enroll.isAvailable = "Y"
								Else
									tmpTeach2Enroll.isAvailable = "N"
								EndIf
								tmpTeach2Enroll.dataSourceKey = courseSuffix + "_Courses"
								
								For Local checkDouble:bbEnroll = EachIn teacherEnrollment
									If checkDouble.externalPersonKey = tmpTeach2Enroll.externalPersonKey And checkDouble.externalCourseKey = tmpTeach2Enroll.externalCourseKey
										isNewTeach = False
									EndIf
								Next
								If isNewTeach
									ListAddLast(bbEnrollList, tmpTeach2Enroll)
									ListAddLast(teacherEnrollment, tmpTeach2Enroll)
									writeLog("Teacher: " + tmpTeach2Enroll.externalPersonKey + " Class: " + tmpTeach2Enroll.externalCourseKey)
								EndIf
								isNewTeach = True
							EndIf
							Exit
						EndIf
					Next
					
					'tmpTeachEnroll.externalCourseKey = Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + ".parent"
					
					If teachersEnabled
						tmpTeachEnroll.isAvailable = "Y"
					Else
						tmpTeachEnroll.isAvailable = "N"
					EndIf
					tmpTeachEnroll.dataSourceKey = courseSuffix + "_Courses"
					
					For Local checkDouble:bbEnroll = EachIn teacherEnrollment
						If checkDouble.externalPersonKey = tmpTeachEnroll.externalPersonKey And checkDouble.externalCourseKey = tmpTeachEnroll.externalCourseKey
							isNewTeach = False
						EndIf
					Next
					If isNewTeach
						ListAddLast(bbEnrollList, tmpTeachEnroll)
						ListAddLast(teacherEnrollment, tmpTeachEnroll)
						writeLog("Teacher: " + tmpTeachEnroll.externalPersonKey + " Class: " + tmpTeachEnroll.externalCourseKey)
					EndIf
				End If
			Next
		Else
			'Check if Teacher has been enrolled in Parent
			'If not Enroll Teacher in Parent
			For Local testIsTeach:InowStaff = EachIn InowStaffList
				If nextInowEnrollment.teacherNum = testIsTeach.teacherID
					Local tmpTeachEnroll:bbEnroll = New bbEnroll
					
					tmpTeachEnroll.role = "instructor"
					tmpTeachEnroll.externalPersonKey = testIsTeach.teacherID
					
					tmpTeachEnroll.externalCourseKey = nextInowEnrollment.section
					
					If teachersEnabled
						tmpTeachEnroll.isAvailable = "Y"
					Else
						tmpTeachEnroll.isAvailable = "N"
					EndIf
					tmpTeachEnroll.dataSourceKey = courseSuffix + "_Courses"
					
					For Local checkDouble:bbEnroll = EachIn teacherEnrollment
						If checkDouble.externalPersonKey = tmpTeachEnroll.externalPersonKey And checkDouble.externalCourseKey = tmpTeachEnroll.externalCourseKey
							isNewTeach = False
						EndIf
					Next
					
					If isNewTeach
						ListAddLast(bbEnrollList, tmpTeachEnroll)
						ListAddLast(teacherEnrollment, tmpTeachEnroll)
						writeLog("Teacher: " + tmpTeachEnroll.externalPersonKey + " Class: " + tmpTeachEnroll.externalCourseKey)
					EndIf
				End If
			Next
		End If
		
		
	Next


'
''	writeLog("[Info] Cleaning up enrollment database after parent container creation.") 'Just a reminder telling me I erase multiple entries.
'	
'	For Local nextInowEnrollment:InowEnrollment = EachIn InowEnrollList
'	''	Local newTeach = True
'		'Local doExclude = 0
'	'	Local tmpBBEnroll:bbEnroll = New bbEnroll
'		
'		'For Local checkExclude:String = EachIn excludeList
'		'	If Instr(nextInowEnrollment.section, checkExclude)
'		'		doExclude = 1
'		'	End If
'	'	Next
'		
'	'	tmpBBEnroll.externalCourseKey = nextInowEnrollment.section
'	'	For Local testString:String = EachIn teacherEnrollment
'	'		If testString = nextInowEnrollment.section + nextInowEnrollment.teacherNum
'	'			newTeach = False
'	'		End If
'	'	Next
'		
'		
'		
'	'	Local isTeach = False
'		If newTeach = True
'			
'			'Erase multiple entries due to my lazy programming creating the multiple entries in the first place.
'	'		Local skipT = False
'	'		If Not doExclude
'	'			For Local doubleTeacher:String = EachIn teacherEnrollment
'	'				If doubleTeacher = Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + nextInowEnrollment.teacherNum
'	'					skipT = True
'						'writeLog("WARNING: Non-Exclude Double Entry skipping " + Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + " " + nextInowEnrollment.teacherNum)
'	'				EndIf
'	'			Next
'	'		Else
'	'			For Local doubleTeacher:String = EachIn teacherEnrollment
'	'				If doubleTeacher = nextInowEnrollment.section + nextInowEnrollment.teacherNum
'	'					skipT = True
'	'					writeLog("[WARNING] Exclude Double Entry skipping " + nextInowEnrollment.section + nextInowEnrollment.teacherNum)
'	'				EndIf
'	'			Next
'	'		EndIf
'			
'	'		If Not skipT
'				'ListAddLast(bbEnrollList, tmpBBEnroll)
'	'			If Not doExclude
'	'				ListAddLast(teacherEnrollment, Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + nextInowEnrollment.teacherNum)
'	'			Else
'	'				ListAddLast(teacherEnrollment, nextInowEnrollment.section + nextInowEnrollment.teacherNum)
'	'			EndIf
'	'		EndIf
'			
'			For Local testIsTeach:InowStaff = EachIn InowStaffList
'				
'				If nextInowEnrollment.teacherNum = testIsTeach.teacherID
'					'writeLog("BB Teacher:" + testIsTeach.teacherID + " Class:" + Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + ".parent")
'					tmpBBEnroll.role = "instructor"
'					tmpBBEnroll.externalPersonKey = testIsTeach.teacherID'nextInowEnrollment.stateId
'					isTeach = True
'					
'					If Not doExclude
'						tmpBBEnroll.externalCourseKey = Left(nextInowEnrollment.section, Len(nextInowEnrollment.section) - 4) + ".parent"
'					Else
'						tmpBBEnroll.externalCourseKey = nextInowEnrollment.section
'					EndIf
'					
'					If teachersEnabled
'						tmpBBEnroll.isAvailable = "Y"
'					Else
'						tmpBBEnroll.isAvailable = "N"
'					EndIf
'					Exit
'				End If
'				
'			Next
'			
'		End If
'		
'		
'		
'		If Not isTeach
'			writeLog("BB Student:" + tmpBBEnroll.externalPersonKey + " Class:" + tmpBBEnroll.externalCourseKey)
'			ListAddLast(bbEnrollList, tmpBBEnroll)
'			tmpBBEnroll.role = "student"
'			tmpBBEnroll.externalPersonKey = nextInowEnrollment.stateId
'			If studentsEnabled
'				tmpBBEnroll.isAvailable = "Y"
'			Else
'				tmpBBEnroll.isAvailable = "N"
'			EndIf
'		End If
'		
'		
'		
'		
'		
'		tmpBBEnroll.dataSourceKey = courseSuffix + "_Courses"'[CHANGEME] + courseSuffix
		
		'writeLog("=====")
		'writeLog(tmpBBEnroll.externalCourseKey)
		'writeLog(tmpBBEnroll.externalPersonKey)
		'writeLog(tmpBBEnroll.role)
		'writeLog(tmpBBEnroll.isAvailable)
		'writeLog(tmpBBEnroll.dataSourceKey)
		
'	Next
	
	
	
End Function

Function BBProcessClasses()
	Local parents:TList = New TList
	'Local lastTeacher
	For Local nextInowCourse:InowCourse = EachIn InowCourseList
		Local doExclude = 0
		Local tmpBBClass:bbClass = New bbClass
		ListAddLast(bbClassList, tmpBBClass)
		
		tmpBBClass.courseID = courseSuffix + "." + nextInowCourse.sectionNumber'[CHANGEME] + courseSuffix
		
		
		
		For Local checkExclude:String = EachIn excludeList
			If Instr(tmpBBClass.courseID, checkExclude)
				doExclude = 1
			End If
		Next
		
		'Is this a new section were in?
		If Not doExclude
			Local newParent = True
			For Local parentCheck:String = EachIn parents
				If parentCheck = Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
					newParent = False
				
					tmpBBClass.courseParent = parentCheck + ".parent"
				
					Exit
				ElseIf Instr(nextInowCourse.term, "1st Semester") And parentCheck = "SEM1." + Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
					newParent = False
				
					tmpBBClass.courseParent = parentCheck + ".parent"
				
					Exit
				ElseIf Instr(nextInowCourse.term, "2nd Semester") And parentCheck = "SEM2." + Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
					newParent = False
					tmpBBClass.courseParent = parentCheck + ".parent"
					
					Exit
				EndIf
			Next
		
			If newParent
				Local nextParent:bbClass = New bbClass
			
				nextParent.courseID = Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
				nextParent.courseName = nextInowCourse.longName
			
				If Not Instr(nextInowCourse.term, ",")
					If Instr(nextInowCourse.term, "1st Semester")
						nextParent.courseID = "SEM1." + nextParent.courseID
						nextParent.courseName = "SEM1 " + nextParent.courseName
					ElseIf Instr(nextInowCourse.term, "2nd Semester")
						nextParent.courseID = "SEM2." + nextParent.courseID
						nextParent.courseName = "SEM2 " + nextParent.courseName
					EndIf
					writeLog("[SEMESTER CLASS] " + nextParent.courseID + ".parent")
				EndIf
			
				ListAddLast(parents, nextParent.courseID)
			
				nextParent.courseID = nextParent.courseID + ".parent"
			
				If Not doExclude
					ListAddFirst(bbClassList, nextParent)
					tmpBBClass.courseParent = nextParent.courseID
				EndIf
			
				nextParent.externalCourseKey = nextParent.courseID
				nextParent.dataSourceKey = courseSuffix + "_Courses"
				nextParent.courseParent = ""
				nextParent.isAvailable = "Y"
			
			
			End If
		
		EndIf
		
		
		
'		If lastSection <> Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4) And lastSection <> "SEM1." + Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4) And lastSection <> "SEM2." + Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
'			Local nextSection:bbClass = New bbClass
'			
'			nextSection.courseID = Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4) + ".parent"'[CHANGEME] + courseSuffix
'			nextSection.courseName = nextInowCourse.longName'[CHANGEME] +courseSuffix
'			
'			If Not Instr(nextInowCourse.term, ",")
'			
'				If Instr(nextInowCourse.term, "1st Semester")
'					nextSection.courseID = "SEM1." + nextSection.courseID
'					nextSection.courseName = "SEM1 " + nextSection.courseName
'				ElseIf Instr(nextInowCourse.term, "2nd Semester")
'					nextSection.courseID = "SEM2." + nextSection.courseID
'					nextSection.courseName = "SEM2 " + nextSection.courseName
'				EndIf
'			EndIf
'			
'			
'			
'			
'			nextSection.externalCourseKey = nextSection.courseID
'			nextSection.dataSourceKey = courseSuffix + "_Courses"'[CHANGEME] + courseSuffix
'			nextSection.courseParent = ""
'			nextSection.isAvailable = "Y"
'			If Not doExclude
'				ListAddFirst(bbClassList, nextSection)
'			EndIf
'			lastSection = Left(tmpBBClass.courseID, Len(tmpBBClass.courseID) - 4)
'			
'			If Not Instr(nextInowCourse.term, ",")
'			
'				If Instr(nextInowCourse.term, "1st Semester")
'					lastSection = "SEM1." + lastSection
'				ElseIf Instr(nextInowCourse.term, "2nd Semester")
'					lastSection = "SEM2." + lastSection
'				EndIf
'			EndIf
'		End If
		
		
		tmpBBClass.externalCourseKey = tmpBBClass.courseID'courseSuffix + "." + nextInowCourse.sectionNumber'[CHANGEME] + courseSuffix
		tmpBBClass.courseName = nextInowCourse.longName + "-P" + nextInowCourse.period'[CHANGEME] + courseSuffix
		
		tmpBBClass.dataSourceKey = courseSuffix + "_Courses"'[CHANGEME] + courseSuffix
		tmpBBClass.isAvailable = "Y"
		
		
	Next
End Function