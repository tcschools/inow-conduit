external_course_key|course_id|course_name|master_course_key|data_source_key|Available_ind
200005aa.parent|200005aa.parent|English 9||1415_Courses|Y
200005aa.001|200005aa.001|English 9-P1|200005aa.parent|1415_Courses|Y
200005aa.002|200005aa.002|English 9-P2|200005aa.parent|1415_Courses|Y
200005aa.004|200005aa.004|English 9-P7|200005aa.parent|1415_Courses|Y
200007aa.parent|200007aa.parent|English 9 Advanced||1415_Courses|Y
200007aa.001|200007aa.001|English 9 Advanced-P3|200007aa.parent|1415_Courses|Y
200007aa.002|200007aa.002|English 9 Advanced-P4|200007aa.parent|1415_Courses|Y
200009aa.parent|200009aa.parent|English 10||1415_Courses|Y
200009aa.002|200009aa.002|English 10-P4|200009aa.parent|1415_Courses|Y
200009aa.003|200009aa.003|English 10-P1|200009aa.parent|1415_Courses|Y
200009aa.004|200009aa.004|English 10-P7|200009aa.parent|1415_Courses|Y
200017.001|200017.001|English 12-P1||1415_Courses|Y
200017.002|200017.002|English 12-P2||1415_Courses|Y
200017.004|200017.004|English 12-P6||1415_Courses|Y
200011aa.parent|200011aa.parent|English 10 Pre AP||1415_Courses|Y
200011aa.001|200011aa.001|English 10 Pre AP-P3|200011aa.parent|1415_Courses|Y
200011aa.002|200011aa.002|English 10 Pre AP-P6|200011aa.parent|1415_Courses|Y
200016.parent|200016.parent|AP Eng Lang  Comp||1415_Courses|Y
200016.001|200016.001|AP Eng Lang  Comp-P3|200016.parent|1415_Courses|Y
200020.parent|200020.parent|AP Eng Lit Comp||1415_Courses|Y
200020.001|200020.001|AP Eng Lit Comp-P2|200020.parent|1415_Courses|Y
802202aa.parent|802202aa.parent|Yearbook||1415_Courses|Y
802202aa.001|802202aa.001|Yearbook-P7|802202aa.parent|1415_Courses|Y
230016aa.parent|230016aa.parent|US History 10||1415_Courses|Y
230016aa.002|230016aa.002|US History 10-P3|230016aa.parent|1415_Courses|Y
230016aa.003|230016aa.003|US History 10-P5|230016aa.parent|1415_Courses|Y
230016aa.004|230016aa.004|US History 10-P6|230016aa.parent|1415_Courses|Y
230019aa.parent|230019aa.parent|US History 11||1415_Courses|Y
230019aa.002|230019aa.002|US History 11-P4|230019aa.parent|1415_Courses|Y
230019aa.003|230019aa.003|US History 11-P6|230019aa.parent|1415_Courses|Y
230019aa.004|230019aa.004|US History 11-P7|230019aa.parent|1415_Courses|Y
230018aa.parent|230018aa.parent|U S History Pre AP||1415_Courses|Y
230018aa.001|230018aa.001|U S History Pre AP-P4|230018aa.parent|1415_Courses|Y
230022aa.parent|230022aa.parent|AP US History||1415_Courses|Y
230022aa.001|230022aa.001|AP US History-P2|230022aa.parent|1415_Courses|Y
230018aa.parent|230018aa.parent|U S History Pre AP||1415_Courses|Y
230018aa.002|230018aa.002|U S History Pre AP-P7|230018aa.parent|1415_Courses|Y
230041aa.parent|230041aa.parent|US Government||1415_Courses|Y
230041aa.001|230041aa.001|US Government-P1|230041aa.parent|1415_Courses|Y
230041aa.002|230041aa.002|US Government-P4|230041aa.parent|1415_Courses|Y
230041aa.003|230041aa.003|US Government-P6|230041aa.parent|1415_Courses|Y
230043aa.parent|230043aa.parent|U S Government Honors||1415_Courses|Y
230043aa.002|230043aa.002|U S Government Honors-P5|230043aa.parent|1415_Courses|Y
230013aa.parent|230013aa.parent|World History||1415_Courses|Y
230013aa.001|230013aa.001|World History-P3|230013aa.parent|1415_Courses|Y
230013aa.002|230013aa.002|World History-P4|230013aa.parent|1415_Courses|Y
230013aa.004|230013aa.004|World History-P1|230013aa.parent|1415_Courses|Y
230051.parent|230051.parent|Economics||1415_Courses|Y
230051.001|230051.001|Economics-P1|230051.parent|1415_Courses|Y
230051.002|230051.002|Economics-P4|230051.parent|1415_Courses|Y
230015aa.parent|230015aa.parent|World History Advanced||1415_Courses|Y
230015aa.001|230015aa.001|World History Advanced-P2|230015aa.parent|1415_Courses|Y
230051.parent|230051.parent|Economics||1415_Courses|Y
230051.003|230051.003|Economics-P6|230051.parent|1415_Courses|Y
230015aa.parent|230015aa.parent|World History Advanced||1415_Courses|Y
230015aa.002|230015aa.002|World History Advanced-P5|230015aa.parent|1415_Courses|Y
230053aa.parent|230053aa.parent|Economics Honors||1415_Courses|Y
230053aa.002|230053aa.002|Economics Honors-P5|230053aa.parent|1415_Courses|Y
230201.parent|230201.parent|Current Affairs||1415_Courses|Y
230201.001|230201.001|Current Affairs-P3|230201.parent|1415_Courses|Y
230201.002|230201.002|Current Affairs-P3|230201.parent|1415_Courses|Y
230019aa.parent|230019aa.parent|US History 11||1415_Courses|Y
230019aa.005|230019aa.005|US History 11-P1|230019aa.parent|1415_Courses|Y
210008.parent|210008.parent|Algebra A||1415_Courses|Y
210008.002|210008.002|Algebra A-P4|210008.parent|1415_Courses|Y
210008.003|210008.003|Algebra A-P2|210008.parent|1415_Courses|Y
210008.004|210008.004|Algebra A-P3|210008.parent|1415_Courses|Y
210025.parent|210025.parent|AP Calculus||1415_Courses|Y
210025.001|210025.001|AP Calculus-P3|210025.parent|1415_Courses|Y
210017aa.parent|210017aa.parent|Algebra II Wth Trig||1415_Courses|Y
210017aa.001|210017aa.001|Algebra II Wth Trig-P1|210017aa.parent|1415_Courses|Y
210017aa.002|210017aa.002|Algebra II Wth Trig-P2|210017aa.parent|1415_Courses|Y
210012ab.parent|210012ab.parent|Geometry Pre AP||1415_Courses|Y
210012ab.001|210012ab.001|Geometry Pre AP-P1|210012ab.parent|1415_Courses|Y
210012ab.002|210012ab.002|Geometry Pre AP-P7|210012ab.parent|1415_Courses|Y
210010aa.parent|210010aa.parent|Geometry||1415_Courses|Y
210010aa.001|210010aa.001|Geometry-P3|210010aa.parent|1415_Courses|Y
210020aa.parent|210020aa.parent|Pre-Calculus||1415_Courses|Y
210020aa.001|210020aa.001|Pre-Calculus-P2|210020aa.parent|1415_Courses|Y
210020aa.002|210020aa.002|Pre-Calculus-P6|210020aa.parent|1415_Courses|Y
210020aa.003|210020aa.003|Pre-Calculus-P7|210020aa.parent|1415_Courses|Y
220073.parent|220073.parent|Physics||1415_Courses|Y
220073.001|220073.001|Physics-P1|220073.parent|1415_Courses|Y
220073.002|220073.002|Physics-P6|220073.parent|1415_Courses|Y
220051aa.001|220051aa.001|Physical Science-P1||1415_Courses|Y
220051aa.003|220051aa.003|Physical Science-P7||1415_Courses|Y
220051aa.004|220051aa.004|Physical Science-P4||1415_Courses|Y
220062aa.parent|220062aa.parent|Chemistry Pre AP||1415_Courses|Y
220062aa.001|220062aa.001|Chemistry Pre AP-P2|220062aa.parent|1415_Courses|Y
220062aa.002|220062aa.002|Chemistry Pre AP-P3|220062aa.parent|1415_Courses|Y
220064.parent|220064.parent|AP Chemistry||1415_Courses|Y
220064.001|220064.001|AP Chemistry-P5|220064.parent|1415_Courses|Y
220011aa.001|220011aa.001|Biology-P1||1415_Courses|Y
220011aa.002|220011aa.002|Biology-P2||1415_Courses|Y
220011aa.003|220011aa.003|Biology-P6||1415_Courses|Y
220011aa.004|220011aa.004|Biology-P3||1415_Courses|Y
220013aa.parent|220013aa.parent|Biology Advanced||1415_Courses|Y
220013aa.001|220013aa.001|Biology Advanced-P4|220013aa.parent|1415_Courses|Y
220013aa.002|220013aa.002|Biology Advanced-P7|220013aa.parent|1415_Courses|Y
220014.parent|220014.parent|AP Biology||1415_Courses|Y
220014.001|220014.001|AP Biology-P5|220014.parent|1415_Courses|Y
220028.parent|220028.parent|AnatomyPhysiology||1415_Courses|Y
220028.001|220028.001|AnatomyPhysiology-P3|220028.parent|1415_Courses|Y
220028.002|220028.002|AnatomyPhysiology-P4|220028.parent|1415_Courses|Y
220028.003|220028.003|AnatomyPhysiology-P5|220028.parent|1415_Courses|Y
250002.001|250002.001|Health-P1||1415_Courses|Y
250002.002|250002.002|Health-P1||1415_Courses|Y
250002.003|250002.003|Health-P4||1415_Courses|Y
250002.004|250002.004|Health-P4||1415_Courses|Y
250002.005|250002.005|Health-P3||1415_Courses|Y
250002.006|250002.006|Health-P3||1415_Courses|Y
240003.parent|240003.parent|Weights Football||1415_Courses|Y
240003.001|240003.001|Weights Football-P1|240003.parent|1415_Courses|Y
240003ac.parent|240003ac.parent|Weights||1415_Courses|Y
240003ac.001|240003ac.001|Weights-P2|240003ac.parent|1415_Courses|Y
240003ac.002|240003ac.002|Weights-P3|240003ac.parent|1415_Courses|Y
240003aa.parent|240003aa.parent|Physical Education||1415_Courses|Y
240003aa.001|240003aa.001|Physical Education-P1|240003aa.parent|1415_Courses|Y
240003aa.002|240003aa.002|Physical Education-P2|240003aa.parent|1415_Courses|Y
240003aa.003|240003aa.003|Physical Education-P3|240003aa.parent|1415_Courses|Y
240003aa.005|240003aa.005|Physical Education-P5|240003aa.parent|1415_Courses|Y
240003aa.006|240003aa.006|Physical Education-P6|240003aa.parent|1415_Courses|Y
240003aa.007|240003aa.007|Physical Education-P7|240003aa.parent|1415_Courses|Y
240003ab.parent|240003ab.parent|Weights Girls||1415_Courses|Y
240003ab.001|240003ab.001|Weights Girls-P7|240003ab.parent|1415_Courses|Y
400026.001|400026.001|Career Preparedness A-P1||1415_Courses|Y
400026.002|400026.002|Career Preparedness A-P1||1415_Courses|Y
400026.003|400026.003|Career Preparedness A-P3||1415_Courses|Y
400026.004|400026.004|Career Preparedness A-P3||1415_Courses|Y
400026.005|400026.005|Career Preparedness A-P5||1415_Courses|Y
400026.006|400026.006|Career Preparedness A-P5||1415_Courses|Y
550011.parent|550011.parent|Marketing Principles||1415_Courses|Y
550011.001|550011.001|Marketing Principles-P5|550011.parent|1415_Courses|Y
410008.parent|410008.parent|Constr Fin  Int Sys||1415_Courses|Y
410008.001|410008.001|Constr Fin  Int Sys-P3|410008.parent|1415_Courses|Y
420009.parent|420009.parent|Agriscience||1415_Courses|Y
420009.001|420009.001|Agriscience-P4|420009.parent|1415_Courses|Y
420009.002|420009.002|Agriscience-P6|420009.parent|1415_Courses|Y
420009.003|420009.003|Agriscience-P7|420009.parent|1415_Courses|Y
470012.parent|470012.parent|Accounting||1415_Courses|Y
470012.001|470012.001|Accounting-P1|470012.parent|1415_Courses|Y
470012.002|470012.002|Accounting-P2|470012.parent|1415_Courses|Y
400022.parent|400022.parent|Personal Finance||1415_Courses|Y
400022.001|400022.001|Personal Finance-P1|400022.parent|1415_Courses|Y
420051.parent|420051.parent|Horticulture Sci||1415_Courses|Y
420051.001|420051.001|Horticulture Sci-P5|420051.parent|1415_Courses|Y
510004.parent|510004.parent|Family Consumer Science||1415_Courses|Y
510004.002|510004.002|Family Consumer Science-P6|510004.parent|1415_Courses|Y
510004.003|510004.003|Family Consumer Science-P7|510004.parent|1415_Courses|Y
470011.parent|470011.parent|BankingFinanc Serv||1415_Courses|Y
470011.001|470011.001|BankingFinanc Serv-P4|470011.parent|1415_Courses|Y
400031.parent|400031.parent|Coop Education Sem||1415_Courses|Y
400031.001|400031.001|Coop Education Sem-P6|400031.parent|1415_Courses|Y
400031.002|400031.002|Coop Education Sem-P7|400031.parent|1415_Courses|Y
270154.parent|270154.parent|Spanish II||1415_Courses|Y
270154.001|270154.001|Spanish II-P1|270154.parent|1415_Courses|Y
270154.003|270154.003|Spanish II-P7|270154.parent|1415_Courses|Y
270153.parent|270153.parent|Spanish I||1415_Courses|Y
270153.001|270153.001|Spanish I-P2|270153.parent|1415_Courses|Y
270153.002|270153.002|Spanish I-P5|270153.parent|1415_Courses|Y
270153.003|270153.003|Spanish I-P6|270153.parent|1415_Courses|Y
802206ag.parent|802206ag.parent|Library Aide||1415_Courses|Y
802206ag.001|802206ag.001|Library Aide-P1|802206ag.parent|1415_Courses|Y
802206ag.002|802206ag.002|Library Aide-P2|802206ag.parent|1415_Courses|Y
802206ag.003|802206ag.003|Library Aide-P3|802206ag.parent|1415_Courses|Y
802206ag.004|802206ag.004|Library Aide-P4|802206ag.parent|1415_Courses|Y
802206ag.005|802206ag.005|Library Aide-P5|802206ag.parent|1415_Courses|Y
802206ag.006|802206ag.006|Library Aide-P6|802206ag.parent|1415_Courses|Y
802206ag.007|802206ag.007|Library Aide-P7|802206ag.parent|1415_Courses|Y
802206ab.parent|802206ab.parent|Library Aide 1/2||1415_Courses|Y
802206ab.002|802206ab.002|Library Aide 1/2-P2|802206ab.parent|1415_Courses|Y
802206ab.003|802206ab.003|Library Aide 1/2-P3|802206ab.parent|1415_Courses|Y
802206ab.004|802206ab.004|Library Aide 1/2-P4|802206ab.parent|1415_Courses|Y
802206ab.005|802206ab.005|Library Aide 1/2-P5|802206ab.parent|1415_Courses|Y
802206ab.006|802206ab.006|Library Aide 1/2-P6|802206ab.parent|1415_Courses|Y
802206ab.007|802206ab.007|Library Aide 1/2-P7|802206ab.parent|1415_Courses|Y
802206ae.parent|802206ae.parent|Peer Tutor||1415_Courses|Y
802206ae.001|802206ae.001|Peer Tutor-P1|802206ae.parent|1415_Courses|Y
802206ae.002|802206ae.002|Peer Tutor-P2|802206ae.parent|1415_Courses|Y
802206ae.003|802206ae.003|Peer Tutor-P3|802206ae.parent|1415_Courses|Y
802206ae.004|802206ae.004|Peer Tutor-P4|802206ae.parent|1415_Courses|Y
802206ae.005|802206ae.005|Peer Tutor-P5|802206ae.parent|1415_Courses|Y
802206ae.006|802206ae.006|Peer Tutor-P6|802206ae.parent|1415_Courses|Y
802206ae.007|802206ae.007|Peer Tutor-P7|802206ae.parent|1415_Courses|Y
802206ab.parent|802206ab.parent|Library Aide 1/2||1415_Courses|Y
802206ab.008|802206ab.008|Library Aide 1/2-P1|802206ab.parent|1415_Courses|Y
802206ab.009|802206ab.009|Library Aide 1/2-P2|802206ab.parent|1415_Courses|Y
802206ab.010|802206ab.010|Library Aide 1/2-P4|802206ab.parent|1415_Courses|Y
802206ab.011|802206ab.011|Library Aide 1/2-P5|802206ab.parent|1415_Courses|Y
802206ab.012|802206ab.012|Library Aide 1/2-P6|802206ab.parent|1415_Courses|Y
802206ab.013|802206ab.013|Library Aide 1/2-P7|802206ab.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.001|802206af.001|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
802206ab.parent|802206ab.parent|Library Aide 1/2||1415_Courses|Y
802206ab.014|802206ab.014|Library Aide 1/2-P3|802206ab.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.002|802206af.002|Peer Tutor 1/2-P2|802206af.parent|1415_Courses|Y
802206af.003|802206af.003|Peer Tutor 1/2-P3|802206af.parent|1415_Courses|Y
802206af.004|802206af.004|Peer Tutor 1/2-P4|802206af.parent|1415_Courses|Y
802206af.005|802206af.005|Peer Tutor 1/2-P5|802206af.parent|1415_Courses|Y
802206af.006|802206af.006|Peer Tutor 1/2-P6|802206af.parent|1415_Courses|Y
802206af.007|802206af.007|Peer Tutor 1/2-P7|802206af.parent|1415_Courses|Y
280051ab.parent|280051ab.parent|Men's Choir||1415_Courses|Y
280051ab.001|280051ab.001|Men's Choir-P2|280051ab.parent|1415_Courses|Y
802206ah.parent|802206ah.parent|Office Aide||1415_Courses|Y
802206ah.001|802206ah.001|Office Aide-P1|802206ah.parent|1415_Courses|Y
802206ah.002|802206ah.002|Office Aide-P2|802206ah.parent|1415_Courses|Y
802206ah.003|802206ah.003|Office Aide-P3|802206ah.parent|1415_Courses|Y
802206ah.004|802206ah.004|Office Aide-P4|802206ah.parent|1415_Courses|Y
802206ah.006|802206ah.006|Office Aide-P6|802206ah.parent|1415_Courses|Y
802206ah.007|802206ah.007|Office Aide-P7|802206ah.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.008|802206af.008|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
802206af.009|802206af.009|Peer Tutor 1/2-P2|802206af.parent|1415_Courses|Y
802206af.010|802206af.010|Peer Tutor 1/2-P3|802206af.parent|1415_Courses|Y
802206ac.parent|802206ac.parent|OfficeAide 1/2||1415_Courses|Y
802206ac.003|802206ac.003|OfficeAide 1/2-P3|802206ac.parent|1415_Courses|Y
802206ac.004|802206ac.004|OfficeAide 1/2-P4|802206ac.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.011|802206af.011|Peer Tutor 1/2-P4|802206af.parent|1415_Courses|Y
802206ac.parent|802206ac.parent|OfficeAide 1/2||1415_Courses|Y
802206ac.005|802206ac.005|OfficeAide 1/2-P5|802206ac.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.012|802206af.012|Peer Tutor 1/2-P5|802206af.parent|1415_Courses|Y
802206ac.parent|802206ac.parent|OfficeAide 1/2||1415_Courses|Y
802206ac.007|802206ac.007|OfficeAide 1/2-P7|802206ac.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.013|802206af.013|Peer Tutor 1/2-P6|802206af.parent|1415_Courses|Y
802206af.014|802206af.014|Peer Tutor 1/2-P7|802206af.parent|1415_Courses|Y
802206ac.parent|802206ac.parent|OfficeAide 1/2||1415_Courses|Y
802206ac.008|802206ac.008|OfficeAide 1/2-P1|802206ac.parent|1415_Courses|Y
802206ac.009|802206ac.009|OfficeAide 1/2-P2|802206ac.parent|1415_Courses|Y
280055aa.parent|280055aa.parent|Concert Choir||1415_Courses|Y
280055aa.001|280055aa.001|Concert Choir-P3|280055aa.parent|1415_Courses|Y
802206ac.parent|802206ac.parent|OfficeAide 1/2||1415_Courses|Y
802206ac.010|802206ac.010|OfficeAide 1/2-P3|802206ac.parent|1415_Courses|Y
802206ac.011|802206ac.011|OfficeAide 1/2-P4|802206ac.parent|1415_Courses|Y
802206ac.012|802206ac.012|OfficeAide 1/2-P5|802206ac.parent|1415_Courses|Y
802206ac.013|802206ac.013|OfficeAide 1/2-P6|802206ac.parent|1415_Courses|Y
802206ac.014|802206ac.014|OfficeAide 1/2-P7|802206ac.parent|1415_Courses|Y
280051ad.parent|280051ad.parent|Men's Choir Intermediate||1415_Courses|Y
280051ad.001|280051ad.001|Men's Choir Intermediate-P5|280051ad.parent|1415_Courses|Y
280051aa.parent|280051aa.parent|Women's Choir||1415_Courses|Y
280051aa.001|280051aa.001|Women's Choir-P6|280051aa.parent|1415_Courses|Y
280031aa.parent|280031aa.parent|Band||1415_Courses|Y
280031aa.001|280031aa.001|Band-P6|280031aa.parent|1415_Courses|Y
280033aa.parent|280033aa.parent|Band Intermediate||1415_Courses|Y
280033aa.001|280033aa.001|Band Intermediate-P5|280033aa.parent|1415_Courses|Y
280035aa.parent|280035aa.parent|Band Advanced||1415_Courses|Y
280035aa.001|280035aa.001|Band Advanced-P4|280035aa.parent|1415_Courses|Y
280039.parent|280039.parent|Jazz Band||1415_Courses|Y
280039.001|280039.001|Jazz Band-P7|280039.parent|1415_Courses|Y
280057aa.parent|280057aa.parent|Chamber Choir||1415_Courses|Y
280057aa.001|280057aa.001|Chamber Choir-P4|280057aa.parent|1415_Courses|Y
280051ac.parent|280051ac.parent|Women's Choir Intermediate||1415_Courses|Y
280051ac.001|280051ac.001|Women's Choir Intermediate-P5|280051ac.parent|1415_Courses|Y
280057ab.parent|280057ab.parent|Show Choir||1415_Courses|Y
280057ab.001|280057ab.001|Show Choir-P7|280057ab.parent|1415_Courses|Y
200013aa.parent|200013aa.parent|English 11||1415_Courses|Y
200013aa.001|200013aa.001|English 11-P1|200013aa.parent|1415_Courses|Y
200013aa.002|200013aa.002|English 11-P3|200013aa.parent|1415_Courses|Y
200013aa.003|200013aa.003|English 11-P5|200013aa.parent|1415_Courses|Y
200013aa.004|200013aa.004|English 11-P6|200013aa.parent|1415_Courses|Y
200007aa.parent|200007aa.parent|English 9 Advanced||1415_Courses|Y
200007aa.003|200007aa.003|English 9 Advanced-P5|200007aa.parent|1415_Courses|Y
400026.007|400026.007|Career Preparedness A-P7||1415_Courses|Y
400026.008|400026.008|Career Preparedness A-P7||1415_Courses|Y
250002.008|250002.008|Health-P7||1415_Courses|Y
210017aa.parent|210017aa.parent|Algebra II Wth Trig||1415_Courses|Y
210017aa.003|210017aa.003|Algebra II Wth Trig-P6|210017aa.parent|1415_Courses|Y
220051aa.005|220051aa.005|Physical Science-P5||1415_Courses|Y
460013.parent|460013.parent|EarlyChldhdEduc I||1415_Courses|Y
460013.001|460013.001|EarlyChldhdEduc I-P1, 2|460013.parent|1415_Courses|Y
430071.parent|430071.parent|Applied Weld IPlasm||1415_Courses|Y
430071.001|430071.001|Applied Weld IPlasm-P1, 2|430071.parent|1415_Courses|Y
410024.parent|410024.parent|Emergency Services||1415_Courses|Y
410024.001|410024.001|Emergency Services-P1, 2|410024.parent|1415_Courses|Y
410005.parent|410005.parent|Intro To Draft Desgn||1415_Courses|Y
410005.001|410005.001|Intro To Draft Desgn-P1, 2|410005.parent|1415_Courses|Y
430010.parent|430010.parent|Intermediate Design||1415_Courses|Y
430010.001|430010.001|Intermediate Design-P1, 2|430010.parent|1415_Courses|Y
510066.parent|510066.parent|Intro To Nail Care||1415_Courses|Y
510066.001|510066.001|Intro To Nail Care-P1, 2|510066.parent|1415_Courses|Y
410002.parent|410002.parent|PowrEquibmntTech||1415_Courses|Y
410002.001|410002.001|PowrEquibmntTech-P1, 2|410002.parent|1415_Courses|Y
410001.parent|410001.parent|TwoFourStrkEng||1415_Courses|Y
410001.001|410001.001|TwoFourStrkEng-P1, 2|410001.parent|1415_Courses|Y
802206aa.parent|802206aa.parent|Lab Assistant||1415_Courses|Y
802206aa.001|802206aa.001|Lab Assistant-P1|802206aa.parent|1415_Courses|Y
802206aa.002|802206aa.002|Lab Assistant-P2|802206aa.parent|1415_Courses|Y
802206aa.003|802206aa.003|Lab Assistant-P3|802206aa.parent|1415_Courses|Y
802206aa.004|802206aa.004|Lab Assistant-P4|802206aa.parent|1415_Courses|Y
802206aa.006|802206aa.006|Lab Assistant-P6|802206aa.parent|1415_Courses|Y
802206aa.007|802206aa.007|Lab Assistant-P7|802206aa.parent|1415_Courses|Y
802206ad.parent|802206ad.parent|Lab Assistant 1/2||1415_Courses|Y
802206ad.003|802206ad.003|Lab Assistant 1/2-P3|802206ad.parent|1415_Courses|Y
802206ad.005|802206ad.005|Lab Assistant 1/2-P5|802206ad.parent|1415_Courses|Y
802206ad.008|802206ad.008|Lab Assistant 1/2-P1|802206ad.parent|1415_Courses|Y
230015aa.parent|230015aa.parent|World History Advanced||1415_Courses|Y
230015aa.003|230015aa.003|World History Advanced-P6|230015aa.parent|1415_Courses|Y
240003aj.parent|240003aj.parent|Physical Education 1/2||1415_Courses|Y
240003aj.001|240003aj.001|Physical Education 1/2-P1|240003aj.parent|1415_Courses|Y
240003aj.002|240003aj.002|Physical Education 1/2-P1|240003aj.parent|1415_Courses|Y
240003aj.005|240003aj.005|Physical Education 1/2-P3|240003aj.parent|1415_Courses|Y
240003aj.006|240003aj.006|Physical Education 1/2-P3|240003aj.parent|1415_Courses|Y
240003aj.007|240003aj.007|Physical Education 1/2-P5|240003aj.parent|1415_Courses|Y
240003aj.008|240003aj.008|Physical Education 1/2-P5|240003aj.parent|1415_Courses|Y
240003aj.012|240003aj.012|Physical Education 1/2-P6|240003aj.parent|1415_Courses|Y
240003aj.013|240003aj.013|Physical Education 1/2-P7|240003aj.parent|1415_Courses|Y
240003aj.014|240003aj.014|Physical Education 1/2-P7|240003aj.parent|1415_Courses|Y
240003ae.parent|240003ae.parent|Weights 1/2||1415_Courses|Y
240003ae.002|240003ae.002|Weights 1/2-P2|240003ae.parent|1415_Courses|Y
240003ak.parent|240003ak.parent|Weights Girls 1/2||1415_Courses|Y
240003ak.001|240003ak.001|Weights Girls 1/2-P7|240003ak.parent|1415_Courses|Y
240003ak.002|240003ak.002|Weights Girls 1/2-P7|240003ak.parent|1415_Courses|Y
600053aa.parent|600053aa.parent|Biology Essentials||1415_Courses|Y
600053aa.001|600053aa.001|Biology Essentials-P1|600053aa.parent|1415_Courses|Y
600062aa.parent|600062aa.parent|Forensics Essentials||1415_Courses|Y
600062aa.001|600062aa.001|Forensics Essentials-P4|600062aa.parent|1415_Courses|Y
700006.parent|700006.parent|English Essentials-10||1415_Courses|Y
700006.001|700006.001|English Essentials-10-P1|700006.parent|1415_Courses|Y
700008.parent|700008.parent|English Essentials-12||1415_Courses|Y
700008.001|700008.001|English Essentials-12-P6|700008.parent|1415_Courses|Y
600512aa.parent|600512aa.parent|AAS:Life Skills 9||1415_Courses|Y
600512aa.001|600512aa.001|AAS:Life Skills 9-P5|600512aa.parent|1415_Courses|Y
600002.parent|600002.parent|Adaptive PE||1415_Courses|Y
600002.001|600002.001|Adaptive PE-P1|600002.parent|1415_Courses|Y
802104ab.001|802104ab.001|General Studies-P3||1415_Courses|Y
802104ab.002|802104ab.002|General Studies-P7||1415_Courses|Y
802104ab.003|802104ab.003|General Studies-P6||1415_Courses|Y
700006.parent|700006.parent|English Essentials-10||1415_Courses|Y
700006.002|700006.002|English Essentials-10-P2|700006.parent|1415_Courses|Y
220081ab.001|220081ab.001|Earth and Space Science-P2||1415_Courses|Y
220081ab.003|220081ab.003|Earth and Space Science-P6||1415_Courses|Y
220081ab.004|220081ab.004|Earth and Space Science-P7||1415_Courses|Y
240003ae.parent|240003ae.parent|Weights 1/2||1415_Courses|Y
240003ae.004|240003ae.004|Weights 1/2-P3|240003ae.parent|1415_Courses|Y
280055aa.parent|280055aa.parent|Concert Choir||1415_Courses|Y
280055aa.002|280055aa.002|Concert Choir-P4|280055aa.parent|1415_Courses|Y
200019.parent|200019.parent|College English 101||1415_Courses|Y
200019.001|200019.001|College English 101-P1|200019.parent|1415_Courses|Y
200019.002|200019.002|College English 101-P1|200019.parent|1415_Courses|Y
200019.003|200019.003|College English 101-P5|200019.parent|1415_Courses|Y
200019.004|200019.004|College English 101-P5|200019.parent|1415_Courses|Y
200019.005|200019.005|College English 101-P6|200019.parent|1415_Courses|Y
200019.006|200019.006|College English 101-P6|200019.parent|1415_Courses|Y
200019.007|200019.007|College English 101-P3|200019.parent|1415_Courses|Y
230016.parent|230016.parent|College US History 201||1415_Courses|Y
230016.001|230016.001|College US History 201-P5|230016.parent|1415_Courses|Y
200019.parent|200019.parent|College English 101||1415_Courses|Y
200019.008|200019.008|College English 101-P7|200019.parent|1415_Courses|Y
400031.parent|400031.parent|Coop Education Sem||1415_Courses|Y
400031.003|400031.003|Coop Education Sem-P5|400031.parent|1415_Courses|Y
230074.parent|230074.parent|Psychology College||1415_Courses|Y
230074.001|230074.001|Psychology College-P1|230074.parent|1415_Courses|Y
230074.002|230074.002|Psychology College-P7|230074.parent|1415_Courses|Y
230016.parent|230016.parent|College US History 201||1415_Courses|Y
230016.002|230016.002|College US History 201-P2|230016.parent|1415_Courses|Y
230016.003|230016.003|College US History 201-P2|230016.parent|1415_Courses|Y
230016ab.parent|230016ab.parent|College: US History 202||1415_Courses|Y
230016ab.001|230016ab.001|College: US History 202-P2|230016ab.parent|1415_Courses|Y
230016ab.002|230016ab.002|College: US History 202-P2|230016ab.parent|1415_Courses|Y
230016.parent|230016.parent|College US History 201||1415_Courses|Y
230016.004|230016.004|College US History 201-P1|230016.parent|1415_Courses|Y
240003.parent|240003.parent|Weights Football||1415_Courses|Y
240003.002|240003.002|Weights Football-P1|240003.parent|1415_Courses|Y
700007.parent|700007.parent|English Essentials-11||1415_Courses|Y
700007.003|700007.003|English Essentials-11-P2|700007.parent|1415_Courses|Y
430073.parent|430073.parent|Applied Weld IIIGrv||1415_Courses|Y
430073.001|430073.001|Applied Weld IIIGrv-P1, 2|430073.parent|1415_Courses|Y
430022.parent|430022.parent|Cabinetmaking II||1415_Courses|Y
430022.001|430022.001|Cabinetmaking II-P1, 2|430022.parent|1415_Courses|Y
700005.parent|700005.parent|English Essentials-9||1415_Courses|Y
700005.003|700005.003|English Essentials-9-P3|700005.parent|1415_Courses|Y
600512aa.parent|600512aa.parent|AAS:Life Skills 10||1415_Courses|Y
600512aa.002|600512aa.002|AAS:Life Skills 10-P5|600512aa.parent|1415_Courses|Y
220051aa.006|220051aa.006|Physical Science-P6||1415_Courses|Y
802206.parent|802206.parent|StudentAide6-12C||1415_Courses|Y
802206.021|802206.021|StudentAide6-12C-P5|802206.parent|1415_Courses|Y
280099.parent|280099.parent|Art Appreciation||1415_Courses|Y
280099.001|280099.001|Art Appreciation-P1|280099.parent|1415_Courses|Y
200011aa.parent|200011aa.parent|English 10 Pre AP||1415_Courses|Y
200011aa.003|200011aa.003|English 10 Pre AP-P2|200011aa.parent|1415_Courses|Y
200017.006|200017.006|English 12-P7||1415_Courses|Y
230018aa.parent|230018aa.parent|U S History Pre AP||1415_Courses|Y
230018aa.005|230018aa.005|U S History Pre AP-P1|230018aa.parent|1415_Courses|Y
230051.parent|230051.parent|Economics||1415_Courses|Y
230051.005|230051.005|Economics-P3|230051.parent|1415_Courses|Y
230041aa.parent|230041aa.parent|US Government||1415_Courses|Y
230041aa.005|230041aa.005|US Government-P3|230041aa.parent|1415_Courses|Y
480073.001|480073.001|ACT Prep-P5||1415_Courses|Y
480073.002|480073.002|ACT Prep-P5||1415_Courses|Y
210027.parent|210027.parent|Statistics, AP||1415_Courses|Y
210027.001|210027.001|Statistics, AP-P5|210027.parent|1415_Courses|Y
480073.003|480073.003|ACT Prep-P1||1415_Courses|Y
480073.004|480073.004|ACT Prep-P1||1415_Courses|Y
480073.005|480073.005|ACT Prep-P4||1415_Courses|Y
480073.006|480073.006|ACT Prep-P4||1415_Courses|Y
210017aa.parent|210017aa.parent|Algebra II Wth Trig||1415_Courses|Y
210017aa.004|210017aa.004|Algebra II Wth Trig-P7|210017aa.parent|1415_Courses|Y
210012ab.parent|210012ab.parent|Geometry Pre AP||1415_Courses|Y
210012ab.003|210012ab.003|Geometry Pre AP-P4|210012ab.parent|1415_Courses|Y
210010aa.parent|210010aa.parent|Geometry||1415_Courses|Y
210010aa.002|210010aa.002|Geometry-P2|210010aa.parent|1415_Courses|Y
210010aa.003|210010aa.003|Geometry-P5|210010aa.parent|1415_Courses|Y
220013aa.parent|220013aa.parent|Biology Advanced||1415_Courses|Y
220013aa.003|220013aa.003|Biology Advanced-P2|220013aa.parent|1415_Courses|Y
220081ab.002|220081ab.002|Earth and Space Science-P4||1415_Courses|Y
220062aa.parent|220062aa.parent|Chemistry Pre AP||1415_Courses|Y
220062aa.003|220062aa.003|Chemistry Pre AP-P4|220062aa.parent|1415_Courses|Y
400026.009|400026.009|Career Preparedness A-P4||1415_Courses|Y
400026.010|400026.010|Career Preparedness A-P4||1415_Courses|Y
400017.parent|400017.parent|Entrepreneurship||1415_Courses|Y
400017.001|400017.001|Entrepreneurship-P6|400017.parent|1415_Courses|Y
420012.parent|420012.parent|Intro To Metal Fab||1415_Courses|Y
420012.001|420012.001|Intro To Metal Fab-P1|420012.parent|1415_Courses|Y
400022.parent|400022.parent|Personal Finance||1415_Courses|Y
400022.002|400022.002|Personal Finance-P1|400022.parent|1415_Courses|Y
400022.003|400022.003|Personal Finance-P4|400022.parent|1415_Courses|Y
400022.004|400022.004|Personal Finance-P5|400022.parent|1415_Courses|Y
400022.005|400022.005|Personal Finance-P4|400022.parent|1415_Courses|Y
400022.006|400022.006|Personal Finance-P5|400022.parent|1415_Courses|Y
510033.parent|510033.parent|Family Studies & Comm Serv I||1415_Courses|Y
510033.001|510033.001|Family Studies & Comm Serv I-P3|510033.parent|1415_Courses|Y
210009.parent|210009.parent|Algebra B||1415_Courses|Y
210009.004|210009.004|Algebra B-P3|210009.parent|1415_Courses|Y
210009.005|210009.005|Algebra B-P2|210009.parent|1415_Courses|Y
210009.006|210009.006|Algebra B-P6|210009.parent|1415_Courses|Y
210016.parent|210016.parent|Algebra II||1415_Courses|Y
210016.001|210016.001|Algebra II-P5|210016.parent|1415_Courses|Y
210016.002|210016.002|Algebra II-P7|210016.parent|1415_Courses|Y
210007ab.parent|210007ab.parent|Algebra I Advanced||1415_Courses|Y
210007ab.001|210007ab.001|Algebra I Advanced-P1|210007ab.parent|1415_Courses|Y
210007ab.002|210007ab.002|Algebra I Advanced-P4|210007ab.parent|1415_Courses|Y
220028.parent|220028.parent|AnatomyPhysiology||1415_Courses|Y
220028.004|220028.004|AnatomyPhysiology-P2|220028.parent|1415_Courses|Y
270155.parent|270155.parent|Spanish III||1415_Courses|Y
270155.001|270155.001|Spanish III-P3|270155.parent|1415_Courses|Y
230043aa.parent|230043aa.parent|U S Government Honors||1415_Courses|Y
230043aa.007|230043aa.007|U S Government Honors-P1|230043aa.parent|1415_Courses|Y
230053aa.parent|230053aa.parent|Economics Honors||1415_Courses|Y
230053aa.010|230053aa.010|Economics Honors-P1|230053aa.parent|1415_Courses|Y
250002.022|250002.022|Health-P5||1415_Courses|Y
250002.023|250002.023|Health-P5||1415_Courses|Y
230043aa.parent|230043aa.parent|U S Government Honors||1415_Courses|Y
230043aa.015|230043aa.015|U S Government Honors-P3|230043aa.parent|1415_Courses|Y
230053aa.parent|230053aa.parent|Economics Honors||1415_Courses|Y
230053aa.015|230053aa.015|Economics Honors-P3|230053aa.parent|1415_Courses|Y
520005.parent|520005.parent|InformtnTechFund||1415_Courses|Y
520005.001|520005.001|InformtnTechFund-P1, 2|520005.parent|1415_Courses|Y
410018.parent|410018.parent|InformtnTechSuppSer||1415_Courses|Y
410018.001|410018.001|InformtnTechSuppSer-P1, 2|410018.parent|1415_Courses|Y
230043aa.parent|230043aa.parent|U S Government Honors||1415_Courses|Y
230043aa.010|230043aa.010|U S Government Honors-P6|230043aa.parent|1415_Courses|Y
230053aa.parent|230053aa.parent|Economics Honors||1415_Courses|Y
230053aa.011|230053aa.011|Economics Honors-P6|230053aa.parent|1415_Courses|Y
230074.parent|230074.parent|Psychology College||1415_Courses|Y
230074.003|230074.003|Psychology College-P6|230074.parent|1415_Courses|Y
250002.020|250002.020|Health-P6||1415_Courses|Y
270153.parent|270153.parent|Spanish I||1415_Courses|Y
270153.008|270153.008|Spanish I-P1|270153.parent|1415_Courses|Y
480073.007|480073.007|ACT Prep-P4||1415_Courses|Y
480073.008|480073.008|ACT Prep-P4||1415_Courses|Y
230201aa.parent|230201aa.parent|Current Affairs||1415_Courses|Y
230201aa.001|230201aa.001|Current Affairs-P2|230201aa.parent|1415_Courses|Y
240003ac.parent|240003ac.parent|Weights||1415_Courses|Y
240003ac.004|240003ac.004|Weights-P4|240003ac.parent|1415_Courses|Y
240003ae.parent|240003ae.parent|Weights 1/2||1415_Courses|Y
240003ae.005|240003ae.005|Weights 1/2-P4|240003ae.parent|1415_Courses|Y
230074.parent|230074.parent|Psychology College||1415_Courses|Y
230074.004|230074.004|Psychology College-P2|230074.parent|1415_Courses|Y
600419.parent|600419.parent|AAS:Reading-9||1415_Courses|Y
600419.002|600419.002|AAS:Reading-9-P4|600419.parent|1415_Courses|Y
600419.005|600419.005|AAS:Reading-12-P4|600419.parent|1415_Courses|Y
600459.parent|600459.parent|AAS:Mathematics-9||1415_Courses|Y
600459.002|600459.002|AAS:Mathematics-9-P6|600459.parent|1415_Courses|Y
600459.005|600459.005|AAS:Mathematics-12-P6|600459.parent|1415_Courses|Y
600499.parent|600499.parent|AAS:Social Studies-9||1415_Courses|Y
600499.002|600499.002|AAS:Social Studies-9-P2|600499.parent|1415_Courses|Y
600499.005|600499.005|AAS:Social Studies-12-P2|600499.parent|1415_Courses|Y
600062.parent|600062.parent|Earth Science Essentials||1415_Courses|Y
600062.002|600062.002|Earth Science Essentials-P2|600062.parent|1415_Courses|Y
600054aa.parent|600054aa.parent|Physical Science Essentials||1415_Courses|Y
600054aa.003|600054aa.003|Physical Science Essentials-P5|600054aa.parent|1415_Courses|Y
802104ab.006|802104ab.006|General Studies-P6||1415_Courses|Y
600479aa.parent|600479aa.parent|AAS:Science 9||1415_Courses|Y
600479aa.002|600479aa.002|AAS:Science 9-P3|600479aa.parent|1415_Courses|Y
600512aa.parent|600512aa.parent|AAS:Life Skills 12||1415_Courses|Y
600512aa.004|600512aa.004|AAS:Life Skills 12-P5|600512aa.parent|1415_Courses|Y
600479aa.parent|600479aa.parent|AAS:Science 12||1415_Courses|Y
600479aa.005|600479aa.005|AAS:Science 12-P3|600479aa.parent|1415_Courses|Y
600302.parent|600302.parent|Workforce Essentials||1415_Courses|Y
600302.001|600302.001|Workforce Essentials-P3|600302.parent|1415_Courses|Y
600551.parent|600551.parent|AAS:Health||1415_Courses|Y
600551.001|600551.001|AAS:Health-P7|600551.parent|1415_Courses|Y
600521aa.parent|600521aa.parent|AAS:Music||1415_Courses|Y
600521aa.001|600521aa.001|AAS:Music-P7|600521aa.parent|1415_Courses|Y
802104ab.008|802104ab.008|General Studies-P4||1415_Courses|Y
280001.parent|280001.parent|Arts Survey||1415_Courses|Y
280001.001|280001.001|Arts Survey-P2|280001.parent|1415_Courses|Y
250002.015|250002.015|Health-P2||1415_Courses|Y
270155.parent|270155.parent|Spanish III||1415_Courses|Y
270155.002|270155.002|Spanish III-P1|270155.parent|1415_Courses|Y
230053aa.parent|230053aa.parent|Economics Honors||1415_Courses|Y
230053aa.012|230053aa.012|Economics Honors-P4|230053aa.parent|1415_Courses|Y
230043aa.parent|230043aa.parent|U S Government Honors||1415_Courses|Y
230043aa.016|230043aa.016|U S Government Honors-P4|230043aa.parent|1415_Courses|Y
210020aa.parent|210020aa.parent|Pre-Calculus||1415_Courses|Y
210020aa.004|210020aa.004|Pre-Calculus-P3|210020aa.parent|1415_Courses|Y
200013aa.parent|200013aa.parent|English 11||1415_Courses|Y
200013aa.005|200013aa.005|English 11-P7|200013aa.parent|1415_Courses|Y
270154.parent|270154.parent|Spanish II||1415_Courses|Y
270154.004|270154.004|Spanish II-P3|270154.parent|1415_Courses|Y
230022aa.parent|230022aa.parent|AP US History||1415_Courses|Y
230022aa.002|230022aa.002|AP US History-P3|230022aa.parent|1415_Courses|Y
450006.parent|450006.parent|Business Tech App||1415_Courses|Y
450006.001|450006.001|Business Tech App-P7|450006.parent|1415_Courses|Y
802104ac.parent|802104ac.parent|General Studies 1/2||1415_Courses|Y
802104ac.005|802104ac.005|General Studies 1/2-P3|802104ac.parent|1415_Courses|Y
210015.parent|210015.parent|Algebraic Conn||1415_Courses|Y
210015.001|210015.001|Algebraic Conn-P7|210015.parent|1415_Courses|Y
230022aa.parent|230022aa.parent|AP US History||1415_Courses|Y
230022aa.006|230022aa.006|AP US History-P7|230022aa.parent|1415_Courses|Y
200005aa.parent|200005aa.parent|English 9||1415_Courses|Y
200005aa.003|200005aa.003|English 9-P7|200005aa.parent|1415_Courses|Y
802206ae.parent|802206ae.parent|Peer Tutor||1415_Courses|Y
802206ae.010|802206ae.010|Peer Tutor-P7|802206ae.parent|1415_Courses|Y
280001.parent|280001.parent|Arts Survey||1415_Courses|Y
280001.002|280001.002|Arts Survey-P6|280001.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.025|802206af.025|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
802206ae.parent|802206ae.parent|Peer Tutor||1415_Courses|Y
802206ae.008|802206ae.008|Peer Tutor-P3|802206ae.parent|1415_Courses|Y
700005.parent|700005.parent|English Essentials-9||1415_Courses|Y
700005.004|700005.004|English Essentials-9-P6|700005.parent|1415_Courses|Y
270155.parent|270155.parent|Spanish III||1415_Courses|Y
270155.003|270155.003|Spanish III-P7|270155.parent|1415_Courses|Y
802206ae.parent|802206ae.parent|Peer Tutor||1415_Courses|Y
802206ae.015|802206ae.015|Peer Tutor-P7|802206ae.parent|1415_Courses|Y
802206ae.016|802206ae.016|Peer Tutor-P1|802206ae.parent|1415_Courses|Y
802104ab.004|802104ab.004|General Studies-P7||1415_Courses|Y
280051ad.parent|280051ad.parent|Men's Choir Intermediate||1415_Courses|Y
280051ad.002|280051ad.002|Men's Choir Intermediate-P5|280051ad.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.019|802206af.019|Peer Tutor 1/2-P5|802206af.parent|1415_Courses|Y
802206af.015|802206af.015|Peer Tutor 1/2-P4|802206af.parent|1415_Courses|Y
802206af.016|802206af.016|Peer Tutor 1/2-P4|802206af.parent|1415_Courses|Y
802206ae.parent|802206ae.parent|Peer Tutor||1415_Courses|Y
802206ae.011|802206ae.011|Peer Tutor-P1|802206ae.parent|1415_Courses|Y
200019.parent|200019.parent|College English 101||1415_Courses|Y
200019.009|200019.009|College English 101-P2|200019.parent|1415_Courses|Y
240003.parent|240003.parent|Weights Football||1415_Courses|Y
240003.003|240003.003|Weights Football-P1|240003.parent|1415_Courses|Y
600499.parent|600499.parent|AAS:Social Studies-10||1415_Courses|Y
600499.003|600499.003|AAS:Social Studies-10-P2|600499.parent|1415_Courses|Y
600479.parent|600479.parent|AAS:Science-10||1415_Courses|Y
600479.003|600479.003|AAS:Science-10-P3|600479.parent|1415_Courses|Y
600419.parent|600419.parent|AAS:Reading-10||1415_Courses|Y
600419.003|600419.003|AAS:Reading-10-P4|600419.parent|1415_Courses|Y
600459.parent|600459.parent|AAS:Mathematics-10||1415_Courses|Y
600459.003|600459.003|AAS:Mathematics-10-P6|600459.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.026|802206af.026|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
802206af.020|802206af.020|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
200017.007|200017.007|English 12-P4||1415_Courses|Y
280051ac.parent|280051ac.parent|Women's Choir Intermediate||1415_Courses|Y
280051ac.002|280051ac.002|Women's Choir Intermediate-P5|280051ac.parent|1415_Courses|Y
230201.parent|230201.parent|Current Affairs||1415_Courses|Y
230201.003|230201.003|Current Affairs-P6|230201.parent|1415_Courses|Y
280001.parent|280001.parent|Arts Survey||1415_Courses|Y
280001.003|280001.003|Arts Survey-P7|280001.parent|1415_Courses|Y
802104ab.007|802104ab.007|General Studies-P4||1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.021|802206af.021|Peer Tutor 1/2-P3|802206af.parent|1415_Courses|Y
250002.007|250002.007|Health-P7||1415_Courses|Y
802104ac.parent|802104ac.parent|General Studies 1/2||1415_Courses|Y
802104ac.006|802104ac.006|General Studies 1/2-P5|802104ac.parent|1415_Courses|Y
802206af.parent|802206af.parent|Peer Tutor 1/2||1415_Courses|Y
802206af.017|802206af.017|Peer Tutor 1/2-P1|802206af.parent|1415_Courses|Y
802206af.018|802206af.018|Peer Tutor 1/2-P7|802206af.parent|1415_Courses|Y
230201.parent|230201.parent|Current Affairs||1415_Courses|Y
230201.004|230201.004|Current Affairs-P5|230201.parent|1415_Courses|Y
