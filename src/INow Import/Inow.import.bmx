'Import for Information Now

'Directory Variables
Global InowImport_source:String = programDir + "/Inow Import/raw/"
Global InowImport_data:String = programDir + "/Inow Import/data/"


'Lists
Global InowCourseList:TList = New TList
Global InowEnrollList:TList = New TList
Global InowStaffList:TList = New TList
Global InowStudentList:TList = New TList

'Types
Type InowCourse
	Field sectionNumber:String
	Field longName:String
	Field startingGrade:String
	Field term:String
	Field period:String
End Type

Type InowEnrollment
	Field section:String
	Field stateId:String
	Field lName:String
	Field fName:String
	Field teacherNum:String
	Field teacher:String
	Field schoolNum:String
	Field acaSession:String
End Type

Type InowStaff
	Field teacherID:String
	Field lastName:String
	Field firstName:String
	Field email:String
End Type

Type InowStudent
	Field lastName:String
	Field firstName:String
	Field middleName:String
	Field schoolNum:String
	Field studentID:String
	Field isEnrolled:String
End Type

'Function which is called by Integrator in the Import function
Function doInowImport()
	writeLog("[Inow] Downloading most recent export via " + InowImport_source + "Inow Exports.bat")
	system_(InowImport_source + "Inow Exports.bat")

	If FileSize(InowImport_source + "TallasseeCourse.csv") < 2000
		writeLog("[ERROR] Course Import Invalid Size. Less than 2kb. File is " + FileSize(InowImport_source + "TallasseeCourse.csv"))
		RuntimeError("Error with INow Import FileSize. Please check logs.")
		End
	EndIf
	If FileSize(InowImport_source + "TallasseeEnrollment.csv") < 20000
		writeLog("[ERROR] Enrollment Import Invalid Size. Less than 20kb. File is " + FileSize(InowImport_source + "TallasseeEnrollment.csv"))
		RuntimeError("Error with INow Import FileSize. Please check logs.")
		End
	EndIf
	If FileSize(InowImport_source + "TallasseeStaff.csv") < 2000
		writeLog("[ERROR] Staff Import Invalid Size. Less than 2kb. File is " + FileSize(InowImport_source + "TallasseeStaff.csv"))
		RuntimeError("Error with INow Import FileSize. Please check logs.")
		End
	EndIf
	If FileSize(InowImport_source + "TallasseeStudent.csv") < 5000
		writeLog("[ERROR] Student Import Invalid Size. Less than 5kb. File is " + FileSize(InowImport_source + "TallasseeStudent.csv"))
		RuntimeError("Error with INow Import FileSize. Please check logs.")
		End
	EndIf
	
	writeLog("[INow] Files from " + InowImport_source + " seem correct sizes, replacing data in " + InowImport_data)


	If debugApp = False
	'	writeLog("Deleting Cached INow Data")
		DeleteFile(InowImport_data + "TallasseeCourse.csv")
		DeleteFile(InowImport_data + "TallasseeEnrollment.csv")
		DeleteFile(InowImport_data + "TallasseeStaff.csv")
		DeleteFile(InowImport_data + "TallasseeStudent.csv")
	EndIf
	
'	writeLog("Refreshing INow Data")
	CopyFile(InowImport_source + "TallasseeCourse.csv", InowImport_data + "TallasseeCourse.csv")
	CopyFile(InowImport_source + "TallasseeEnrollment.csv", InowImport_data + "TallasseeEnrollment.csv")
	CopyFile(InowImport_source + "TallasseeStaff.csv", InowImport_data + "TallasseeStaff.csv")
	CopyFile(InowImport_source + "TallasseeStudent.csv", InowImport_data + "TallasseeStudent.csv")
	
	
	
End Function

Function doInowProcessImport()
	'read files from .csv and import into appropriate types
		
	writeLog("[Inow] Processing Courses")
	loadCourse()
	
	writeLog("[Inow] Processing Students")
	loadStudent()
	
	writeLog("[Inow] Processing Staff")
	loadStaff()
	
	writeLog("[Inow] Processing Enrollments")
	loadEnrollment()
End Function

Function loadStudent()
	Local file:TStream = OpenFile(InowImport_data + "TallasseeStudent.csv")
	
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[6]
		Local dataList:TList = convertFromCSV(ReadLine(file), "|")
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nStu:InowStudent = New InowStudent
		ListAddLast(InowStudentList, nStu)
			'writeLog("-----")
		nStu.lastName = data[0]
			'writeLog(nStu.lastName)
		nStu.firstName = data[1]
		'	writeLog(nStu.firstName)
		nStu.middleName = data[2]
		'	writeLog(nStu.middleName)
		nStu.schoolNum = data[3]
		'	writeLog(nStu.schoolNum)
		nStu.studentID = data[4]
		'	writeLog(nStu.studentID)
		nStu.isEnrolled = data[5]
		'	writeLog(nStu.isEnrolled)
	Until Eof(file)
	CloseFile file
End Function

Function loadStaff()
	Local file:TStream = OpenFile(InowImport_data + "TallasseeStaff.csv")
	
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[4]
		Local dataList:TList = convertFromCSV(ReadLine(file), "|")
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nStaff:InowStaff = New InowStaff
		ListAddLast(InowStaffList, nStaff)
		'	writeLog("-----")
		nStaff.teacherID = data[0]
		'	writeLog(nStaff.teacherID)
		nStaff.lastName = data[1]
		'	writeLog(nStaff.lastName)
		nStaff.firstName = data[2]
		'	writeLog(nStaff.firstName)
		nStaff.email = data[3]
		'	writeLog(nStaff.email)
	Until Eof(file)
	CloseFile file
End Function

Function loadEnrollment()
	Local file:TStream = OpenFile(InowImport_data + "TallasseeEnrollment.csv")
	
	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[7]
		Local dataList:TList = convertFromCSV(ReadLine(file), "|")
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local nroll:InowEnrollment = New InowEnrollment
		InowEnrollList.AddLast(nroll)
		'	writeLog("-----")
		nroll.section = data[0]
		'	writeLog(nroll.section)
		nroll.stateId = data[1]
		'	writeLog(nroll.stateId)
		nroll.fName = data[2]
		'	writeLog(nroll.fName)
		nroll.teacherNum = data[3]
		'	writeLog(nroll.teacherNum)
		nroll.teacher = data[4]
		'	writeLog(nroll.teacher)
		nroll.schoolNum = data[5]
		'	writeLog(nroll.schoolNum)
		nroll.acaSession = data[6]
		'	writeLog(nroll.acaSession)
	Until Eof(file)
	CloseFile file
End Function


Function loadCourse()
	Local file:TStream = OpenFile(InowImport_data + "TallasseeCourse.csv")

	ReadLine(file) 'title
	ReadLine(file) 'whitespace
	ReadLine(file) 'starter
	ReadLine(file) 'headers
	Repeat
		Local data:String[5]
		Local dataList:TList = convertFromCSV(ReadLine(file), "|")
		Local i = 0
		For Local colm:String = EachIn dataList
			data[i] = colm
			i = i + 1
		Next
		Local thisCourse:InowCourse = New InowCourse
		InowCourseList.AddLast(thisCourse)
		'	writeLog("-----")
		thisCourse.sectionNumber = data[0]
		'	writeLog(thisCourse.sectionNumber)
		thisCourse.longName = data[1]
		'	writeLog(thisCourse.longName)
		thisCourse.startingGrade = data[2]
		'	writeLog(thisCourse.startingGrade)
		thisCourse.term = data[3]
		'	writeLog(thisCourse.term)
		thisCourse.period = data[4]
		'	writeLog(thisCourse.period)
	Until Eof(file)
	CloseFile(file)
End Function