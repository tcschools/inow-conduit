'Will be fed the DC file and will need to reformat it and save it as dc.csv
Global DCExport_data:String = "C:/Exports/VBOSS/"
Type dcEntry
	Field studentID:String
	Field lastName:String
	Field firstName:String
	Field middleName:String
	Field ssn:String
	Field school:String
	Field grade:String
	Field homeRoom:String
	Field birthMonth:String
	Field birthDay:String
	Field birthYear:String
	Field ethnicity:String
	Field eligibility:String
	Field aStreet:String
	Field aCity:String
	Field aState:String
	Field zip:String
	Field homePhone:String
	Field guardianlast:String
	Field guardianFirst:String
	Field workPhone:String
	Field sex:String
	Field wd:String
	Field stateNum:String
EndType

Global dcEntryList:TList = New TList

Function runDC()
	DCprocessData()
	DCexportData()
End Function

Function DCexportData()
	writeLog("[DC] Writing DC Data")
	Local dcFile:TStream = WriteFile(DCExport_data + "dc.csv")
	WriteLine(dcFile, "StateSNUM,Eligibility")
	For Local newEntry:dcEntry = EachIn dcEntryList
		Select newEntry.eligibility
			Case 1
				newEntry.eligibility = "FREE"
			Case 2
				newEntry.eligibility = "REDUCED"
			Case 3
				newEntry.eligibility = "PAID"
			Case 4
				newEntry.eligibility = "SNAP"
				WriteLine(dcFile, newEntry.stateNum + "," + newEntry.eligibility)
			Case 5
				newEntry.eligibility = "TANF"
				WriteLine(dcFile, newEntry.stateNum + "," + newEntry.eligibility)
			Case 6
				newEntry.eligibility = "FDPIR"
			Case 7
				newEntry.eligibility = "FOSTER"
			Case 8
				newEntry.eligibility = "EXTDELIG-SNAP"
			Case 9
				newEntry.eligibility = "EXTDELIG-TANF"
			Case 10
			Case 11
		End Select
	
		
	Next
	CloseFile(dcFile)
End Function

Function DCprocessData()
	writeLog("[DC] Processing DC Data")
	Local vbossFile:TStream = OpenFile(DCExport_data + "VBOSS.csv")
	ReadLine(vbossFile)
	While Not Eof(vbossFile)
		Local wordList:TList = convertFromCSV(ReadLine(vbossFile), ",")
		Local wordCount = 1
		Local tmpEntry:dcEntry = New dcEntry
		ListAddLast(dcEntryList, tmpEntry)
		For Local nextWord:String = EachIn wordList
			nextWord = Left(nextWord, Len(nextWord) - 1)
			nextWord = Right(nextWord, Len(nextWord) - 1)
			nextWord = Trim(nextWord)
		
			
			Select wordCount
				Case 1
					tmpEntry.studentID = nextWord
				Case 2
					tmpEntry.lastName = nextWord
				Case 3
					tmpEntry.firstName = nextWord
				Case 4
					tmpEntry.middleName = nextWord
				Case 5
					tmpEntry.ssn = nextWord
				Case 6
					tmpEntry.school = nextWord
				Case 7
					tmpEntry.grade = nextWord
				Case 8
					tmpEntry.homeRoom = nextWord
				Case 9
					tmpEntry.birthMonth = nextWord
				Case 10
					tmpEntry.birthDay = nextWord
				Case 11
					tmpEntry.birthYear = nextWord
				Case 12
					tmpEntry.ethnicity = nextWord
				Case 13
					tmpEntry.eligibility = nextWord
				Case 14
					tmpEntry.aStreet = nextWord
				Case 15
					tmpEntry.aCity = nextWord
				Case 16
					tmpEntry.aState = nextWord
				Case 17
					tmpEntry.zip = nextWord
				Case 18
					tmpEntry.homePhone = nextWord
				Case 19
					tmpEntry.guardianlast = nextWord
				Case 20
					tmpEntry.guardianFirst = nextWord
				Case 21
					tmpEntry.workPhone = nextWord
				Case 22
					tmpEntry.sex = nextWord
				Case 23
					tmpEntry.wd = nextWord
				Case 24
					tmpEntry.stateNum = nextWord
			End Select
			wordCount = wordCount + 1
		Next
	Wend
	CloseFile(vbossFile)
End Function