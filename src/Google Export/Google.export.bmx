'Google Export

'Directory Variable
Global GoogleExport_data:String = programDir + "/Google Export/data/"

'Type
Type googleUser
	Field samid:String
	Field firstName:String
	Field lastName:String
EndType

'Lists
Global googleUserList:TList = New TList

'Export
Function doGoogleProcessExport()
	'For every highschool student create a google user.
	For Local checkStudent:InowStudent = EachIn InowStudentList
		Local tmpGoogleUser:googleUser = New googleUser
		ListAddLast(googleUserList, tmpGoogleUser)
		
		tmpGoogleUser.firstName = checkStudent.firstName
		tmpGoogleUser.lastName = checkStudent.lastName
		tmpGoogleUser.samid = tmpGoogleUser.firstName + "." + tmpGoogleUser.lastName
	Next
EndFunction

Function doGoogleExport()
	DeleteFile(GoogleExport_data + "email_process.txt")
	Local emailFile:TStream = WriteFile(GoogleExport_data + "email_process.txt")
	
	For Local tmpEmail:googleUser = EachIn googleUserList
		WriteLine(emailFile, tmpEmail.samid.Replace(" ", "") + "," + tmpEmail.firstName.Replace(" ", "") + "," + tmpEmail.lastName)
	Next
	CloseFile(emailFile)
End Function

Function doGoogleSend()
	writeLog("[Google] Sending Google Data via " + GoogleExport_data + "SendEmailProcess.bat")
	system_(GoogleExport_data + "SendEmailProcess.bat")
End Function