Global logDir:String
Global logFile:TStream

Function startLog()
	logDir = programDir + "/Logs/"
	logFile = WriteFile(logDir + "LogFile.txt")
	writeLog("[Log] Starting Runtime Log")
End Function

Function killLog()
	writeLog("[Log] Killing Runtime Log")
	CloseFile(logFile)
End Function

Function writeLog(entry:String)
	Local logAppend:String = "[" + CurrentTime() + "] "
	If debugApp
		Print logAppend + entry
	EndIf
	WriteLine(logFile, logAppend + entry)
End Function