Global logDir:String
Global logFile:TStream

Function startLog()
	logDir = programDir + "/Logs/"
	logFile = WriteFile(logDir + "LogFile.txt")
	writeLog("start log")
End Function

Function killLog()
	writeLog("kill log")
	CloseFile(logFile)
End Function

Function writeLog(entry:String)
	If debugApp
		Local logAppend:String = "[" + CurrentTime() + "] "
		Print logAppend + entry
		WriteLine(logFile, logAppend + entry)
	EndIf
End Function